/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_sysmon.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   IR coms peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_sysmon_init(void) - initialise the gpio pins
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <s4380479_hal_sysmon.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
 * Initialises the pins used for the system monitor
 */
extern void s4380479_hal_sysmon_init(void) {

	GPIO_InitTypeDef GPIO_InitStructure;

	__BRD_D10_GPIO_CLK();
	__BRD_D11_GPIO_CLK();
	__BRD_D12_GPIO_CLK();

	/* Configure the D10 pin as an output */
	GPIO_InitStructure.Pin = BRD_D10_PIN;				//Pin
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;		//Output Mode
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;//Enable Pull up, down or no pull resister
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;			//Pin latency
	HAL_GPIO_Init(BRD_D10_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D11 pin as an output */
	GPIO_InitStructure.Pin = BRD_D11_PIN;				//Pin
	HAL_GPIO_Init(BRD_D11_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D12 pin as an output */
	GPIO_InitStructure.Pin = BRD_D12_PIN;				//Pin
	HAL_GPIO_Init(BRD_D12_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin
}
