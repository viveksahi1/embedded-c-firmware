/**
 ******************************************************************************
 * @@file    mylib/s4380479_hal_ledbar.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   LED Bar peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * void ledbar_seg_set(int segment, unsigned char segment_value) - toggle ledbar
 * extern void s4380479_hal_ledbar_init(void) - init ledbar pins
 * extern void s4380479_hal_ledbar_write(unsigned short value,
 * 										int mode) - write to ledbar segmetns
 ******************************************************************************
 */

#ifndef S4380479_HAL_LEDBAR_H
#define S4380479_HAL_LEDBAR_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#define MAX_LED 10
/* External function prototypes -----------------------------------------------*/

extern void s4380479_hal_ledbar_init(void);
extern void s4380479_hal_ledbar_write(unsigned short value, int mode);
#endif

