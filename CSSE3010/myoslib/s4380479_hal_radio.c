/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_radio.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   radio peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_hal_radio_init() intialise radio
 * s4380479_hal_radio_fsmprocessing() Radio FSM processing loop
 * s4380479_hal_radio_setchan(unsigned char chan) Set the channel of the radio
 * s4380479_hal_radio_settxaddres(unsigned char *addr) Set the transmit address of the radio
 * s4380479_hal_radio_setrxaddres(unsigned char *addr) Set the receive address of the radio
 * s4380479_hal_radio_getchan() Get the channel of the radio
 * s4380479_hal_radio_gettxaddres(unsigned char *addr) get the transmit address of the radio
 * s4380479_hal_radio_getrxaddres(unsigned char *addr) get the receive address of the radio
 * s4380479_hal_radio_sendpacket(char chan, unsigned char *addr, unsigned char *txpacket) Function to send a packet
 * s4380479_hal_radio_setfsmrx() Set Radio FSM into RX mode.
 * int s4380479_hal_radio_getrxstatus() Function to check when packet is received
 * s4380479_hal_radio_getpacket(unsigned char *rxpacket) Function to receive a packet
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_radio.h"
#include "nrf24l01plus.h"
#include "radio_fsm.h"
#include <string.h>
#include <s4380479_hal_hamming.h>
#include "s4380479_os_printf.h"

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char channel; //radio channel
unsigned char channel_address[5]; //store channel address
unsigned char channel_package[32]; //package to store data
unsigned char s4380479_rx_buffer[32]; //store receiv buffer
unsigned char studentNumber[4] = { 0x91, 0x47, 0x80, 0x43 }; //source addr
int s4380479_hal_radio_rxstatus = 0; // rx status flag
int s4380479_hal_radio_fmscurrentstate; // current fms state
int s4380479_hal_radio_sent = 0; //radio sent flag
int i, error = 0, errorFlag; //err indiacate flags

/**
 * @brief  Initialise radio
 * @param  None
 * @retval None
 */
extern void s4380479_hal_radio_init(void) {

	/* Initialise radio FSM */
	radio_fsm_init();

	/* set radio FSM state to IDLE */
	radio_fsm_setstate(RADIO_FSM_IDLE_STATE);
	s4380479_hal_radio_fmscurrentstate = S4380479_IDLE_STATE;
}

/**
 * @brief  set radio in idle state
 * @param  None
 * @retval None
 */
extern void s4380479_hal_radio_idle(void) {

	/* set radio FSM state to IDLE */
	radio_fsm_setstate(RADIO_FSM_IDLE_STATE);
	s4380479_hal_radio_fmscurrentstate = S4380479_IDLE_STATE;
}

/**
 * @brief  Implements state machine for transceiver
 * @param  None
 * @retval None
 */
extern void s4380479_hal_radio_fsmprocessing(void) {

	switch (s4380479_hal_radio_fmscurrentstate) {

		case S4380479_IDLE_STATE:

			if (radio_fsm_getstate() == RADIO_FSM_IDLE_STATE) {

				if (s4380479_hal_radio_sent == 1) {

					s4380479_hal_radio_sent = 0;
					s4380479_hal_radio_fmscurrentstate = S4380479_TX_STATE;
				} else if (!s4380479_hal_radio_rxstatus) {

					s4380479_hal_radio_fmscurrentstate = S4380479_RX_STATE;	//Set RX state.
				}

			} else {

				/* if error occurs, set state back to IDLE state */
				//debug_printf("ERROR: Radio FSM not in Idle state\n\r");
				radio_fsm_setstate(RADIO_FSM_IDLE_STATE);
			}
			break;

		case S4380479_TX_STATE:	//TX state for writing packet to be sent.

			/* Put radio FSM in TX state, if radio FSM is in IDLE state */
			if (radio_fsm_getstate() == RADIO_FSM_IDLE_STATE) {

				if (radio_fsm_setstate(
				RADIO_FSM_TX_STATE) == RADIO_FSM_INVALIDSTATE) {

					//debug_printf("ERROR: Cannot set Radio FSM RX state\n\r");
					HAL_Delay(100);
				} else {

					radio_fsm_write(channel_package);
					s4380479_hal_radio_fmscurrentstate = S4380479_IDLE_STATE;//set next state as Waiting state
				}
			} else {

				/* if error occurs, set state back to IDLE state */
				//debug_printf("ERROR: Radio FSM not in Idle state\n\r");
				radio_fsm_setstate(RADIO_FSM_IDLE_STATE);
			}
			break;

		case S4380479_RX_STATE:
			// Put radio in RX state if radio FSM is in IDLE or in waiting state
			if ((radio_fsm_getstate() == RADIO_FSM_IDLE_STATE)
					|| (radio_fsm_getstate() == RADIO_FSM_WAIT_STATE)) {

				if (radio_fsm_setstate(
				RADIO_FSM_RX_STATE) == RADIO_FSM_INVALIDSTATE) {

					//debug_printf("ERROR: Cannot set radio FSM to RX state\r\n");
					HAL_Delay(100);
				} else {

					s4380479_hal_radio_fmscurrentstate = S4380479_WAITING_STATE;
				}
			} else {

				// if error occurs set state back to IDLE state
				//debug_printf("ERROR: Radio FSM no in IDLE state\r\n");
				radio_fsm_setstate(RADIO_FSM_IDLE_STATE);
			}
			break;

		case S4380479_WAITING_STATE: // Waiting state for reading received packet
			// Check if radio FSM is in WAITING state
			if (radio_fsm_getstate() == RADIO_FSM_WAIT_STATE) {

				init_s4380479_rx_buffer_buffer();
				// Check for received packet and display
				if (radio_fsm_read(s4380479_rx_buffer) == RADIO_FSM_DONE) {

					errorFlag = 0;
					s4380479_hal_radio_rxstatus = 1;
					for (i = 1; i < 5; i++) {
						if (studentNumber[i - 1] == s4380479_rx_buffer[i]) {

							//do nothing
						} else {

							error = 1;
							break;
						}
					}
					if (error) { //chane this to !error

						//myprintf("%s", "Received from Radio:   ");
						//check for 2-bit errors
						for (i = 10; i < 32; i += 2) {
							if ((s4380479_hal_hamming_decoder(
									(int) s4380479_rx_buffer[i]) << 4
									| s4380479_hal_hamming_decoder(
											(int) s4380479_rx_buffer[i + 1]))
									== -1) {

								//debug_printf("2-bit ERROR\r\n");
								errorFlag = 1;
								break;
							}
						}

						/*	if (!errorFlag) {

						 myprintf("%s", "\"");
						 for (i = 10; i < 32; i += 2) {
						 myprintf("%c",
						 s4380479_hal_hamming_decoder(
						 (int) s4380479_rx_buffer[i])
						 << 4
						 | s4380479_hal_hamming_decoder(
						 (int) s4380479_rx_buffer[i
						 + 1]));
						 }
						 myprintf("%s", "\"\r\n");
						 }*/

						radio_fsm_setstate(RADIO_FSM_IDLE_STATE); // Set radio FSM back to IDLE state
						s4380479_hal_radio_fmscurrentstate =
						S4380479_IDLE_STATE;
					}

				}
				if (s4380479_hal_radio_sent) {

					radio_fsm_setstate(RADIO_FSM_IDLE_STATE); // Set radio FSM back to IDLE state
					s4380479_hal_radio_fmscurrentstate = S4380479_IDLE_STATE;
				}
			}
			break;
	}
}

/**
 * @brief  Sets the radio channel to the gicen channel
 * @param  chan - channel to configure radio
 * @retval None
 */
extern void s4380479_hal_radio_setchan(unsigned char chan) {

	radio_fsm_register_write(NRF24L01P_RF_CH, &chan);
}

/**
 * @brief  Sets the TX adress
 * @param  addr - address of the comms base
 * @retval None
 */
void s4380479_hal_radio_settxaddres(unsigned char *addr) {

	//Power down nrf before powering up.
	radio_fsm_buffer_write(NRF24L01P_TX_ADDR, addr, 5); // Writes TX_Address to nRF24L01
}

/**
 * @brief  Sets the RX adress
 * @param  addr - address of the radio
 * @retval None
 */
extern void s4380479_hal_radio_setrxaddres(unsigned char *addr) {

	nrf24l01plus_wb(NRF24L01P_WRITE_REG | NRF24L01P_RX_ADDR_P0, addr, 5); // Writes TX_Address to nRF24L01
}

/**
 * @brief  Returns current radio channel
 * @param  None
 * @retval None
 */
unsigned char s4380479_hal_radio_getchan(void) {

	uint8_t current_channel;
	radio_fsm_register_read(NRF24L01P_RF_CH, &current_channel);	//Read channel

	return current_channel;
}

/**
 * @brief  gets the TX adress
 * @param  addr - address of the comms base
 * @retval None
 */
extern void s4380479_hal_radio_gettxaddres(unsigned char *addr) {

	radio_fsm_buffer_read(NRF24L01P_TX_ADDR, addr, 5);
}

/**
 * @brief  Gets the RX adress
 * @param  addr - address of the radio
 * @retval None
 */
extern void s4380479_hal_radio_getrxaddres(unsigned char *addr) {

	radio_fsm_buffer_read(NRF24L01P_RX_ADDR_P0, addr, 5);
}

/**
 * @brief  Copies chan, addr and packet info to new variables
 * 		(Does not transmit packet)
 * @param  chan
 * 		current radio channel
 * @param addr
 * 		radio address
 * @param txpacket
 * 		packet to send
 * @retval None
 */
extern void s4380479_hal_radio_sendpacket(char chan, unsigned char *addr,
		unsigned char *txpacket) {

	channel = chan;
	memcpy(channel_address, addr, 5);
	memcpy(channel_package, txpacket, 32);

	s4380479_hal_radio_sent = 1;
}

/**
 * @brief  Sets radio to RX mode
 * @param  None
 * @retval None
 */
extern void s4380479_hal_radio_setfsmrx(void) {

	s4380479_hal_radio_rxstatus = 0;
}

/**
 * @brief  check if radio is in RX mode
 * @param  None
 * @retval rxstatus
 */
int s4380479_hal_radio_getrxstatus(void) {

	return s4380479_hal_radio_rxstatus;
}

/**
 * @brief  copies the retrieved packet to the given rxpacket
 * @param  packet to copy the message to
 * @retval None
 */
extern int s4380479_hal_radio_getpacket(unsigned char *rxpacket) {

	for (i = 0; i < 32; i++) {
		rxpacket[i] = s4380479_rx_buffer[i];
	}
	s4380479_hal_radio_rxstatus = 0;

	return errorFlag;
}

/**
 * @brief  Initialises the rx buffer
 * @param  None
 * @retval None
 */
void init_s4380479_rx_buffer_buffer(void) {

	int i;
	for (i = 0; i < 32; i++) {
		s4380479_rx_buffer[i] = 0;
	}
}

/**
 * Return 1 if given text can be succesfuly converted to a number.
 */
extern int s4380479_radio_is_number(char* text) {
	int i;
	// check for 0 infront of number (eg 03)
	if (text[0] == '0' && text[1] != '\0') {
		return 0;
	}

	// check if each digit is a num
	for (i = 0; i < strlen(text); i++) {
		if (text[i] < '0' || text[i] > '9') {
			return 0;
		}
	}

	return 1;
}
