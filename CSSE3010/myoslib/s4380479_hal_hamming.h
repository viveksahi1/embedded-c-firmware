/**
 ******************************************************************************
 * @@file    mylib/s4380479_hal_hamming.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Hamming encode/decode drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern uint16_t s4380479_hal_error_mask(void); return error mask
 * extern uint16_t s4380479_hal_full_decode(void); return FULL decode
 * extern void s4380479_hal_reset_error_mask(void); reset error mask to 0
 * extern void s4380479_hal_reset_full_decode(void); reset ful decode to 0
 * extern void s4380479_reset_byte_flag(void);	reset msb indicator to 0
 * extern int s4380479_hal_hamming_decoder(void); decode data using hamming decoder
 * extern uint16_t s4380479_hal_hamming_byte_encoder(uint8_t input); encode a complete
 * 																	byte using hammming
 *
 ******************************************************************************
 */

#ifndef S4380479_HAL_HAMMING_H
#define S4380479_HAL_HAMMING_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#define MAX_PAYLOAD 22 //bits
/* External function prototypes -----------------------------------------------*/

extern uint16_t s4380479_hal_hamming_byte_encoder(uint8_t input);
extern int s4380479_hal_hamming_decoder(int in);
uint8_t hex_to_int_converter(uint8_t input_char);
extern uint16_t s4380479_hal_error_mask(void);
extern uint16_t s4380479_hal_full_decode(void);
extern void s4380479_hal_reset_error_mask(void);
extern void s4380479_hal_reset_full_decode(void);
extern void s4380479_reset_byte_flag(void);

#endif

