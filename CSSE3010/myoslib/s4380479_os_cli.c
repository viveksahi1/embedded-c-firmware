/**
 ******************************************************************************
 * @file    mylib/s4380479_os_josystick.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Os joystick drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_os_joystick_init(void) - init joystick
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal_conf.h"
#include "board.h"
#include "debug_printf.h"
#include "s4380479_os_cli.h"
#include "string.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define CLI_TASK_STACK_SIZE		(configMINIMAL_STACK_SIZE * 3)
#define CLI_PRIORITY 			(tskIDLE_PRIORITY + 2)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
void s4380479_CLI_Task(void* param);

/* Init the cli tasks*/
extern void s4380479_os_cli_init() {
	xTaskCreate(s4380479_CLI_Task, "CLI TASK", CLI_TASK_STACK_SIZE,
			NULL, CLI_PRIORITY, NULL);
}

/**
 *  the CLI task
 */
void s4380479_CLI_Task(void* param) {

	int i;
	char cRxedChar;
	char cInputString[100];
	int InputIndex = 0;
	char *pcOutputString;
	BaseType_t xReturned;

	/* Initialise pointer to CLI output buffer. */
	memset(cInputString, 0, sizeof(cInputString));
	pcOutputString = FreeRTOS_CLIGetOutputBuffer();

	for (;;) {

		/* Receive character from terminal */
		cRxedChar = debug_getc();

		/* Process if character if not Null */
		if (cRxedChar != '\0') {

			/* Echo character */
			debug_putc(cRxedChar);

			/* Process only if return is received. */
			if (cRxedChar == '\r') {

				//Put new line and transmit buffer
				debug_putc('\n');
				debug_flush();

				/* Put null character in command input string. */
				cInputString[InputIndex] = '\0';

				xReturned = pdTRUE;
				/* Process command input string. */
				while (xReturned != pdFALSE) {

					/* Returns pdFALSE, when all strings have been returned */
					xReturned = FreeRTOS_CLIProcessCommand(cInputString,
							pcOutputString, configCOMMAND_INT_MAX_OUTPUT_SIZE);

					/* Display CLI command output string (not thread safe) */
					portENTER_CRITICAL();
					for (i = 0; i < strlen(pcOutputString); i++) {
						debug_putc(*(pcOutputString + i));
					}
					portEXIT_CRITICAL();

					vTaskDelay(5);	//Must delay between debug_printfs.
				}

				memset(cInputString, 0, sizeof(cInputString));
				InputIndex = 0;

			} else {

				debug_flush();		//Transmit USB buffer

				if (cRxedChar == '\r') {

					/* Ignore the character. */
				} else if (cRxedChar == '\b') {

					/* Backspace was pressed.  Erase the last character in the
					 string - if any.*/
					if (InputIndex > 0) {
						InputIndex--;
						cInputString[InputIndex] = '\0';
					}

				} else {

					/* A character was entered.  Add it to the string
					 entered so far.  When a \n is entered the complete
					 string will be passed to the command interpreter. */
					if (InputIndex < INDEX_SIZE) {
						cInputString[InputIndex] = cRxedChar;
						InputIndex++;
					}
				}
			}
		}

		vTaskDelay(50);
	}
}

