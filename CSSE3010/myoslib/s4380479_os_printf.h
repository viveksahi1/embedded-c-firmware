/**
 ******************************************************************************
6* @file    mylib/s4380479_os_printf.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   OS printf drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPrintf(void) - task to receive from printf
 ******************************************************************************
 */

#ifndef S4380479_OS_PRINTF_H
#define S4380479_OS_PRINTF_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define myprintf(fmt, args...)		s4380479_os_printf(fmt, args)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
QueueHandle_t s4380479_QueuePrintf;
SemaphoreHandle_t s4380479_SemaphorePrintf;

/* External function prototypes -----------------------------------------------*/
extern void s4380479_os_printf_init(void);
void s4380479_os_printf(char* fmt, ...);

#endif

