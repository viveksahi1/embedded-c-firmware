/**
 ******************************************************************************
6 * @@file    mylib/s4380479_hal_ir_coms.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Ir receiver drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_manchester_encode(uint8_t msb, uint8_t lsb,
		int encode[], int mode) manchester encodes hex byte input

*  extern int s4380479_hal_manchester_decode(uint8_t input[], int size) manchest
*  	decode a hex input
 ******************************************************************************
 */

#ifndef S4380479_HAL_IR_COMS_H
#define S4380479_HAL_IR_COMS_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define NUM_ENCODE_BITS 8
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_hal_manchester_encode(uint8_t msb,
		uint8_t lsb, int encode[], int mode);
extern int s4380479_hal_manchester_decode(uint8_t input[], int size);

#endif

