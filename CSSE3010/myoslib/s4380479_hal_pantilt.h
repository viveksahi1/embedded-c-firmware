/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_pantilt.h
 * @author  Vivek Sahi (43804791)
 * @date    08032018
 * @brief   Pan Tilt driver
 *
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_hal_pantilt_init(void) - intialise PWM
 ******************************************************************************
 */

#ifndef S4380479_HAL_PANTILT_H
#define S4380479_HAL_PANTILT_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include "stdio.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/

#define PWM_CLOCKFREQ		50000
#define PWM_PULSEPERIOD		1.45 * (PWM_CLOCKFREQ/1000)
#define PWM_PERIOD			20*PWM_CLOCKFREQ/1000 //2*PWM_CLOCKFREQ/10 = 20ms
#define PAN_MODE 			0
#define TILT_MODE			1
#define PAN_CHANNEL			TIM_CHANNEL_1 //change to tim_1
#define TILT_CHANNEL		TIM_CHANNEL_2 //change to tim_1
#define PAN_PIN				BRD_D6_PIN
#define TILT_PIN			BRD_D5_PIN
#define PWM_TIMER			TIM1
#define PWM_GPIO_AF			GPIO_AF1_TIM1
#define PWM_PIN_CLK()		__TIM1_CLK_ENABLE()
#define PWM_TIMER_HANDLER	TIM_Init_pan_tilt
#define PAN_DC_GET() 		__HAL_TIM_GET_COMPARE(&PWM_TIMER_HANDLER, PAN_CHANNEL)
#define PAN_DC_SET(value) 	__HAL_TIM_SET_COMPARE(&PWM_TIMER_HANDLER, PAN_CHANNEL, value)
#define TILT_DC_GET() 		__HAL_TIM_GET_COMPARE(&PWM_TIMER_HANDLER, TILT_CHANNEL)
#define TILT_DC_SET(value) 	__HAL_TIM_SET_COMPARE(&PWM_TIMER_HANDLER, TILT_CHANNEL, value)

#define s4380479_hal_pantilt_pan_write(angle)	pantilt_angle_write(PAN_MODE, angle)
#define s4380479_hal_pantilt_pan_read() 		pantilt_angle_read(PAN_MODE)
#define s4380479_hal_pantilt_tilt_write(angle) 	pantilt_angle_write(TILT_MODE, angle)
#define s4380479_hal_pantilt_tilt_read() 		pantilt_angle_read(TILT_MODE)

TIM_HandleTypeDef TIM_Init_pan_tilt; // timer handler

void pantilt_angle_write(int type, int angle);
int pantilt_angle_read(int type);
void pantilt_metronome(int mode);
extern void s4380479_hal_pantilt_init(void);
void update_ledbar(int angle, int mode);

#endif

