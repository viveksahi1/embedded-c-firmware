/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_hamming.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Hamming encode/decode functions
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *	extern uint16_t s4380479_hal_error_mask(void); return error mask
 *	extern uint16_t s4380479_hal_full_decode(void); return FULL decode
 *	extern void s4380479_hal_reset_error_mask(void); reset error mask to 0
 *	extern void s4380479_hal_reset_full_decode(void); reset ful decode to 0
 *	extern void s4380479_reset_byte_flag(void);	reset msb indicator to 0
 *	extern int s4380479_hal_hamming_decoder(void); decode data using hamming decoder
 *	extern uint16_t s4380479_hal_hamming_byte_encoder(uint8_t input); encode a complete byteusing hammming
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <s4380479_hal_ledbar.h>
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <s4380479_hal_hamming.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint16_t errorMask = 0; // Hamming decode error mask
uint16_t fullDecode = 0; // hamming decode FULL
int firstByte = 0; //msb decode byte indicator

/**
 * This function return the 2 byte hamming error_mask
 */
extern uint16_t s4380479_hal_error_mask(void) {

	return errorMask;
}

/**
 * This function return the 2 byte hamming FULL Decode
 */
extern uint16_t s4380479_hal_full_decode(void) {

	return fullDecode;
}

/**
 * This function sets error_mask to 0
 */
extern void s4380479_hal_reset_error_mask(void) {

	errorMask = 0;
}

/**
 * This function sets Full Decode to 0
 */
extern void s4380479_hal_reset_full_decode(void) {

	fullDecode = 0;
}

/**
 * This function sets msb byte indicator to 0
 */
extern void s4380479_reset_byte_flag(void) {

	firstByte = 0;
}

/**
 * Implement Hamming Code + parity checking
 * Hamming code is based on the following generator and parity check matrices
 * G = [ 0 1 1 | 1 0 0 0 ;
 *       1 0 1 | 0 1 0 0 ;
 *       1 1 0 | 0 0 1 0 ;
 *       1 1 1 | 0 0 0 1 ;
 *
 * hence H =
 * [ 1 0 0 | 0 1 1 1 ;
 *   0 1 0 | 1 0 1 1 ;
 *   0 0 1 | 1 1 0 1 ];
 *
 * y = x * G, syn = H * y'
 *
 *
 * NOTE: !! is used to get 1 out of non zeros
 */
uint8_t hamming_byte_encoder(uint8_t in) {

	uint8_t d0, d1, d2, d3;
	uint8_t p0 = 0, h0, h1, h2;
	uint8_t z;
	uint8_t out;

	/* extract bits */
	d0 = !!(in & 0x1);
	d1 = !!(in & 0x2);
	d2 = !!(in & 0x4);
	d3 = !!(in & 0x8);

	/* calculate hamming parity bits */
	h0 = d0 ^ d1 ^ d2;
	h1 = d0 ^ d2 ^ d3;
	h2 = d0 ^ d1 ^ d3;

	/* generate out byte without parity bit P0 */
	out = (h0 << 1) | (h1 << 2) | (h2 << 3) | (d0 << 4) | (d1 << 5) | (d2 << 6)
			| (d3 << 7);

	/* calculate even parity bit */
	for (z = 1; z < 8; z++)
		p0 = p0 ^ !!(out & (1 << z));

	out |= p0;

	return (out);
}

/**
 * Implement Hamming Code on a full byte of input
 * This means that 16-bits out output is needed
 *
 * @param takes a 1 byte work and encodes it using hamming matrix
 *
 * @return 2 byte hamming encoded word
 */
extern uint16_t s4380479_hal_hamming_byte_encoder(uint8_t input) {

	uint16_t out;
	/* first encode D0..D3 (first 4 bits),
	 * then D4..D7 (second 4 bits).
	 */
	out = hamming_byte_encoder(input & 0xF)
			| (hamming_byte_encoder(input >> 4) << 8);

	return (out);
}

/**
 * This function takes a complete byte and decode using hamming matrix
 *
 * @param byte to encode
 *
 * @return int value decode using hamming matrix
 */
extern int s4380479_hal_hamming_decoder(int in) {

	uint8_t h0, h1, h2, d0, d1, d2, d3, p0, parity_bit, s0, s1, s2;
	int errorbit;
	uint16_t decoded_input = 0;
	uint8_t decoded;

	/* extract bits */
	p0 = !!(in & (1 << 0));
	h0 = !!(in & (1 << 1));
	h1 = !!(in & (1 << 2));
	h2 = !!(in & (1 << 3));
	d0 = !!(in & (1 << 4));
	d1 = !!(in & (1 << 5));
	d2 = !!(in & (1 << 6));
	d3 = !!(in & (1 << 7));

	//get decoded input
	decoded_input |= (p0 << 0) | (h0 << 1) | (h1 << 2) | (h2 << 3) | (d0 << 4)
			| (d1 << 5) | (d2 << 6) | (d3 << 7);
	//syndrome bits
	s0 = h0 ^ d0 ^ d1 ^ d2;
	s1 = h1 ^ d0 ^ d2 ^ d3;
	s2 = h2 ^ d0 ^ d1 ^ d3;

	/* error bit and parity bit */
	errorbit = s0 | s1 << 1 | s2 << 2;
	parity_bit = h0 ^ h1 ^ h2 ^ d0 ^ d1 ^ d2 ^ d3;

	//flip error bit
	if (errorbit) {

		//check if 2 or more erros exist
		if (parity_bit == p0) {

			return -1;
		} else { //find and invert error bit

			switch (errorbit) {

				case 1: //1 0 0

					h0 = !h0;
					break;

				case 2: //0 1 0

					h1 = !h1;
					break;

				case 3: //0 1 1

					d2 = !d2;
					break;

				case 4: //1 0 0

					h2 = !h2;
					break;

				case 5: //1 0 1

					d1 = !d1;
					break;

				case 6: //0 1 1

					d3 = !d3;
					break;

				case 7: //1 1 1

					d0 = !d0;
					break;
			}
		}
	} else if (parity_bit != p0) {

		p0 = parity_bit;
	}
	decoded = (p0 << 0) | (h0 << 1) | (h1 << 2) | (h2 << 3) | (d0 << 4)
			| (d1 << 5) | (d2 << 6) | (d3 << 7);
	errorMask |= decoded_input ^ decoded;

	if (!firstByte) {

		errorMask = errorMask << 8;
		fullDecode |= decoded << 8;
		firstByte = 1;
	} else {

		firstByte = 0;
		fullDecode |= decoded;
	}

	return d3 << 3 | d2 << 2 | d1 << 1 | d0;
}

/**
 * This function takes a 1 byte char and convert it into int value
 *
 * @param hex digit to convet to int
 *
 * @param int value
 */
uint8_t hex_to_int_converter(uint8_t input_char) {

	switch (input_char) {

		case '0':

			input_char = 0;
			break;

		case '1':

			input_char = 1;
			break;

		case '2':

			input_char = 2;
			break;

		case '3':

			input_char = 3;
			break;

		case '4':

			input_char = 4;
			break;

		case '5':

			input_char = 5;
			break;

		case '6':

			input_char = 6;
			break;

		case '7':

			input_char = 7;
			break;

		case '8':

			input_char = 8;
			break;

		case '9':

			input_char = 9;
			break;

		case 'A':

			input_char = 10;
			break;

		case 'B':

			input_char = 11;
			break;

		case 'C':

			input_char = 12;
			break;

		case 'D':

			input_char = 13;
			break;

		case 'E':

			input_char = 14;
			break;

		case 'F':

			input_char = 15;
			break;

		default:
			break;
	}

	return input_char;
}

