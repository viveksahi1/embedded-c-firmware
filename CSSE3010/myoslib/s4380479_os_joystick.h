/**
 ******************************************************************************
6 * @@file    mylib/s4380479_os_joystick.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   OS joystick drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_os_joystick_init(void) - init joystick
 ******************************************************************************
 */

#ifndef S4380479_OS_JOYSTICK_H
#define S4380479_OS_JOYSTICK_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_os_joystick_init(void);

#endif

