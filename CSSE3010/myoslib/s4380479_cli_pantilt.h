/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_pantilt.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   OS joystick drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_CLI_PANTILT_H
#define S4380479_CLI_PANTILT_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
#include "s4380479_os_cli.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
static BaseType_t prvPanCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvTiltCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
extern s4380479_cli_pantilt_init(void);

extern CLI_Command_Definition_t xPan;

extern CLI_Command_Definition_t xTilt;

#endif

