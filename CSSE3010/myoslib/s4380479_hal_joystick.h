/**
 ******************************************************************************
 * @@file    mylib/s4380479_hal_joystick.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   joystick peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_joystick_init(void) -initialise joystick
 * int joystick_read(ADC_HandleTypeDef AdcHandle) - reads joystick vcalue
 * int joystick_value(int adc) - convert adc to joystick value
 ******************************************************************************
 */

#ifndef S4380479_HAL_JOYSTICK_H
#define S4380479_HAL_JOYSTICK_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define s4380479_hal_joystick_x_read() joystick_value(joystick_read(AdcHandle_x))
#define s4380479_hal_joystick_y_read() joystick_value(joystick_read(AdcHandle_y))
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
ADC_HandleTypeDef AdcHandle_x; //handler for X value
ADC_HandleTypeDef AdcHandle_y; //handler for y value

extern void s4380479_hal_joystick_init(void);
int joystick_read(ADC_HandleTypeDef AdcHandle);
extern int joystick_value(int adc); // adc to voltage converter

#endif

