/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_ledbar.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   LED Bar peripheral driver
 *	     REFERENCE: LEDledbar_datasheet.pdf
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * void ledbar_seg_set(int segment, unsigned char segment_value) - toggle ledbar
 * extern void s4380479_hal_ledbar_init(void) - init ledbar pins
 * extern void s4380479_hal_ledbar_write(unsigned short value,
 * 										int mode) - write to ledbar segmetns
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <s4380479_hal_ledbar.h>
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
 * function to toggle ledbar pins for given segment to segment_value
 *
 * @param segment
 * 		ledbar segment
 *
 * @param segment_value
 * 		value to set ledbar segment
 */
void ledbar_seg_set(int segment, unsigned char segment_value) {

	/*
	 Turn segment on (segment_value = 1) or off (segment_value = 0)

	 */
	if (segment == 0) {

		HAL_GPIO_WritePin(BRD_D25_GPIO_PORT, BRD_D25_PIN, segment_value & 0x01);
	} else if (segment == 1) {

		HAL_GPIO_WritePin(BRD_D24_GPIO_PORT, BRD_D24_PIN, segment_value & 0x01);
	} else if (segment == 2) {

		HAL_GPIO_WritePin(BRD_D23_GPIO_PORT, BRD_D23_PIN, segment_value & 0x01);
	} else if (segment == 3) {

		HAL_GPIO_WritePin(BRD_D22_GPIO_PORT, BRD_D22_PIN, segment_value & 0x01);
	} else if (segment == 4) {

		HAL_GPIO_WritePin(BRD_D21_GPIO_PORT, BRD_D21_PIN, segment_value & 0x01);
	} else if (segment == 5) {

		HAL_GPIO_WritePin(BRD_D20_GPIO_PORT, BRD_D20_PIN, segment_value & 0x01);
	} else if (segment == 6) {

		//do nothing D19 is reserved for radio
	} else if (segment == 7) {

		HAL_GPIO_WritePin(BRD_D18_GPIO_PORT, BRD_D18_PIN, segment_value & 0x01);
	} else if (segment == 8) {

		HAL_GPIO_WritePin(BRD_D17_GPIO_PORT, BRD_D17_PIN, segment_value & 0x01);
	} else if (segment == 9) {

		HAL_GPIO_WritePin(BRD_D16_GPIO_PORT, BRD_D16_PIN, segment_value & 0x01);
	}
}

/**
 * @brief  Initialise LEDBar.
 * @param  None
 * @retval None
 */
extern void s4380479_hal_ledbar_init(void) {
	/* inti ledbar gpio pins*/
	__BRD_D25_GPIO_CLK();
	__BRD_D24_GPIO_CLK();
	__BRD_D23_GPIO_CLK();
	__BRD_D22_GPIO_CLK();
	__BRD_D21_GPIO_CLK();
	__BRD_D20_GPIO_CLK();
	__BRD_D18_GPIO_CLK();
	__BRD_D17_GPIO_CLK();
	__BRD_D16_GPIO_CLK();

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;		//Output Mode
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;//Enable Pull up, down or no pull resister
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;			//Pin latency

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D25_PIN;				//Pin
	HAL_GPIO_Init(BRD_D25_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D24_PIN;				//Pin
	HAL_GPIO_Init(BRD_D24_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D23_PIN;				//Pin
	HAL_GPIO_Init(BRD_D23_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D22_PIN;				//Pin
	HAL_GPIO_Init(BRD_D22_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D21_PIN;				//Pin
	HAL_GPIO_Init(BRD_D21_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D20_PIN;				//Pin
	HAL_GPIO_Init(BRD_D20_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D18_PIN;				//Pin
	HAL_GPIO_Init(BRD_D18_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D17_PIN;				//Pin
	HAL_GPIO_Init(BRD_D17_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

	/* Configure the D0 pin as an output */
	GPIO_InitStructure.Pin = BRD_D16_PIN;				//Pin
	HAL_GPIO_Init(BRD_D16_GPIO_PORT, &GPIO_InitStructure);	//Initialise Pin

}

/**
 * @brief  Set the LED Bar GPIO pins high or low, depending on the bit of �value�
 *         i.e. value bit 0 is 1 � LED Bar 0 on
 *          value bit 1 is 1 � LED BAR 1 on
 *
 * @param  value
 * @retval None
 */
extern void s4380479_hal_ledbar_write(unsigned short value, int mode) {

	/* Use bit shifts (<< or >>) and bit masks (1 << bit_index) to determine if a bit is set

	 e.g. The following pseudo code checks if bit 0 of value is 1.
	 if ((value & (1 << 0)) == (1 << 0))	{
	 Turn on LED BAR Segment 0.
	 }
	 */
	for (int i = 0; i < MAX_LED; i++) {
		if ((value & (1 << i)) == (1 << i)) {
			ledbar_seg_set(i, mode);
		}
	}

}

