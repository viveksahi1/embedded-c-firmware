/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_ir.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   IR peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * sxxxxxx_hal_ir_init() -initialise IR receiver/transmitter
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <s4380479_hal_ir.h>
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <s4380479_hal_hamming.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
 * @brief  Initialise IR transmitter/receiver.
 * @param  None
 * @retval None
 */
extern void s4380479_hal_ir_init(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_OC_InitTypeDef PWMConfig;

	__BRD_D38_GPIO_CLK()
	;
	///// data wave
	GPIO_InitStructure.Pin = BRD_D38_PIN;				//Pin
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;			//Pin latency
	HAL_GPIO_Init(BRD_D38_GPIO_PORT, &GPIO_InitStructure);

	// Timer 2 clock enable
	CARRIER_PIN_CLK();
	__BRD_D35_GPIO_CLK();

	//configure gpio D6 pin for carriere wave
	GPIO_InitStructure.Pin = CARRIER_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Alternate = CARRIER_GPIO_AF;
	HAL_GPIO_Init(BRD_D35_GPIO_PORT, &GPIO_InitStructure);

	TIM_Init.Instance = CARRIER_TIMER;
	TIM_Init.Init.Period = CARRIER_PERIOD;
	TIM_Init.Init.Prescaler = 0;
	TIM_Init.Init.ClockDivision = 0;
	TIM_Init.Init.RepetitionCounter = 0;
	TIM_Init.Init.CounterMode = TIM_COUNTERMODE_UP;

	//CARRIER configuration
	PWMConfig.OCMode = TIM_OCMODE_PWM2;
	PWMConfig.Pulse = CARRIER_PULSEPERIOD;
	PWMConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	PWMConfig.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	PWMConfig.OCFastMode = TIM_OCFAST_DISABLE;
	PWMConfig.OCIdleState = TIM_OCIDLESTATE_RESET;
	PWMConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;

	//STart PWM
	HAL_TIM_PWM_Init(&TIM_Init);
	HAL_TIM_PWM_ConfigChannel(&TIM_Init, &PWMConfig, CARRIER_CHANNEL);
	HAL_TIM_PWM_Start(&TIM_Init, CARRIER_CHANNEL);
}

/**
 * @brief  turns the carrier wave on or off
 * @param  state
 * @retval None
 */

void irhal_carrier(int state) {

	if (state) {
		PWM_DC_SET(197);
	} else {
		PWM_DC_SET(0);
	}
}

extern void s4380479_hal_ir_transmit(int data[]) {
	s4380479_hal_ir_carrieron();
	//set data modulation based on each bit
	for (int i = 0; i < 22; i++) {
		if (data[i]) {
			s4380479_hal_ir_datamodulation_set();
		} else {
			s4380479_hal_ir_datamodulation_clr();
		}
		HAL_Delay(5);
	}
	//CLEAR DATA MODULATION AND CARRIER
	s4380479_hal_ir_carrieroff();
	s4380479_hal_ir_datamodulation_clr();
}

void init_s4380479_msg_buffer(void) {
	int i;
	for (i = 0; i < 12; i++) {
		IR_message_buffer[i] = 0;
	}
}
