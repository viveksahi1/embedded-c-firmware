/**
 ******************************************************************************
6 * @@file    mylib/s4380479_os_cli.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   OS joystick drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_os_cli_init() - init cli interface
 ******************************************************************************
 */

#ifndef S4380479_OS_CLI_H
#define S4380479_OS_CLI_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#define INDEX_SIZE 50

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_os_cli_init();

#endif

