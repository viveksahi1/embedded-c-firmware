/**
 ******************************************************************************
6 * @file    mylib/s4380479_os_pantilt.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   pantilt os drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPanTilt(void) - Task to control the pan and tilt,
 * 											and laser
 ******************************************************************************
 */

#ifndef S4380479_OS_PANTILT_H
#define S4380479_OS_PANTILT_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"



/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
QueueHandle_t s4387479_QueuePan;
QueueHandle_t s4387479_QueueTilt;
SemaphoreHandle_t s4387479_SemaphorePanLeft;
SemaphoreHandle_t s4387479_SemaphorePanRight;
SemaphoreHandle_t s4387479_SemaphoreTiltUp;
SemaphoreHandle_t s4387479_SemaphoreTiltDown;

struct PanMessage {
	int angle;
};

struct TiltMessage {
	int angle;
};

/* External function prototypes -----------------------------------------------*/
extern void s4380479_os_pantilt_init(void);

#endif

