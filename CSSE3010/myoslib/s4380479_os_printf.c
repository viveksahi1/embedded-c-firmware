/**
 ******************************************************************************
 * @file    mylib/s4380479_os_printf.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   OS printf drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPrintf(void) - task to receive from printf
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include "s4380479_os_printf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define buffer 						200
#define PRINT_TASK_STACK_SIZE		(configMINIMAL_STACK_SIZE * 4)
#define PRINT_PRIORITY 				(tskIDLE_PRIORITY + 2)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char dataToPrint[200];

/**
 * Initialises the pins used for the system monitor
 */
void s4380479_TaskPrintf(void) {

	while (1) {

		if (s4380479_QueuePrintf != NULL) {

			char dataToPrint[200];
			if (xQueueReceive(s4380479_QueuePrintf, &dataToPrint, 1)) {

					debug_printf("%s", &dataToPrint);
			}

		}
	}
	vTaskDelay(1);

}

/** task safe printf function */
void s4380479_os_printf(char* fmt, ...) {

	va_list args;
	va_start(args, fmt);
	vsprintf(&dataToPrint, fmt, args);
	va_end(args);

	//s4380479_QueuePrintf = xQueueCreate(10, sizeof(buffer));
	if (xQueueSend(s4380479_QueuePrintf, dataToPrint,
			( portTickType ) 10) != pdPASS) {
		//failed
	}

}

extern void s4380479_os_printf_init(void) {
	//init semaphores and queues
	//s4380479_SemaphorePrintf = xSemaphoreCreateBinary();
	s4380479_QueuePrintf = xQueueCreate(10, sizeof(char)*200);

	xTaskCreate(&s4380479_TaskPrintf, "PRINTF", PRINT_TASK_STACK_SIZE, NULL,
	PRINT_PRIORITY, NULL);

}

