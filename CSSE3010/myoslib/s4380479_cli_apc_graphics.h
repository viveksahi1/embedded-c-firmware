/**
 ******************************************************************************
 * @file    mylib/s4380479_cli_apc_graphics.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   apc graphics commands drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_cli_apc_graphics_init(void); - init apc shell commands
 ******************************************************************************
 */

#ifndef S4380479_CLI_APC_GRAPHICS_H
#define S4380479_CLI_APC_GRAPHICS_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
#include "s4380479_os_printf.h"
#include "FreeRTOS_CLI.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_cli_apc_graphics_init(void);

static BaseType_t prvOrigin(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvLine(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvSquare(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvBline(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

extern CLI_Command_Definition_t xOrigin;
extern CLI_Command_Definition_t xLine;
extern CLI_Command_Definition_t xSquare;
extern CLI_Command_Definition_t xBline;

SemaphoreHandle_t s4380479_SemaphoreOrigin;
SemaphoreHandle_t s4380479_SemaphoreHline;
SemaphoreHandle_t s4380479_SemaphoreVline;

struct DrawLine {
	int x;
	int y;
	char type;
	int length;
};

#endif

