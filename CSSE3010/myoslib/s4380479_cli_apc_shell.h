/**
 ******************************************************************************
 * @file    mylib/s4380479_cli_apc_shell.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   apc shell commands drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_cli_apc_shell_init(void); - init apc shell commands
 ******************************************************************************
 */

#ifndef S4380479_CLI_APC_SHELL_H
#define S4380479_CLI_APC_SHELL_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
#include "s4380479_os_printf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_cli_apc_shell_init(void);

static BaseType_t prvGetSysCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvSetChanCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvGetChanCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvSetTxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvSetRxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvGetTxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvGetRxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvJoinCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvMoveCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvPenCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvStatusCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvDeleteCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
static BaseType_t prvCreateCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

extern CLI_Command_Definition_t xGetSys;
extern CLI_Command_Definition_t xSetChan;
extern CLI_Command_Definition_t xGetChan;
extern CLI_Command_Definition_t xSetTxAddr;
extern CLI_Command_Definition_t xSetRxAddr;
extern CLI_Command_Definition_t xGetTxAddr;
extern CLI_Command_Definition_t xGetRxAddr;
extern CLI_Command_Definition_t xJoin;
extern CLI_Command_Definition_t xMove;
extern CLI_Command_Definition_t xPen;
extern CLI_Command_Definition_t xStatus;
extern CLI_Command_Definition_t xDelete;
extern CLI_Command_Definition_t xCreate;

#endif

