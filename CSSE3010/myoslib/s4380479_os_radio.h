/**
 ******************************************************************************
6 * @file    mylib/s4380479_os_radio.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   pantilt os drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_OS_RADIO_H
#define S4380479_OS_RADIO_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "s4380479_hal_radio.h"
#include "s4380479_cli_apc_graphics.h"
#include "s4380479_os_pantilt.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SET_CHAN_MODE		0
#define SET_TX_MODE			1
#define SET_RX_MODE			2
#define JOIN_CMD			3
#define MOVE_CMD			4
#define PEN_CMD				5
#define MAX_ADDR_LENGTH		100
#define PLOTTER_ADDR_SIZE	7
#define Z_PEN_UP			0
#define Z_PEN_DOWN			50
#define ACK_MESSAGE			1
#define ERR_MESSAGE			2
#define MAX_BYTES			22
#define GAVE_UP				3
#define ACK_TIMEOUT			3000
#define RESET_DELIVER		0

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
QueueHandle_t s4380479_QueueRadio;
QueueHandle_t s4380479_QueueMove;
QueueHandle_t s4380479_QueueLine;

SemaphoreHandle_t s4380479_SemaphoreGetChan;
SemaphoreHandle_t s4380479_SemaphoreGetTx;
SemaphoreHandle_t s4380479_SemaphoreGetRx;
SemaphoreHandle_t s4380479_SemaphoreJoin;
SemaphoreHandle_t s4380479_SemaphorePenUp;
SemaphoreHandle_t s4380479_SemaphorePenDown;
SemaphoreHandle_t s4380479_SemaphoreReceive;
SemaphoreHandle_t s4380479_SemaphoreAck;

struct RadioMsg {

	int mode;
	int chan;
	char addr[100];
};

struct RadioMove {

	int x;
	int y;
};

struct RadioReceive {

	char radioMsg[100];
};

/* External function prototypes -----------------------------------------------*/
extern void s4380479_os_radio_init(void);
int draw_line(int x, int y, char type, int length);
int move_position(int x, int y);
void draw_bline(int x1, int y1, int x2, int y2, int step);

#endif

