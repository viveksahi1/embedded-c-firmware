/**
 ******************************************************************************

6 * @@file    mylib/s4380479_hal_sysmon.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   System monitor drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_sysmon_init(void) - init LA pins
 ******************************************************************************
 */

#ifndef S4380479_HAL_SYSMON_H
#define S4380479_HAL_SYSMON_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define s4380479_hal_sysmon_chan0_clr()		HAL_GPIO_WritePin(BRD_D10_GPIO_PORT, BRD_D10_PIN, 0)
#define s4380479_hal_sysmon_chan0_set()		HAL_GPIO_WritePin(BRD_D10_GPIO_PORT, BRD_D10_PIN, 1)

#define s4380479_hal_sysmon_chan1_clr()		HAL_GPIO_WritePin(BRD_D11_GPIO_PORT, BRD_D11_PIN, 0)
#define s4380479_hal_sysmon_chan1_set()		HAL_GPIO_WritePin(BRD_D11_GPIO_PORT, BRD_D11_PIN, 1)

#define s4380479_hal_sysmon_chan2_clr()		HAL_GPIO_WritePin(BRD_D12_GPIO_PORT, BRD_D12_PIN, 0)
#define s4380479_hal_sysmon_chan2_set()		HAL_GPIO_WritePin(BRD_D12_GPIO_PORT, BRD_D12_PIN, 1)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_hal_sysmon_init(void);

#endif

