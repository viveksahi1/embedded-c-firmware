/**
 ******************************************************************************
 * @file    mylib/s4380479_cli_pantilt.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Os joystick drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "FreeRTOS_CLI.h"
#include "s4380479_cli_pantilt.h"
#include "s4380479_os_pantilt.h"
#include "string.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
CLI_Command_Definition_t xPan = {	/* Structure that defines the "echo" command line command. */
	"pan",
	"pan: <direction> / <Angle>\r\n",
	prvPanCommand,
	1
};

CLI_Command_Definition_t xTilt = {	/* Structure that defines the "echo" command line command. */
	"tilt",
	"tilt: <direction> / <Angle>\r\n",
	prvTiltCommand,
	1
};


/**
 * @brief  Echo Command.
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvPanCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;
	int angle;
	struct PanMessage msg;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s\n\r",
			cCmd_string);

	if (!(strcmp(cCmd_string, "0"))) { // check for negative angle
		msg.angle = atoi(cCmd_string);
		s4387479_QueuePan = xQueueCreate(10, sizeof(msg));
		msg.angle = atoi(cCmd_string);
		if (xQueueSend(s4387479_QueuePan, (void *) &msg,
				( portTickType ) 10) != pdPASS) {
			//failed
		}
	} else {
		if (atoi(cCmd_string) == 0) { //cmd not an integer
			if (!(strcmp(cCmd_string, "left"))) {
				xSemaphoreGive(s4387479_SemaphorePanLeft);

			} else if (!(strcmp(cCmd_string, "right"))) {
				xSemaphoreGive(s4387479_SemaphorePanRight);
			}

		} else { //positive angle in cmd
			msg.angle = atoi(cCmd_string);
			s4387479_QueuePan = xQueueCreate(10, sizeof(msg));
			if (xQueueSendToFront(s4387479_QueuePan, (void *) &msg,
					( portTickType ) 10) != pdPASS) {
				//failed
			}

		}
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Echo Command.
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvTiltCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;
	int angle;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s\n\r",
			cCmd_string);
	struct TiltMessage msg;

	if (!(strcmp(cCmd_string, "0"))) { // check for negative angle
		msg.angle = atoi(cCmd_string);
		s4387479_QueueTilt = xQueueCreate(10, sizeof(msg));
		if (xQueueSendToFront(s4387479_QueueTilt, (void *) &msg,
				( portTickType ) 10) != pdPASS) {
			//failed
		}
	} else {
		if (atoi(cCmd_string) == 0) { //cmd not an integer
			if (!(strcmp(cCmd_string, "up"))) {
				xSemaphoreGive(s4387479_SemaphoreTiltUp);

			} else if (!(strcmp(cCmd_string, "down"))) {
				xSemaphoreGive(s4387479_SemaphoreTiltDown);
			}

		} else { //positive angle in cmd
			msg.angle = atoi(cCmd_string);
			s4387479_QueueTilt = xQueueCreate(10, sizeof(msg));
			if (xQueueSendToFront(s4387479_QueueTilt, (void *) &msg,
					( portTickType ) 10) != pdPASS) {
				//failed
			}

		}
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

extern s4380479_cli_pantilt_init(void) {
	FreeRTOS_CLIRegisterCommand(&xPan);
	FreeRTOS_CLIRegisterCommand(&xTilt);
}

