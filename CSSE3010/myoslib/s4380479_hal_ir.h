/**
 ******************************************************************************
6 * @@file    mylib/s4380479_hal_ir.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   joystick peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_ir_init(void) - initialises IR
 *
 ******************************************************************************
 */

#ifndef S4380479_HAL_IR_H
#define S4380479_HAL_IR_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdlib.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
unsigned char IR_message_buffer[12];
/* External function prototypes -----------------------------------------------*/

#define s4380479_hal_ir_carrieron() irhal_carrier(1)
#define s4380479_hal_ir_carrieroff() irhal_carrier(0)
#define s4380479_hal_ir_datamodulation_set() HAL_GPIO_WritePin(BRD_D38_GPIO_PORT, BRD_D38_PIN, 1)
#define s4380479_hal_ir_datamodulation_clr() HAL_GPIO_WritePin(BRD_D38_GPIO_PORT, BRD_D38_PIN, 0)
#define CARRIER_CLOCKFREQ		15000000
#define CARRIER_PULSEPERIOD		0.0131578 *(CARRIER_CLOCKFREQ/1000)
#define CARRIER_PERIOD			0.026315 * CARRIER_CLOCKFREQ/1000 //2*CARRIER_CLOCKFREQ/10 = 20ms
#define CARRIER_CHANNEL			TIM_CHANNEL_4 //change to tim_1
#define CARRIER_PIN				BRD_D35_PIN
#define CARRIER_TIMER			TIM2
#define CARRIER_GPIO_AF			GPIO_AF1_TIM2
#define CARRIER_PIN_CLK()		__TIM2_CLK_ENABLE()
#define CARRIER_TIMER_HANDLER	TIM_Init
#define PWM_DC_GET() 		__HAL_TIM_GET_COMPARE(&CARRIER_TIMER_HANDLER, CARRIER_CHANNEL)
#define PWM_DC_SET(value) 	__HAL_TIM_SET_COMPARE(&CARRIER_TIMER_HANDLER, CARRIER_CHANNEL, value)

TIM_HandleTypeDef TIM_Init;

extern void s4380479_hal_ir_init(void);
void irhal_carrier(int state);
void set_period(int value);
extern void s4380479_hal_ir_transmit(int data[]);


#endif

