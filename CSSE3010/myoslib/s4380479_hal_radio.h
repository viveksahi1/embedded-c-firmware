/**
 ******************************************************************************
 * @@file    mylib/s4380479_hal_radio.h
 * * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   radio peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_hal_radio_init() intialise radio
 * s4380479_hal_radio_fsmprocessing() Radio FSM processing loop
 * s4380479_hal_radio_setchan(unsigned char chan) Set the channel of the radio
 * s4380479_hal_radio_settxaddres(unsigned char *addr) Set the transmit address of the radio
 * s4380479_hal_radio_setrxaddres(unsigned char *addr) Set the receive address of the radio
 * unsigned char s4380479_hal_radio_getchan() Get the channel of the radio
 * s4380479_hal_radio_gettxaddres(unsigned char *addr) get the transmit address of the radio
 * s4380479_hal_radio_getrxaddres(unsigned char *addr) get the receive address of the radio
 * s4380479_hal_radio_sendpacket(char chan, unsigned char *addr, unsigned char *txpacket) Function to send a packet
 * s4380479_hal_radio_setfsmrx() Set Radio FSM into RX mode.
 * int s4380479_hal_radio_getrxstatus() Function to check when packet is received
 * s4380479_hal_radio_getpacket(unsigned char *rxpacket) Function to receive a packet
 *
 ******************************************************************************
 */

#ifndef S4380479_HAL_RADIO_H
#define S4380479_HAL_RADIO_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdio.h>
#include <stdlib.h>
#include "s4380479_hal_hamming.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define S4380479_IDLE_STATE 0
#define S4380479_RX_STATE 1
#define S4380479_TX_STATE 2
#define S4380479_WAITING_STATE 3
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_hal_radio_init(void);
extern void s4380479_hal_radio_fsmprocessing(void);
extern void s4380479_hal_radio_setchan(unsigned char chan);
extern void s4380479_hal_radio_settxaddres(unsigned char* addr);
extern void s4380479_hal_radio_setrxaddres(unsigned char *addr);
unsigned char s4380479_hal_radio_getchan(void);
extern void s4380479_hal_radio_gettxaddres(unsigned char *addr);
extern void s4380479_hal_radio_getrxaddres(unsigned char *addr);
extern void s4380479_hal_radio_sendpacket(char chan, unsigned char *addr, unsigned char *txpacket);
extern void s4380479_hal_radio_setfsmrx(void);
int s4380479_hal_radio_getrxstatus(void);
extern int s4380479_hal_radio_getpacket(unsigned char *rxpacket);
void init_s4380479_rx_buffer_buffer(void);
extern void s4380479_hal_radio_idle(void);
extern int s4380479_radio_is_number(char* text);

#endif

