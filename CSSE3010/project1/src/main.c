/**
 ******************************************************************************
 * @file    project1.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Project 1 main file
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <stdio.h>
#include <s4380479_hal_pantilt.h>
#include <s4380479_hal_joystick.h>
#include <s4380479_hal_ir.h>
#include <s4380479_hal_ir_coms.h>
#include <s4380479_hal_ledbar.h>
#include <s4380479_hal_radio.h>
#include <s4380479_hal_hamming.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define IDLE				1
#define PAN_TILT_CONSOLE	2
#define PAN_TILT_JOYSTICK	3
#define	ENCODE_DECODE		4
#define	IR_DUPLEX			5
#define	RADIO_DUPLEX		6
#define	FULL_DUPLEX			7
#define IR_AUTO_SPEED		8
#define ENTER_KEY			13
#define PRINTF_REFLECT		1
#define MAX_IR_STRING		11
#define ACK_MESSAGE			1
#define ERR_MESSAGE			2
#define MAX_BYTES			22
#define IR_ACK				6
#define IR_NAK				21
#define	IR_STX				2
#define	IR_ETX				3
#define BUFFER_SIZE			100
#define LEDBAR_ALL			1023
#define LEDBAR_STATE		14
#define SEND_LED			16
#define RECEIVE_LED			32
#define ACK_LED				128
#define ERR_LED				256

/* Private macro -------------------------------------------------------------*/
/* Private variables (Global) ---------------------------------------------------------*/
char userInputBuffer[BUFFER_SIZE]; //set input buffer to
char irMesssage[12]; // store IR message received
int storeFreq[BUFFER_SIZE * 4]; //frequencies of falling/rising ir
//flag variables
int userInputLength = 0, numCharReceived = 0, counterInterrupt = 0,
		transmitBit = 1, irMessageLength = 0, stxFlag = 0, etxFlag = 0,
		ackFlag = 0, iRReceiveFlag = 0, iCCountInterrupt = 0, messageSent = 0,
		messageReceived = 0, errReceived = 0;
char hexValue[12][5]; //convert input char to hex
int numCharTransmit = 0, stxSendFlag = 0; //stores IR decode data value
unsigned int signalTime;
int dataBits[BUFFER_SIZE * 5], bitPos = 0, charPos = 0;
uint8_t inputHexNibble[44];
TIM_HandleTypeDef TIM_Init_Radio; //handler for radio
TIM_HandleTypeDef TIM_IRX_Init; //hander for IR capture
uint8_t baseTxAddr[] = { 0x46, 0x33, 0x22, 0x11, 0x00 };
uint8_t baseRxAddr[] = { 0x91, 0x47, 0x80, 0x43, 0x00 };
unsigned char packetBuffer[32] = { 0xA1, 0x46, 0x33, 0x22, 0x11, 0x91, 0x47,
		0x80, 0x43, 0x00 };
unsigned char ackMsg[22] = { 0x47, 0x1E, 0x2B, 0x0, 0x47, 0x35, 0x2B, 0x0, 0x47,
		0xB8 };
unsigned char errMsg[22] = { 0x47, 0x59, 0x2B, 0x0, 0x59, 0x2b, 0x2B, 0x0, 0x59,
		0x2B };
uint8_t currentChannel = 46; //current radio channel
unsigned char rxPacket[32];
int irEncode[22]; //store encoded bits
int mode = IDLE; //default mode to idle

/* Private function prototypes -----------------------------------------------*/
void Hardware_init(void);
int servo_negative(int angle);
int servo_positive(int angle);
void clear_buffer(void);
void send_ack(int mode, int ack);
int check_message(void);
void print_sending_message(void);
void print_ir_acknowledge(int type);
void decode_ir_message_data(void);
void switch_mode(char Rxchar);
void ledbar_state_indicator(int stage);
void indicate_ack_procedure(int value);
void init_hardware_drivers(void);
void setup_radio(void);
int servo_console_control(char Rxchar);
void manchester_mode(void);
void hamming_mode(void);
void encode_msg_to_send(void);
void demodulate_ir_signal(void);
int initiate_radio_send_procedure(void);
int bound_tilt_angle(int angle);

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {
	/* Local Variables */
	char rxChar; //stores the current console char
	// Variables used to as FLags for Radio and IR
	int radioReSend = 0, reSentCounter = 0, irReSend = 0, decodedChar,
			heartBeat = 0, charSent = 0;
	// Timer variables uses to add delay to process
	uint32_t timeAck = 0, waitIr = 0, heartBeatTimer = HAL_GetTick(),
			panTiltTimer = HAL_GetTick(), irSendWait = 0, joyStickWait = 0;
	// init pan/tilt angles to 0
	int panAngle = 0, tiltAngle = 0, xInput, yInput, propAngle;

	/* Hardware Initialisation */
	BRD_init(); // Initialise Board
	Hardware_init(); // Initialise hardware peripherals
	init_hardware_drivers();
	setup_radio(); // configure challen and addreses
	s4380479_hal_ledbar_write(LEDBAR_ALL, 0);

	/* get init joystick positions */
	int initXPos = s4380479_hal_joystick_x_read();
	int initYPos = s4380479_hal_joystick_y_read();
	setbuf(stdout, NULL); //getchar buffer
	debug_printf("CSSE3010 Project 1\r\n");

	/* Main processing loop */
	while (1) {

		//toggle heart beat led
		if ((HAL_GetTick() - heartBeatTimer) > 200) { //WAIT 200MS BEFORE TOGGLE

			heartBeat ^= 1; //toggle headtbeat
			s4380479_hal_ledbar_write(1, heartBeat);
			heartBeatTimer = HAL_GetTick();
		}
		ledbar_state_indicator(mode); //indicate current mode on ledbar

		/* Receive characters using getc */
		rxChar = debug_getc();
		// Check if character is not null
		if (rxChar != '\0') {
#ifdef PRINTF_REFLECT
			debug_printf("%c", rxChar); // reflect byte using printf - must delay before calling printf again
#else
					debug_putc(RxChar); // reflect byte using putc - puts character into buffer
#endif
			if (rxChar == ENTER_KEY && userInputLength == 1) {

				rxChar = userInputBuffer[userInputLength - 1]; //swicth mode
				switch_mode(rxChar);
				userInputLength = 0;
			} else if (rxChar != ENTER_KEY) {

				userInputBuffer[userInputLength++] = rxChar;
			} else {

				printf("\n");
			}
		}
		/* Print Pant/Tilt angles each second */
		if ((mode == PAN_TILT_CONSOLE || mode == PAN_TILT_JOYSTICK)
				&& (HAL_GetTick() - panTiltTimer) > 1000) {

			printf("Pan: %d Tilt: %d\r\n", panAngle, tiltAngle);
			panTiltTimer = HAL_GetTick();
		}
		// Update send and receive leds
		if (messageSent || messageReceived) {

			messageSent = 0;
			messageReceived = 0;
			s4380479_hal_ledbar_write(RECEIVE_LED, messageReceived);
			s4380479_hal_ledbar_write(SEND_LED, messageSent);
		}

		/* IDLE mode */
		if (mode == IDLE && userInputLength > 1) {

			//reset char buffer
			userInputLength = 0;
			/* Pan/Tilt COnsole Mode*/
		} else if (mode == PAN_TILT_CONSOLE) { //console servo control

			if (rxChar == 'D' || rxChar == 'A' || rxChar == 'a'
					|| rxChar == 'd') {

				panAngle = servo_console_control(rxChar);
			} else if (rxChar == 'W' || rxChar == 'S' || rxChar == 's'
					|| rxChar == 'w') {

				tiltAngle = servo_console_control(rxChar);
			} else if (rxChar == ENTER_KEY) {

				userInputLength = 0; //clear user input buffer
			}
			/* Pan/Tilt Joystick Mode */
		} else if (mode == PAN_TILT_JOYSTICK) {

			if ((HAL_GetTick() - joyStickWait) > 15) { //slow down joystick

				//update joystick values
				xInput = s4380479_hal_joystick_x_read();
				yInput = s4380479_hal_joystick_y_read();
				if (xInput > initXPos) {

					propAngle = (xInput - initXPos) / 6;
					panAngle = s4380479_hal_pantilt_pan_read()
							- (2 + propAngle);
					s4380479_hal_pantilt_pan_write(panAngle);
				} else if (xInput < initXPos) {

					propAngle = (-xInput + initXPos) / 6;
					panAngle = s4380479_hal_pantilt_pan_read()
							+ (3 + propAngle);
					s4380479_hal_pantilt_pan_write(panAngle);
				} else if ((yInput - initYPos) >= 2) {

					propAngle = (yInput - initYPos) / 6;
					tiltAngle = s4380479_hal_pantilt_tilt_read()
							- (3 + propAngle);
					s4380479_hal_pantilt_tilt_write(tiltAngle);
				} else if (yInput < initYPos) {

					propAngle = (-yInput + initYPos) / 6;
					tiltAngle = s4380479_hal_pantilt_tilt_read()
							+ (3 + propAngle);
					s4380479_hal_pantilt_tilt_write(tiltAngle);
				}
				tiltAngle = bound_tilt_angle(tiltAngle);
				panAngle = bound_tilt_angle(panAngle);
				joyStickWait = HAL_GetTick();
			}
			if (rxChar == ENTER_KEY || userInputLength > 5) {

				userInputLength = 0; //clear user input buffer
			}
			//HAL_Delay(10);
		} else if (mode == ENCODE_DECODE) { // Manchester/Hamming Encode/Decode

			if (rxChar == ENTER_KEY) {

				if (userInputBuffer[0] == 'M') { //Manchester

					manchester_mode();
				} else if (userInputBuffer[0] == 'H') { //hamming

					hamming_mode();
				}
				userInputLength = 0;
			}
		} else if (mode == IR_DUPLEX) { // IR DUPLEX

			/* IR Sending Procedure */
			if ((rxChar == ENTER_KEY && userInputLength > 2)
					|| (irReSend && (HAL_GetTick() - timeAck) > 3000
							&& reSentCounter < 3) || stxSendFlag) {

				if (userInputBuffer[0] == 'I' && userInputBuffer[1] == 'T') {

					/*  Transmit STX */
					if (!stxSendFlag) {

						encode_msg_to_send(); //encode message and send STX
						print_ir_acknowledge(IR_STX); //STX
						stxSendFlag = 1;
						irSendWait = HAL_GetTick();
					}
					/* Transmit each char */
					if (stxSendFlag && (charSent != numCharTransmit)
							&& (HAL_GetTick() - irSendWait) > 50) {

						//Message to transmit
						for (; charSent < numCharTransmit; charSent++) {
							s4380479_hal_manchester_encode(
									hexValue[charSent][0],
									hexValue[charSent][1], irEncode, 0);
							//UPDATE FLAGS
							charSent++;
							s4380479_hal_ir_transmit(irEncode);
							irSendWait = HAL_GetTick();
							break;
						}
					}

					/*  Transmit ETX */
					if (charSent == numCharTransmit) { //full message sent

						print_ir_acknowledge(IR_ETX); //ETX
						//print send string
						printf("Sent from IR: ");
						for (int i = 0; i < numCharTransmit; i++) {
							printf("%c", userInputBuffer[i + 2]);
						}
						printf("\r\n");

						messageSent = 1; //indicate message sent
						charSent = 0;
						stxSendFlag = 0;
						s4380479_hal_ledbar_write(SEND_LED, messageSent);
						irReSend = 1;
						reSentCounter++;
						timeAck = HAL_GetTick();
					}
				} else {

					userInputLength = 0; //FORMAT ERROR
				}
			} else if (rxChar == ENTER_KEY) {

				userInputLength = 0; //FORMAT ERROR
			}
			if (reSentCounter > 2) {
				// no need to send again
				printf("After 3 transmits giving up\r\n");
				irReSend = 0;
				reSentCounter = 0;
				userInputLength = 0;
			}
			indicate_ack_procedure(irReSend);

			/* IR receiving */
			if (iCCountInterrupt && !iRReceiveFlag) {

				waitIr = HAL_GetTick(); //get time of msg receive
				iRReceiveFlag = 1;
			} else if (iCCountInterrupt && (HAL_GetTick() - waitIr) > 2000) {

				messageReceived = 1;
				s4380479_hal_ledbar_write(RECEIVE_LED, messageReceived);
				decode_ir_message_data(); //convert IR message to bits -> store
				numCharReceived = bitPos / 22; //length of the ir signal
				demodulate_ir_signal();

				// if Stx and ETX received
				if (stxFlag && etxFlag) {

					for (int i = 0; i < irMessageLength; i++) {
						printf("%c", irMesssage[i]);
					}
					printf("\r\n");
					printf("Sent from IR: ACK\r\n");
					print_ir_acknowledge(IR_ACK);
				} else if (ackFlag == 1) { //ack received

					irReSend = 0;
					reSentCounter = 0;
					userInputLength = 0;
					ackFlag = 0;
				} else if (ackFlag == 2) { //nack received

					irReSend = 0;
					reSentCounter = 0;
					userInputLength = 0;
					ackFlag = 0;
				}

				stxFlag = 0;
				etxFlag = 0;
				irMessageLength = 0;
				iCCountInterrupt = 0;
				iRReceiveFlag = 0;
				bitPos = 0; //reset bit position
			}
		} else if (mode == RADIO_DUPLEX) { // RADIO DUPLEX MODE

			//UPDATE LED BAR
			s4380479_hal_ledbar_write(ERR_LED, errReceived);
			s4380479_hal_radio_fsmprocessing();

			/* sending radio message */
			if ((rxChar == ENTER_KEY && userInputLength > 2)
					|| (radioReSend && (HAL_GetTick() - timeAck) > 3000
							&& reSentCounter < 3)) {

				if (initiate_radio_send_procedure()) {

					radioReSend = 1; // begin ack prcedure
					reSentCounter++;
					timeAck = HAL_GetTick();
				}
			} else if (rxChar == ENTER_KEY) {

				userInputLength = 0; //FORMAT ERROR
			}

			/*  CHECK ACK END */
			if (reSentCounter > 2) {

				// no need to send again
				radioReSend = 0;
				reSentCounter = 0;
				userInputLength = 0;
				clear_buffer();
			}
			indicate_ack_procedure(radioReSend);

			/* reading packet */
			if (s4380479_hal_radio_getrxstatus() == 1) {

				messageReceived = 1;
				s4380479_hal_ledbar_write(RECEIVE_LED, messageReceived);
				if (s4380479_hal_radio_getpacket(rxPacket)) {

					//2-bit error -> send ERR
					send_ack(1, 0);
				} else { //message can be decoded

					if (!check_message()) {

						//send ACK
						send_ack(1, 1);
					} else if (check_message() == ACK_MESSAGE) {

						//message correctly sent
						radioReSend = 0;
						reSentCounter = 0;
						userInputLength = 0;
						clear_buffer();
						errReceived = 0;
					} else {

						//ERR received resend message
						errReceived = 1;
						timeAck = HAL_GetTick();
					}
				}
			}
			HAL_Delay(10); //Delay for 10ms.
		} else if (mode == FULL_DUPLEX) { // FULL DUPLEX

			s4380479_hal_ledbar_write(ERR_LED, errReceived);
			s4380479_hal_radio_fsmprocessing();

			/* sending radio message */
			if ((rxChar == ENTER_KEY && userInputLength > 2)
					|| (radioReSend && (HAL_GetTick() - timeAck) > 3000
							&& reSentCounter < 3)) {

				if (initiate_radio_send_procedure()) {

					radioReSend = 1; // begin ack prcedure
					reSentCounter++;
					timeAck = HAL_GetTick();
				}
			} else if (rxChar == ENTER_KEY) {

				userInputLength = 0; //FORMAT ERROR
			}

			/*  CHECK ACK END */
			if (reSentCounter > 2) {

				// no need to send again
				radioReSend = 0;
				reSentCounter = 0;
				userInputLength = 0;
				clear_buffer();
			}
			indicate_ack_procedure(radioReSend);

			/*  IR receiving */
			if (iCCountInterrupt && !iRReceiveFlag) {

				waitIr = HAL_GetTick(); //get time of msg receive
				iRReceiveFlag = 1;
			} else if (iCCountInterrupt && (HAL_GetTick() - waitIr) > 2000) {

				messageReceived = 1;
				s4380479_hal_ledbar_write(RECEIVE_LED, messageReceived);

				//convert IR message to bits -> store
				decode_ir_message_data();
				for (int j = 0; j < 4; j++) {
					charPos = 4 + (j * 4); //FIRST MESSAGE BIT IS AT 4
					inputHexNibble[j] = dataBits[charPos] << 3
							| dataBits[charPos + 1] << 2
							| dataBits[charPos + 2] << 1
							| dataBits[charPos + 3];
				}
				decodedChar = s4380479_hal_manchester_decode(inputHexNibble,
						16);

				// validate decode char
				switch (decodedChar) {

					case IR_ACK:

						reSentCounter = 3;
						errReceived = 0;
						printf("Received from IR: ACK\r\n");
						break;

					case IR_NAK:

						//ERR received resend message
						errReceived = 1;
						timeAck = HAL_GetTick();
						printf("Received from IR: NACK\r\n");
						break;

					default:

						//RETRANSMIT
						break;
				}
				bitPos = 0; //reset bit position
				iCCountInterrupt = 0;
				iRReceiveFlag = 0;
			}
			//reading RADIO packet
			if (s4380479_hal_radio_getrxstatus() == 1) {

				messageReceived = 1;
				s4380479_hal_ledbar_write(RECEIVE_LED, messageReceived);
				if (s4380479_hal_radio_getpacket(rxPacket)) {

					//2-bit error -> send ERR
					print_ir_acknowledge(IR_NAK);
					printf("Sent from IR: NACK\r\n");
				} else { //message can be decoded

					if (!check_message()) {

						//send ACK
						print_ir_acknowledge(IR_ACK);
						printf("Sent from IR: ACK\r\n");
					} else if (check_message() == ACK_MESSAGE) {

						// print message received from IR but continue ack procedure
					}
				}
			}
			HAL_Delay(10); //Delay for 10ms.
		}
	} //while
	return 1;
} //main

/**
 * Demodulate the IR signla received and update flags appropiately
 */
void demodulate_ir_signal(void) {
	int decodedIrChar;
	for (int i = 0; i < numCharReceived; i++) {
		if (!etxFlag && !ackFlag) {

			if (i > MAX_IR_STRING + 1) {

				etxFlag = 1; //assume ETX received
				break;
			}

			for (int j = 0; j < 4; j++) {
				charPos = (i * 22) + (j * 4) + 4;
				inputHexNibble[j] = dataBits[charPos] << 3
						| dataBits[charPos + 1] << 2
						| dataBits[charPos + 2] << 1 | dataBits[charPos + 3];
			}
			decodedIrChar = s4380479_hal_manchester_decode(inputHexNibble, 16);
			//decode char
			switch (decodedIrChar) {

				case IR_STX:

					stxFlag = 1;
					break;

				case IR_ETX:

					etxFlag = 1;
					printf("Received from IR: ");
					break;

				case IR_ACK:

					printf("Received from IR: ACK\r\n");
					ackFlag = 1;
					break;

				case IR_NAK:

					ackFlag = 2;
					printf("Received from IR: NACK\r\n");
					break;

				default:
					if (!stxFlag) {

						printf("Received from IR: %c\r\n", decodedIrChar);
					} else if (irMessageLength < 11) {

						irMesssage[irMessageLength++] = decodedIrChar;
					} else {

						etxFlag = 1;
					}
					break;
			}
		}
	} // ETX received
}

/**
 * Decode the user input using hamming decoder and send the message via radio
 *
 * Returns int to indicate messae sent
 * 	1 -> sent
 */
int initiate_radio_send_procedure(void) {

	if (userInputBuffer[0] == 'R' && userInputBuffer[1] == 'T') {

		//haming encode message
		for (numCharTransmit = 0; numCharTransmit < userInputLength - 2;
				numCharTransmit++) {
			if (numCharTransmit == MAX_IR_STRING) {

				userInputLength = MAX_IR_STRING + 2;
				break;
			}
			//load data into packet buffer
			packetBuffer[(2 * numCharTransmit) + 10] =
					s4380479_hal_hamming_byte_encoder(
							userInputBuffer[numCharTransmit + 2]) >> 8;
			packetBuffer[(2 * numCharTransmit) + 11] =
					s4380479_hal_hamming_byte_encoder(
							userInputBuffer[numCharTransmit + 2]) & 0x00FF;
		}
		//send message
		s4380479_hal_radio_sendpacket(currentChannel, baseTxAddr, packetBuffer);
		s4380479_hal_radio_setfsmrx();
		print_sending_message();

		//indicate message sent
		messageSent = 1;
		s4380479_hal_ledbar_write(SEND_LED, messageSent);

		return 1;
	} else {

		userInputLength = 0; //FORMAT ERROR
	}

	return 0;
}

/*
 * Handle hamming encoding and decoding (mode 4)
 */
void hamming_mode(void) {

	int DecodeNum1, DecodeNum2;
	//hamming encoding
	if (userInputBuffer[1] == 'E' && userInputLength == 4) {

		DecodeNum1 = hex_to_int_converter(userInputBuffer[2]) << 4
				| hex_to_int_converter(userInputBuffer[3]);
		printf("%02X\r\n", s4380479_hal_hamming_byte_encoder(DecodeNum1));
	} else if (userInputBuffer[1] == 'D' && userInputLength == 6) {

		//hamming decoding reset flags
		s4380479_hal_reset_error_mask();
		s4380479_hal_reset_full_decode();
		s4380479_reset_byte_flag();

		DecodeNum1 = (hex_to_int_converter(userInputBuffer[2]) << 4
				| hex_to_int_converter(userInputBuffer[3]));
		DecodeNum2 = (hex_to_int_converter(userInputBuffer[4]) << 4
				| hex_to_int_converter(userInputBuffer[5]));
		if (s4380479_hal_hamming_decoder(DecodeNum1) == -1
				|| s4380479_hal_hamming_decoder(DecodeNum2) == -1) {

			printf("2-bit ERROR\r\n");
		} else {

			//hamming decoding reset flags
			s4380479_hal_reset_error_mask();
			s4380479_hal_reset_full_decode();
			s4380479_reset_byte_flag();
			debug_printf("%X%X (Full:%04X ErrMask:%04X)\r\n",
					s4380479_hal_hamming_decoder(DecodeNum1),
					s4380479_hal_hamming_decoder(DecodeNum2),
					s4380479_hal_full_decode(), s4380479_hal_error_mask());
		}
	}
}

/**
 * Encode IR signal using manchester encoding
 */
void encode_msg_to_send(void) {

	//encode message to send
	for (numCharTransmit = 0; numCharTransmit < userInputLength - 2;
			numCharTransmit++) {
		if (numCharTransmit == MAX_IR_STRING) {

			break;
		}
		//convert input to hex
		sprintf(hexValue[numCharTransmit], "%0X",
				(int) userInputBuffer[numCharTransmit + 2]);
	}
}

/**
 * Ensures the tile servo remain in bounds
 *
 * @param takes angle value
 *
 * return corrected angle
 */
int bound_tilt_angle(int angle) {

	if (angle < -75) {

		return -75;
	} else if (angle > 75) {

		return 75;
	}

	return angle;
}

/*
 * Handle manchester encoding and decoding (mode 4)
 */
void manchester_mode(void) {

	uint8_t DecodeInput[4]; //store hex nibbles to decode
	//manchester encoding
	if (userInputBuffer[1] == 'E' && userInputLength == 4) {

		s4380479_hal_manchester_encode(userInputBuffer[2], userInputBuffer[3],
				irEncode, 1);
	} else if (userInputBuffer[1] == 'D' && userInputLength == 6) {

		//manchester decoding
		for (int i = 0; i < 4; i++) {
			DecodeInput[i] = userInputBuffer[i + 2];
		}
		printf("%02X\r\n", s4380479_hal_manchester_decode(DecodeInput, 16));
	}
}

/**
 * Function to handle console servo angles for console input
 *
 * @param takes Rxchar to find dire to move
 *
 * return the angle servo moved to
 */
int servo_console_control(char Rxchar) {

	int angle = 0;
	if (Rxchar == 'D' || Rxchar == 'd') {

		angle = servo_positive(s4380479_hal_pantilt_pan_read() - 5);
		s4380479_hal_pantilt_pan_write(angle);
	} else if (Rxchar == 'A' || Rxchar == 'a') {

		angle = servo_negative(s4380479_hal_pantilt_pan_read() + 5);
		s4380479_hal_pantilt_pan_write(angle);
	} else if (Rxchar == 'W' || Rxchar == 'w') {

		angle = servo_positive(
		s4380479_hal_pantilt_tilt_read() + 5);
		s4380479_hal_pantilt_tilt_write(angle);
	} else if (Rxchar == 'S' || Rxchar == 's') {

		angle = servo_negative(
		s4380479_hal_pantilt_tilt_read() - 5);
		s4380479_hal_pantilt_tilt_write(angle);
	}
	userInputLength = 0; //clear user input buffer

	return angle;
}

/**
 * Setup radio channel and RX?TX adresses,
 * also clears the packet buffer
 */
void setup_radio(void) {

	clear_buffer(); //clear message buffer
//set channel
	s4380479_hal_radio_setchan(currentChannel);
//set tx and rx address
	s4380479_hal_radio_settxaddres(baseTxAddr);
	s4380479_hal_radio_setrxaddres(baseRxAddr);
}

/**
 * Initialise hardware peripherals
 */
void init_hardware_drivers(void) {

	s4380479_hal_ledbar_init();
	s4380479_hal_pantilt_init(); //init pan/tilt
	s4380479_hal_joystick_init(); //init joystick
	s4380479_hal_ir_init(); //init transmitter module
	s4380479_hal_radio_init(); //init radio module
}

/**
 * Turns the ACk led on/off depending on state
 */
void indicate_ack_procedure(int value) {

	s4380479_hal_ledbar_write(ACK_LED, value);
	HAL_Delay(10); // delay for 10ms so led can be seen
}

/**
 * ledbar state indicator
 *
 * @param switches led based on given mode
 */
void ledbar_state_indicator(int stage) {

	s4380479_hal_ledbar_write(LEDBAR_STATE, 0); //clear state ledbar
	if (stage == IDLE) {

		//do nothing
	} else if (stage == PAN_TILT_CONSOLE) {

		s4380479_hal_ledbar_write(8, 1); // 1 ->8 on ledbar
	} else if (stage == PAN_TILT_JOYSTICK) {

		s4380479_hal_ledbar_write(4, 1); // 2 ->4 on ledbar
	} else if (stage == ENCODE_DECODE) {

		s4380479_hal_ledbar_write(12, 1); // 3 ->4 on ledbar
	} else if (stage == IR_DUPLEX) {

		s4380479_hal_ledbar_write(2, 1); // 4 ->4 on ledbar
	} else if (stage == RADIO_DUPLEX) {

		s4380479_hal_ledbar_write(10, 1); // 5 ->4 on ledbar
	} else if (stage == FULL_DUPLEX) {

		s4380479_hal_ledbar_write(6, 1); // 6 ->4 on ledbar
	} else if (stage == IR_AUTO_SPEED) {

		s4380479_hal_ledbar_write(14, 1); // 7 ->4 on ledbar
	}
}

/**
 * Handle mode switch
 *
 * @param char to switch mode to
 */
void switch_mode(char RxChar) {

	if (RxChar == '?') {

		mode = IDLE;
		debug_printf("CSSE3010 Project 1\r\n");
		debug_printf("1 Idle\r\n");
		debug_printf("2 P/T Terminal\r\n");
		debug_printf("3 P/T Joystick\r\n");
		debug_printf("4 Encode/Decode\r\n");
		debug_printf("5 IR Duplex\r\n");
		debug_printf("6 Radio Duplex\r\n");
		debug_printf("7 Full Duplex\r\n");
		debug_printf("8 IR Auto Speed\r\n");
	} else if (RxChar == '1') {

		debug_printf("Mode 1: Idle\r\n");
		mode = IDLE;
	} else if (RxChar == '2') {

		debug_printf("Mode 2: P/T Terminal Control\r\n");
		mode = PAN_TILT_CONSOLE;
	} else if (RxChar == '3') {

		debug_printf("Mode 3: P/T Joystick Control\r\n");
		mode = PAN_TILT_JOYSTICK;
	} else if (RxChar == '4') {

		debug_printf("Mode 4: Encode/Decode\r\n");
		mode = ENCODE_DECODE;
	} else if (RxChar == '5') {

		debug_printf("Mode 5: IR Duplex\r\n");
		mode = IR_DUPLEX;
	} else if (RxChar == '6') {

		debug_printf("Mode 6: Radio Duplex\r\n");
		mode = RADIO_DUPLEX;
	} else if (RxChar == '7') {

		debug_printf("Mode 7: Full Duplex\r\n");
		mode = FULL_DUPLEX;
	} else if (RxChar == '8') {

		debug_printf("Mode 8: IR Auto Speed\r\n");
		mode = IR_AUTO_SPEED;
	}
}

/**
 * Decodes Received IR data
 */
void decode_ir_message_data(void) {

	//for each rise/fall edge
	for (int i = 0; i < iCCountInterrupt; i++) {
		if (storeFreq[i] == 0 && i == 0) {

			//starting bit 1
			dataBits[bitPos++] = 0;
		} else if (storeFreq[i] < 100) {

			//delay between packets
			dataBits[bitPos++] = transmitBit ^ 1;
			dataBits[bitPos++] = transmitBit ^ 1;
		} else {

			//consecutive bits
			int j = 1000 / (storeFreq[i] * 4);
			while (j) {

				dataBits[bitPos++] = transmitBit ^ 1;
				j--;
			}
		}
		transmitBit ^= 1; //switch trigger mode
	}
	dataBits[bitPos++] = 0;
}

/**
 * prints IR acknowledge messages
 *
 * @param type - messgae type Ack/Nack/STX/ETX
 */
void print_ir_acknowledge(int type) {

	switch (type) {

		case IR_ACK:

			s4380479_hal_manchester_encode('0', '6', irEncode, 0);
			break;

		case IR_NAK:

			s4380479_hal_manchester_encode('1', '5', irEncode, 0);
			break;

		case IR_STX:

			s4380479_hal_manchester_encode('0', '2', irEncode, 0);
			break;

		case IR_ETX:

			s4380479_hal_manchester_encode('0', '3', irEncode, 0);
			break;

		default:
			break;
	}
	s4380479_hal_ir_transmit(irEncode);
}

/**
 * prints the message sent via radio
 */
void print_sending_message(void) {

	int j;
	debug_printf("Sent from Radio: \"");
	for (j = 0; j < userInputLength - 2; j++) {
		printf("%c", userInputBuffer[j + 2]);
	}
	printf("\"\r\n");
}

/**
 * Sends ack/err messsage
 * mode 1 == Radio
 * mode 0 == IR
 * type 1 = ack
 * type 0 = err
 */
void send_ack(int mode, int type) {

	int j;
	if (mode) { //Radio

		clear_buffer();
		//fill up packet buffer
		if (type) {

			for (j = 0; j < 10; j++) {
				packetBuffer[j + 10] = ackMsg[j];
			}
			printf("Sent from Radio: \"A C K\"\r\n");
		} else {

			for (j = 0; j < 10; j++) {
				packetBuffer[j + 10] = errMsg[j];
			}
			printf("Sent from Radio: \"E R R\"\r\n");
		}

		//transmit packet
		s4380479_hal_radio_sendpacket(currentChannel, baseTxAddr, packetBuffer);
		s4380479_hal_radio_setfsmrx();
		clear_buffer();
	}
}

/**
 * Check if the received message is ACK/ERR
 *
 * return int to suggest the message received
 */
int check_message(void) {

	int j, match = 1;
	//if msg ack
	for (j = 0; j < MAX_BYTES; j++) {
		if (ackMsg[j] != rxPacket[j + 10]) {

			match = 0;
			break;
		} else {

			match = ACK_MESSAGE;
		}
	}
	if (!match) {

		//if msg err
		for (j = 0; j < MAX_BYTES; j++) {
			if (errMsg[j] != rxPacket[j + 10]) {

				match = 0;
				break;
			} else {

				match = ERR_MESSAGE;
			}
		}
	}

	return match;
}

/**
 * @brief  Initialise Hardware
 * @param  None
 * @retval None
 */
void Hardware_init(void) {

	unsigned short PrescalerValue;
	TIM_IC_InitTypeDef TIM_ICInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	BRD_LEDInit();		//Initialise LEDS
	/* Turn off LEDs */
	BRD_LEDRedOff();
	BRD_LEDGreenOff();
	BRD_LEDBlueOff();

	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

	/* enable clocks*/
	__TIM4_CLK_ENABLE()
	;
	__TIM3_CLK_ENABLE()
	;
	//input capture
	__BRD_A6_GPIO_CLK()
	;

	/* Configure the A6 pin with TIM3 input capture */
	GPIO_InitStructure.Pin = BRD_A6_PIN;				//Pin
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; //Set mode to be output alternate
	GPIO_InitStructure.Pull = GPIO_NOPULL; //Enable Pull up, down or no pull resister
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;			//Pin latency
	GPIO_InitStructure.Alternate = GPIO_AF2_TIM3;//Set alternate function to be timer 2
	HAL_GPIO_Init(BRD_A6_GPIO_PORT, &GPIO_InitStructure);

	// Compute the prescaler value
	PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / 50000) - 1;

	/* TIM Base configuration for radio */
	TIM_Init_Radio.Instance = TIM4;				//Enable Timer 2
	TIM_Init_Radio.Init.Period = 50000 / 1000;//Set period count to be 1ms, so timer interrupt occurs every 1ms.
	TIM_Init_Radio.Init.Prescaler = PrescalerValue;	//Set prescaler value
	TIM_Init_Radio.Init.ClockDivision = 0;			//Set clock division
	TIM_Init_Radio.Init.RepetitionCounter = 0;	// Set reload Value
	TIM_Init_Radio.Init.CounterMode = TIM_COUNTERMODE_UP;//Set timer to count up.

	//configure timer handler for input capture
	TIM_IRX_Init.Instance = TIM3;
	TIM_IRX_Init.Init.Period = 2 * 50000 / 10;
	TIM_IRX_Init.Init.Prescaler = PrescalerValue;
	TIM_IRX_Init.Init.ClockDivision = 0;
	TIM_IRX_Init.Init.RepetitionCounter = 0;
	TIM_IRX_Init.Init.CounterMode = TIM_COUNTERMODE_UP;

	// Configure TIM3 Input capture
	TIM_ICInitStructure.ICPolarity = TIM_ICPOLARITY_BOTHEDGE;//Set to trigger on rising edge
	TIM_ICInitStructure.ICSelection = TIM_ICSELECTION_DIRECTTI;
	TIM_ICInitStructure.ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.ICFilter = 0;

	/* Initialise Timer */
	HAL_TIM_Base_Init(&TIM_Init_Radio);
	/* Set priority of Timer 2 update Interrupt [0 (HIGH priority) to 15(LOW priority)] */
	HAL_NVIC_SetPriority(TIM4_IRQn, 10, 0);
	HAL_NVIC_SetPriority(TIM3_IRQn, 10, 0);
	HAL_NVIC_EnableIRQ(TIM4_IRQn);
	NVIC_EnableIRQ(TIM3_IRQn);

	// Enable input capture for Timer 3, channel 3
	HAL_TIM_IC_Init(&TIM_IRX_Init);
	HAL_TIM_IC_ConfigChannel(&TIM_IRX_Init, &TIM_ICInitStructure,
	TIM_CHANNEL_4);
	// Start Timer 2 base unit in interrupt mode
	HAL_TIM_Base_Start_IT(&TIM_Init_Radio);
	HAL_TIM_IC_Start_IT(&TIM_IRX_Init, TIM_CHANNEL_4);
}

/**
 * @brief  Timer 1 Input Capture Interrupt handler
 * @param  TImer handler.
 * @retval None
 */
void HAL_TIM3_IRQHandler(TIM_HandleTypeDef *htim) {

	unsigned int input_capture_value;

	/* Clear Input Capture Flag */
	__HAL_TIM_CLEAR_IT(&TIM_IRX_Init, TIM_IT_TRIGGER);

	/* Read and display the Input Capture value of Timer 3, channel 4 */
	input_capture_value = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
	storeFreq[iCCountInterrupt++] = 1000 / (HAL_GetTick() - signalTime);
	signalTime = HAL_GetTick();     // Update the signal tracker*/

	//safeguard IR receiver
	if (iCCountInterrupt > 400) {
		iCCountInterrupt = 0;
	}
}

/**
 * Tim3 handler
 */
void TIM3_IRQHandler(void) {

	HAL_TIM3_IRQHandler(&TIM_IRX_Init);
	BRD_LEDRedToggle();
}

/**
 * @brief Period elapsed callback in non blocking mode
 * @param htim: Pointer to a TIM_HandleTypeDef that contains the configuration information for the TIM module.
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	if (htim->Instance == TIM4) {
		counterInterrupt++;
	}
}

//Override default mapping of this handler to Default_Handler
void TIM4_IRQHandler(void) {

	HAL_TIM_IRQHandler(&TIM_Init_Radio);
}

/**
 * Adjust the servo pan and tilt angles in console mode
 *
 * @param angle to correct
 *
 * returns corrected angle
 */
int servo_negative(int angle) {

	int error = angle % 5;
	if (error < 0) {

		angle -= error;
	} else if (error) {

		angle += (5 - error);
	}
	if (angle > 75) {

		angle = 75;
	} else if (angle < -75) {

		angle = -75;
	}

	return angle;
}

/**
 * Adjust the servo pan and tilt angles in console mode
 *
 * @param angle to correct
 *
 * returns corrected angle
 */
int servo_positive(int angle) {

//get angle then increment
	int error = angle % 5;
	if (error > 0) {

		angle += (5 - error);
	} else if (error < 0) {

		angle -= error;
	}
	if (angle > 75) {

		angle = 75;
	} else if (angle < -75) {

		angle = -75;
	}

	return angle;
}

/**
 * @brief  Clears the buffer
 * @param  None
 * @retval None
 */
void clear_buffer(void) {
	int PacketIndex;
	for (PacketIndex = 10; PacketIndex < 32; PacketIndex++) {
		packetBuffer[PacketIndex] = 0;
	}
}
