/**
 ******************************************************************************
 * @file    ex_HAL/main.c
 * @author  MDS
 * @date    27112017
 * @brief   Nucleo429ZI onboard LED flashing example.
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <s4380479_hal_ledbar.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/**
 * @brief  Main program - flashes onboard LEDs
 * @param  None
 * @retval None
 */
int main(void) {

	BRD_init();			//Initalise Board
	s4380479_hal_ledbar_init();
	int i;

	/* Main processing loop */
	while (1) {
		i = 0;
		s4380479_hal_ledbar_write(i);
		HAL_Delay(1000);

		for (i = 8; i > 2; i /= 2) {
			s4380479_hal_ledbar_write(i);
			HAL_Delay(1000);
		}
		i = 12;
		s4380479_hal_ledbar_write(i);
		HAL_Delay(1000);

		i = 2;
		s4380479_hal_ledbar_write(i);
		HAL_Delay(1000);

		i = 10;
		s4380479_hal_ledbar_write(i);
		HAL_Delay(1000);

		i = 6;
		s4380479_hal_ledbar_write(i);
		HAL_Delay(1000);

		i = 14;
		s4380479_hal_ledbar_write(i);
		HAL_Delay(1000);

		/*for (; i > 3 ; i /= 2) {
		 s4380479_hal_ledbar_write(i);
		 HAL_Delay(1000);
		 }*/

	}
	return 1;
}

