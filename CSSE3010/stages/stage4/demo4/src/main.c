/**
 ******************************************************************************
 * @file    demo4/main.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   radio
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_radio.h"
#include "radio_fsm.h"
#include "nrf24l01plus.h"
#include "s4380479_hal_hamming.h"

/* Private typedef -----------------------------------------------------------*/
unsigned char my_channel = 46;
/* Private define ------------------------------------------------------------*/
//#define MAX_PAYLOAD 10
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t base_tx_addr[] = { 0x46, 0x33, 0x22, 0x11, 0x00};
uint8_t base_rx_addr[] = { 0x91, 0x47, 0x80, 0x43, 0x00 };
unsigned char packetbuffer[32] = {0xA1, 0x46, 0x33, 0x22, 0x11, 0x91, 0x47,
		0x80, 0x43, 0x00};
uint8_t current_channel = 46;
TIM_HandleTypeDef TIM_Init;
int count_interrupt;
unsigned char rxpacket[32];


/* Private function prototypes -----------------------------------------------*/
void Hardware_init(void);
void clear_buffer(void);

/**
 * @brief  Main program - flashes onboard LEDs
 * @param  None
 * @retval None
 */
int main(void) {

	setbuf(stdout, NULL);
	char RxChar;
	int payload = 0;

	//initiialisation
	BRD_init();			//Initalise Board
	Hardware_init();	//Initalise hardware modules

	s4380479_hal_radio_getchan();
	s4380479_hal_radio_init();
	clear_buffer();

	//set channel
	s4380479_hal_radio_setchan(current_channel);
	s4380479_hal_radio_getchan();

	//set tx and rx address
	s4380479_hal_radio_settxaddres(base_tx_addr);
	s4380479_hal_radio_setrxaddres(base_rx_addr);
	s4380479_hal_radio_sendpacket(current_channel, base_tx_addr,
							packetbuffer);
	/* Main processing loop */
	while (1) {
		s4380479_hal_radio_fsmprocessing();

		RxChar = debug_getc();
		if (RxChar != '\0') {

			debug_printf("%c", RxChar);	// reflect byte using printf - must delay before calling printf again
			// if not 'enter' key and ! payload full
			if (RxChar != 13 && payload < 10) {

				packetbuffer[payload++ + 10] = s4380479_hal_hamming_byte_encoder(RxChar) >> 8;
				packetbuffer[payload + 10] = s4380479_hal_hamming_byte_encoder(RxChar) & 0x00FF;
				payload++;
			} else {

				s4380479_hal_radio_sendpacket(current_channel, base_tx_addr,
						packetbuffer);
				s4380479_hal_radio_setfsmrx();
				payload = 0;
				clear_buffer();
			}
		}

		//reading packet
		if (s4380479_hal_radio_getrxstatus() == 1) {
			s4380479_hal_radio_getpacket(rxpacket);
		}
		HAL_Delay(10); //Delay for 10ms.
	}
	return 0;
}

/**
 * @brief Hardware Initialisation Function.
 * @param  None
 * @retval None
 */
void Hardware_init(void) {
	unsigned short PrescalerValue;

	BRD_LEDInit();		//Initialise Blue LED
	/* Turn off LEDs */
	BRD_LEDRedOff();
	BRD_LEDGreenOff();
	BRD_LEDBlueOff();

	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

	// Timer 2 clock enable
	__TIM4_CLK_ENABLE()
	;

	// Compute the prescaler value
	// Set the clock prescaler to 50kHz
	// SystemCoreClock is the system clock frequency
	PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / 50000) - 1;

	/* TIM Base configuration */
	TIM_Init.Instance = TIM4;				//Enable Timer 2
	TIM_Init.Init.Period = 50000 / 1000;//Set period count to be 1ms, so timer interrupt occurs every 1ms.
	TIM_Init.Init.Prescaler = PrescalerValue;	//Set prescaler value
	TIM_Init.Init.ClockDivision = 0;			//Set clock division
	TIM_Init.Init.RepetitionCounter = 0;	// Set reload Value
	TIM_Init.Init.CounterMode = TIM_COUNTERMODE_UP;	//Set timer to count up.

	/* Initialise Timer */
	HAL_TIM_Base_Init(&TIM_Init);

	/* Set priority of Timer 2 update Interrupt [0 (HIGH priority) to 15(LOW priority)] */
	/* 	DO NOT SET INTERRUPT PRIORITY HIGHER THAN 3 */
	HAL_NVIC_SetPriority(TIM4_IRQn, 10, 0);	//Set Main priority to 10 and sub-priority to 0.

	// Enable the Timer 2 interrupt
	HAL_NVIC_EnableIRQ(TIM4_IRQn);

	// Start Timer 2 base unit in interrupt mode
	HAL_TIM_Base_Start_IT(&TIM_Init);
}

/**
 * @brief Period elapsed callback in non blocking mode
 * @param htim: Pointer to a TIM_HandleTypeDef that contains the configuration information for the TIM module.
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM4) {
		count_interrupt++;
	}
}

//Override default mapping of this handler to Default_Handler
void TIM4_IRQHandler(void) {
	HAL_TIM_IRQHandler(&TIM_Init);
}

/**
 * @brief  Clears the buffer
 * @param  None
 * @retval None
 */
void clear_buffer(void) {
	int i;
	for (i = 9; i < 32; i++) {
		packetbuffer[i] = 0;
	}
}
