/**
 ******************************************************************************
 * @file		ex6_pwm_dynamic/src/main.c
 * @author	MDS
 * @date		08032017
 * @brief	 Produces a dynamic PWM  Waveform on pin D3.
 *			 See Section 18 (TIM3), P576 of the STM32F4xx Reference Manual.
 *
 *			NOTE: Refer to lineS 3163 and 4628 of the stm32f4xx_hal_tim.c, and
 *			lines 960 and 974 of stm32f4xx_hal_tim.h files. Refer to pages
 *			102-103 of the STM32F4-Technical-Training.pdf and page 590 of the
 *			STM32F4XX_reference.pdf. It's pretty good.
 ******************************************************************************
 *
 */

#include <s4380479_hal_pantilt.h>
#include <s4380479_hal_ledbar.h>
#include <math.h>

int main(void) {
	int metronome_min = -40;
	int metronome_max = 40;
	BRD_init();
	s4380479_hal_pantilt_init();
	s4380479_hal_ledbar_init();
	int pwm_value = 0;

	setbuf(stdout, NULL);
	char RxChar;
	while (1) {
		// Receive characters using getc
		RxChar = debug_getc();

		// Check if character is not null
		if (RxChar != '\0') {

#ifdef PRINTF_REFLECT
			debug_printf("Direction: %c\n", RxChar); // reflect byte using printf - must delay before calling printf again
#else
			debug_putc(RxChar); // reflect byte using putc - puts character into buffer
			debug_flush();
#endif
		}
		//HAL_Delay(1000);
		if (RxChar == 'd') {
			//get angle then move to that angle
			int angle = s4380479_hal_pantilt_pan_read();
			int error = angle % 5;

			if (error > 0) {
				angle += (5 - error);
			} else if (error < 0) {
				angle -= error;
			}
			s4380479_hal_pantilt_pan_write(angle + 5);
		} else if (RxChar == 'a') {
			int angle = s4380479_hal_pantilt_pan_read();
			int error = angle % 5;
			if (error < 0) {
				angle -= error;
			} else if (error) {
				angle += (5 - error);
			}
			s4380479_hal_pantilt_pan_write(angle - 5);
		} else if (RxChar == 'w') {
			//get angle then increment
			int angle = s4380479_hal_pantilt_tilt_read();
			int error = angle % 5;

			if (error > 0) {
				angle += (5 - error);
			} else if (error < 0) {
				angle -= error;
			}
			s4380479_hal_pantilt_tilt_write(angle + 5);
		} else if (RxChar == 's') {
			int angle = s4380479_hal_pantilt_tilt_read();
			int error = angle % 5;

			if (error < 0) {
				angle -= error;
			} else if (error) {
				angle += (5 - error);
			}
			s4380479_hal_pantilt_tilt_write(angle - 5);
		} else if (RxChar == 'm') {

			if (s4380479_hal_pantilt_pan_read() < 40) {
				s4380479_hal_pantilt_pan_write(metronome_min);
				pantilt_metronome(0);

			} else {
				s4380479_hal_pantilt_pan_write(metronome_max);
				pantilt_metronome(1);
			}

		} else if (RxChar == 't') {
			//float z = (PWM_DC_GET() - 72.5)/0.55;
		}

		//BRD_LEDRedToggle();
	}

	return 1;
}

