/**
 ******************************************************************************
 * @file    demo5/main.c
 * @author  Vivek Sahi
 * @date    03052018
 * @brief   FreeRTOS LED Flashing program.Creates a task to flash the onboard
 *			 Blue LED. Note the Idle task will also flash the Blue LED.
 *
 *			 NOTE: debug_printf is NOT thread-safe and will fail if called
 *			 directly, from multiple tasks.
 ******************************************************************************
 *
 */

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "debug_printf.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include <s4380479_hal_sysmon.h>
#include <s4380479_os_joystick.h>
#include "s4380479_os_printf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void Hardware_init();
void ApplicationIdleHook(void); /* The idle hook is used to blink the Blue 'Alive LED' every second */
void Task1_Task(void);
void Task2_Task(void);
void Task3_Task(void);

int taskTwoRunning = 1;
TaskHandle_t taskTwoHandler;

/* Task Priorities ------------------------------------------------------------*/
#define task1_PRIORITY					( tskIDLE_PRIORITY + 2 )
#define task2_PRIORITY					( tskIDLE_PRIORITY + 2 )
#define task3_PRIORITY					( tskIDLE_PRIORITY + 1)

/* Task Stack Allocations -----------------------------------------------------*/
#define mainLED_TASK_STACK_SIZE		( configMINIMAL_STACK_SIZE * 2 )
#define	TASK1_STACK_SIZE		( configMINIMAL_STACK_SIZE * 0.2 )

SemaphoreHandle_t s4380479_Joystick_Semaphore; /* Semaphore for joystick interrupt */

/**
 * @brief  Starts all the other tasks, then starts the scheduler.
 * @param  None
 * @retval None
 */
int main(void) {

	BRD_init();
	Hardware_init();
	s4380479_hal_sysmon_init();
	s4380479_os_joystick_init();
	s4380479_os_printf_init();
	s4380479_Joystick_Semaphore = xSemaphoreCreateBinary();

	/* Start the task to flash the LED. */
	xTaskCreate((void *) &Task1_Task, (const unsigned char *) "Task1",
	TASK1_STACK_SIZE, NULL, task1_PRIORITY, NULL);
	xTaskCreate((void *) &Task2_Task, (const unsigned char *) "Task2",
	mainLED_TASK_STACK_SIZE, NULL, task2_PRIORITY, &taskTwoHandler);
	xTaskCreate((void *) &Task3_Task, (const unsigned char *) "Task3",
	mainLED_TASK_STACK_SIZE, NULL, task3_PRIORITY, NULL);

	/* Start the scheduler.

	 NOTE : Tasks run in system mode and the scheduler runs in Supervisor mode.
	 The processor MUST be in supervisor mode when vTaskStartScheduler is
	 called.  The demo applications included in the FreeRTOS.org download switch
	 to supervisor mode prior to main being called.  If you are not using one of
	 these demo application projects then ensure Supervisor mode is used here. */

	vTaskStartScheduler();

	/* We should never get here as control is now taken by the scheduler. */
	return 0;
}

/**
 * @brief  LED Flashing Task.
 * @param  None
 * @retval None
 */
void Task1_Task(void) {

	s4380479_hal_sysmon_chan0_clr();

	for (;;) {
		s4380479_hal_sysmon_chan0_set();
		/* Toggle Blue LED */
		BRD_LEDBlueToggle();
		vTaskDelay(3);

		s4380479_hal_sysmon_chan0_clr();

		/* mandatiry delay */
		vTaskDelay(1);

	}
}

/**
 * @brief  LED Flashing Task.
 * @param  None
 * @retval None
 */
void Task2_Task(void) {

	s4380479_hal_sysmon_chan1_clr();

	for (;;) {
		s4380479_hal_sysmon_chan1_set();
		/* Toggle Blue LED */
		//BRD_LEDBlueToggle();
		vTaskDelay(3);
		s4380479_hal_sysmon_chan1_clr();

		/* mandatiry delay */
		vTaskDelay(1);

	}
}

/**
 * @brief  LED Flashing Task.
 * @param  None
 * @retval None
 */
void Task3_Task(void) {
	s4380479_hal_sysmon_chan2_clr();

	for (;;) {
		s4380479_hal_sysmon_chan2_set();
		//vTaskDelay(3);
		s4380479_hal_sysmon_chan2_clr();

		if (s4380479_Joystick_Semaphore != NULL) {
			/* See if we can obtain the PB semaphore. If the semaphore is not available
			 wait 10 ticks to see if it becomes free. */
			if ( xSemaphoreTake( s4380479_Joystick_Semaphore, 10 ) == pdTRUE) {
				if (taskTwoRunning) {
					vTaskDelete(taskTwoHandler);
				} else {
					xTaskCreate((void *) &Task2_Task,
							(const signed char *) "Task2",
							mainLED_TASK_STACK_SIZE, NULL, task2_PRIORITY,
							&taskTwoHandler);
				}

				taskTwoRunning ^= 1;
			}
		}
	}
}

/**
 * @brief  Hardware Initialisation.
 * @param  None
 * @retval None
 */
void Hardware_init(void) {

	portDISABLE_INTERRUPTS();	//Disable interrupts

	BRD_LEDInit();				//Initialise Blue LED
	BRD_LEDBlueOff();				//Turn off Blue LED

	portENABLE_INTERRUPTS();	//Enable interrupts

}

/**
 * @brief  Application Tick Task.
 * @param  None
 * @retval None
 */
void vApplicationTickHook(void) {

	BRD_LEDBlueOff();
}

/**
 * @brief  Idle Application Task
 * @param  None
 * @retval None
 */
void vApplicationIdleHook(void) {
	static portTickType xLastTx = 0;

	BRD_LEDBlueOff();

	for (;;) {

		/* The idle hook simply prints the idle tick count, every second */
		if ((xTaskGetTickCount() - xLastTx) > (1000 / portTICK_RATE_MS)) {

			xLastTx = xTaskGetTickCount();

			portENTER_CRITICAL();
			debug_printf("IDLE Tick %d\n", xLastTx);
			portEXIT_CRITICAL();

			/* Blink Alive LED */
			BRD_LEDBlueToggle();
		}
	}
}

/**
 * @brief  vApplicationStackOverflowHook
 * @param  Task Handler and Task Name
 * @retval None
 */
void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName) {
	/* This function will get called if a task overflows its stack.   If the
	 parameters are corrupt then inspect pxCurrentTCB to find which was the
	 offending task. */

	BRD_LEDBlueOff();
	(void) pxTask;
	(void) pcTaskName;

	for (;;)
		;
}

/**
 * @brief EXTI line detection callback
 * @param GPIO_Pin: Specifies the pins connected EXTI line
 * @retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	BaseType_t xHigherPriorityTaskWoken;

	if (GPIO_Pin == BRD_USER_BUTTON_PIN) {
		/* Is it time for another Task() to run? */
		xHigherPriorityTaskWoken = pdFALSE;

		if (s4380479_Joystick_Semaphore != NULL) { /* Check if semaphore exists */
			xSemaphoreGiveFromISR(s4380479_Joystick_Semaphore,
					&xHigherPriorityTaskWoken); /* Give PB Semaphore from ISR*/
		}
		/* Perform context switching, if required. */
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}

//Override default mapping of this handler to Default_Handler
void EXTI15_10_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(BRD_USER_BUTTON_PIN);
}

