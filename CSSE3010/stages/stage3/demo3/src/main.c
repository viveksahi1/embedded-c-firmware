/**
 ******************************************************************************
 * @file    ex_HAL/main.c
 * @author  MDS
 * @date    27112017
 * @brief   Nucleo429ZI onboard LED flashing example.
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <s4380479_hal_joystick.h>
#include <s4380479_hal_ir.h>
#include <s4380479_hal_ir_coms.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

unsigned int prev_signal_time;
unsigned int last_Output;
int start_time;
int freq;
//int store[50];
int coun = 0;
int mode = 1;
int bits[286], bit_pos = 0, i;

/**
 * @brief  Main program - flashes onboard LEDs
 * @param  None
 * @retval None
 */
int main(void) {

	BRD_init();			//Initalise Board
	s4380479_hal_joystick_init();
	s4380479_hal_ir_init();
	s4380479_hal_ir_coms_init();
	//Hardware_init();
	setbuf(stdout, NULL);
	char RxChar;
	int encode[22];
	s4380479_hal_reset_frq_counter();
	int num_received_hex = 0;
	int char_pos;
	uint8_t input[44];

	int msg = 0;
	char hex_value[12][5];
	int* msg_data[12];
	//printf("%d\r\n", __LINE__);

	while (1) {
		//s4380479_hal_manchester_encode('4', 'A', encode);
		//s4380479_hal_ir_transmit(encode);
		/////////////transmitting strings
		RxChar = debug_getc();
		if (RxChar != '\0') {
			debug_printf("%c", RxChar);	// reflect byte using printf - must delay before calling printf again
			// if not 'enter' key and ! payload full
			if (RxChar != 13 && msg < 12) {
				IR_message_buffer[msg++] = RxChar;
			} else if (RxChar == 13) {
				for (int i = 0; i < msg; i++) {
					sprintf(hex_value[i], "%0X", (int) IR_message_buffer[i]);
					printf("hexconverted:%c %c\r\n", hex_value[i][0],
							hex_value[i][1]);
				}
				s4380479_hal_manchester_encode('0', '2', encode,0);
				s4380479_hal_ir_transmit(encode);
				HAL_Delay(50);
				for (int i = 0; i < msg; i++) {
					s4380479_hal_manchester_encode(hex_value[i][0],
							hex_value[i][1], encode,0);
					s4380479_hal_ir_transmit(encode);
					HAL_Delay(50);
				}
				s4380479_hal_manchester_encode('0', '3', encode,0);
				s4380479_hal_ir_transmit(encode);
				init_s4380479_msg_buffer();
				msg = 0;
				printf("end of transmission\r\n");
			}
		}
		//printf("%d\r\n", __LINE__);
		//receving strings
		if (s4380479_hal_get_frq_counter() > 50) {
			HAL_Delay(2000); //wait to receive the complete message
			printf("signal detected %d\r\n", s4380479_hal_get_frq_counter()); //signal detected
			if (s4380479_hal_get_frq_counter() >= 40) {
				int* store = s4380479_hal_get_frq();
				for (int i = 0; i < s4380479_hal_get_frq_counter(); i++) { // for each frequency
					if (store[i] == 0 && i == 0) {
						//starting bit 1
						bits[bit_pos++] = 0;
					} else if (store[i] < 100) {
						//ignore
						bits[bit_pos++] = mode ^ 1;
						bits[bit_pos++] = mode ^ 1;
						//printf("bits_posdasda->%d:%d\r\n", bit_pos,i);
						//mode ^= 1;
					} else {
						int j = 1000 / (store[i] * 4);
						while (j) {
							bits[bit_pos++] = mode ^ 1;
							j--;
						}
					}
					mode ^= 1; //next trigger mode
				}

				bits[bit_pos++] = 0;
				//printf("%d data:", bit_pos);
				for (int i = 0; i < bit_pos; i++) {
					if (i % 22 == 0) {
						//printf("  ");
					}
					//printf("%d", bits[i]);
				}
				//printf("\r\n"); // data rceived
				num_received_hex = ((bit_pos - 44) / 22);
				for (int i = 0; i < num_received_hex; i++) { //number of char to decode
					for (int j = 0; j < 4; j++) {
						char_pos = (i * 22) + (4 * j) + 25;
						input[(i * 4) + j] = bits[char_pos + 1] << 3
								| bits[char_pos + 2] << 2
								| bits[char_pos + 3] << 1 | bits[char_pos + 4];
						//printf("decimal:%d i:%d j:%d\r\n:", input[(i * 4) + j], i, j);
					}
				}
				s4380479_hal_manchester_decode(input, num_received_hex * 16,0);
				bit_pos = 0; //reset bit position
				s4380479_hal_reset_frq_counter();
				printf("end of receiving->%d   freq_count->%d \r\n", __LINE__,
						s4380479_hal_get_frq_counter());

			}
		}
		/* for receving single char */
		/*num_received_hex = bit_pos - 5;
		 for (int i = 0; i < num_received_hex / 4; i++) { //for each hex nibble
		 input[i] = bits[(i * 4) + 4] << 3 | bits[(i * 4) + 5] << 2
		 | bits[(i * 4) + 6] << 1 | bits[(i * 4) + 7];
		 }*


		 printf("decoded:%c\r\n",
		 s4380479_hal_manchester_decode(input, num_received_hex));
		 bit_pos = 0; //reset bit position
		 s4380479_hal_reset_frq_counter();
		 }
		 //s4380479_hal_reset_frq_counter();
		 /*RxChar = debug_getc();
		 //HAL_Delay(1000);
		 if (RxChar == 'a') {
		 s4380479_hal_ir_carrieron();
		 } else if (RxChar == 'b') {
		 s4380479_hal_ir_carrieroff();
		 } else if (RxChar == 'c') {
		 s4380479_hal_ir_datamodulation_set()
		 ;
		 } else if (RxChar == 'd') {
		 s4380479_hal_ir_datamodulation_clr()
		 ;
		 }*/
		//HAL_Delay(1000);

	}
	return 1;
}

