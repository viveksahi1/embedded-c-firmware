/**
  ******************************************************************************
  * File Name          : app_x-cube-ble1.h
  * Description        : Header file
  *                    
  ******************************************************************************
  *
  * COPYRIGHT 2019 STMicroelectronics
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APP_X_CUBE_BLE1_H
#define __APP_X_CUBE_BLE1_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l4_b_l475e_iot01a.h"
#include "hci_tl.h"
#include "sample_service.h"
#include "sensor_service.h"
#include "role_type.h"
#include "bluenrg_utils.h"
#include "bluenrg_gatt_server.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "bluenrg_hal_aci.h"
#include "s4380479_os_log_message.h"
#include "cmsis_os.h"
#include "s4380479_ble_adv.h"

// #define BDADDR_SIZE 6

/* Exported Functions --------------------------------------------------------*/
// void s4380479_BLE_SCAN_INIT(void);
void s4380479_BLE_CLIENT_SERVER(BLE_RoleTypeDef BLE_Role);
// void s4380479_BlueNRG_Scan_Process(void);
// void s4380479_BlueNRG_Conn_Process(BLE_RoleTypeDef BLE_Role);
// void s4380479_cli_Scan_Process(void);
void s4380479_scan_user_notify(void *pData);
// void s4380479_execute_scan(void);
void s4380479_User_Init(void);
// void s4380479_printf_devices_found(uint8_t status, uint8_t *addr_type, uint8_t *addr,
//                                uint8_t *data_length, uint8_t *data,
//                                uint8_t *RSSI);
void s4380479_conn_user_notify(void *pData);
void s4380479_init_BLE(void);


#endif 

