/**
  ******************************************************************************
  * @file    sensor_service.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    04-July-2014
  * @brief   Add a sample service using a vendor specific profile.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include "sensor_service.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"

#include <stdio.h>

/** @addtogroup Applications
 *  @{
 */

/** @addtogroup SensorDemo
 *  @{
 */

/** @defgroup SENSOR_SERVICE
 * @{
 */

/** @defgroup SENSOR_SERVICE_Private_Variables
 * @{
 */
/* Private variables ---------------------------------------------------------*/
__IO uint32_t sens_connected = FALSE;
__IO uint8_t set_sens_connectable = 1;
__IO uint16_t sens_connection_handle = 0;
__IO uint8_t sens_notification_enabled = FALSE;
__IO AxesRaw_t axes_data = {0, 0, 0};
uint16_t accServHandle, freeFallCharHandle, accCharHandle;
/**
 * @}
 */

/** @defgroup SENSOR_SERVICE_Private_Macros
 * @{
 */
/* Private macros ------------------------------------------------------------*/
#define COPY_UUID_128(uuid_struct, uuid_15, uuid_14, uuid_13, uuid_12, uuid_11, uuid_10, uuid_9, uuid_8, uuid_7, uuid_6, uuid_5, uuid_4, uuid_3, uuid_2, uuid_1, uuid_0) \
  do                                                                                                                                                                     \
  {                                                                                                                                                                      \
    uuid_struct[0] = uuid_0;                                                                                                                                             \
    uuid_struct[1] = uuid_1;                                                                                                                                             \
    uuid_struct[2] = uuid_2;                                                                                                                                             \
    uuid_struct[3] = uuid_3;                                                                                                                                             \
    uuid_struct[4] = uuid_4;                                                                                                                                             \
    uuid_struct[5] = uuid_5;                                                                                                                                             \
    uuid_struct[6] = uuid_6;                                                                                                                                             \
    uuid_struct[7] = uuid_7;                                                                                                                                             \
    uuid_struct[8] = uuid_8;                                                                                                                                             \
    uuid_struct[9] = uuid_9;                                                                                                                                             \
    uuid_struct[10] = uuid_10;                                                                                                                                           \
    uuid_struct[11] = uuid_11;                                                                                                                                           \
    uuid_struct[12] = uuid_12;                                                                                                                                           \
    uuid_struct[13] = uuid_13;                                                                                                                                           \
    uuid_struct[14] = uuid_14;                                                                                                                                           \
    uuid_struct[15] = uuid_15;                                                                                                                                           \
  } while (0)

#define COPY_ACC_SERVICE_UUID(uuid_struct) COPY_UUID_128(uuid_struct, 0x02, 0x36, 0x6e, 0x80, 0xcf, 0x3a, 0x11, 0xe1, 0x9a, 0xb4, 0x00, 0x02, 0xa5, 0xd5, 0xc5, 0x1b)
#define COPY_FREE_FALL_UUID(uuid_struct) COPY_UUID_128(uuid_struct, 0xe2, 0x3e, 0x78, 0xa0, 0xcf, 0x4a, 0x11, 0xe1, 0x8f, 0xfc, 0x00, 0x02, 0xa5, 0xd5, 0xc5, 0x1b)
#define COPY_ACC_UUID(uuid_struct) COPY_UUID_128(uuid_struct, 0x34, 0x0a, 0x1b, 0x80, 0xcf, 0x4b, 0x11, 0xe1, 0xac, 0x36, 0x00, 0x02, 0xa5, 0xd5, 0xc5, 0x1b)

/* Store Value into a buffer in Little Endian Format */
#define STORE_LE_16(buf, val) (((buf)[0] = (uint8_t)(val)), \
                               ((buf)[1] = (uint8_t)(val >> 8)))
/**
 * @}
 */

/** @defgroup SENSOR_SERVICE_Exported_Functions 
 * @{
 */
/**
 * @brief  Add an accelerometer service using a vendor specific profile.
 *
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_Acc_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_ACC_SERVICE_UUID(uuid);
  ret = aci_gatt_add_serv(UUID_TYPE_128, uuid, PRIMARY_SERVICE, 7,
                          &accServHandle);
  if (ret != BLE_STATUS_SUCCESS)
    goto fail;

  COPY_FREE_FALL_UUID(uuid);
  ret = aci_gatt_add_char(accServHandle, UUID_TYPE_128, uuid, 1,
                          CHAR_PROP_NOTIFY, ATTR_PERMISSION_NONE, 0,
                          16, 0, &freeFallCharHandle);
  if (ret != BLE_STATUS_SUCCESS)
    goto fail;

  COPY_ACC_UUID(uuid);
  ret = aci_gatt_add_char(accServHandle, UUID_TYPE_128, uuid, 6,
                          CHAR_PROP_NOTIFY | CHAR_PROP_READ,
                          ATTR_PERMISSION_NONE,
                          GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                          16, 0, &accCharHandle);
  if (ret != BLE_STATUS_SUCCESS)
    goto fail;
  printf("Service ACC added. Handle 0x%04X, Free fall Charac handle: 0x%04X, Acc Charac handle: 0x%04X\r\n", accServHandle, freeFallCharHandle, accCharHandle);
  return BLE_STATUS_SUCCESS;

fail:
  printf("Error while adding ACC service.\r\n");
  return BLE_STATUS_ERROR;
}

/**
 * @brief  Send a notification for a Free Fall detection.
 *
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Free_Fall_Notify(void)
{
  uint8_t val;
  tBleStatus ret;

  val = 0x01;
  ret = aci_gatt_update_char_value(accServHandle, freeFallCharHandle, 0, 1,
                                   &val);

  if (ret != BLE_STATUS_SUCCESS)
  {
    printf("Error while updating FFall characteristic.\r\n");
    return BLE_STATUS_ERROR;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update acceleration characteristic value.
 *
 * @param  Structure containing acceleration value in mg
 * @retval Status
 */
tBleStatus Acc_Update(AxesRaw_t *data)
{
  tBleStatus ret;
  uint8_t buff[6];

  STORE_LE_16(buff, data->AXIS_X);
  STORE_LE_16(buff + 2, data->AXIS_Y);
  STORE_LE_16(buff + 4, data->AXIS_Z);

  ret = aci_gatt_update_char_value(accServHandle, accCharHandle, 0, 6, buff);

  if (ret != BLE_STATUS_SUCCESS)
  {
    printf("Error while updating ACC characteristic.\r\n");
    return BLE_STATUS_ERROR;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Puts the device in connectable mode.
 *         If you want to specify a UUID list in the advertising data, those data can
 *         be specified as a parameter in aci_gap_set_discoverable().
 *         For manufacture data, aci_gap_update_adv_data must be called.
 * @param  None 
 * @retval None
 */
/**
 * @brief  Puts the device in connectable mode.
 *         If you want to specify a UUID list in the advertising data, those data can
 *         be specified as a parameter in aci_gap_set_discoverable().
 *         For manufacture data, aci_gap_update_adv_data must be called.
 * @param  None 
 * @retval None
 */
void setConnectable(void)
{
  tBleStatus ret;

  const char local_name[] = {AD_TYPE_COMPLETE_LOCAL_NAME, '4', '3', '8', '0', '4', '7', '9', '1'};
  /* disable scan response */
  hci_le_set_scan_resp_data(0, NULL);
  printf("General Discoverable Mode.\r\n");

  ret = aci_gap_set_discoverable(ADV_DATA_TYPE, ADV_INTERV_MIN, ADV_INTERV_MAX, PUBLIC_ADDR,
                                 NO_WHITE_LIST_USE, sizeof(local_name), local_name, 0, NULL, 0, 0);
  if (ret != BLE_STATUS_SUCCESS)
  {
    printf("Error while setting discoverable mode (%d)\r\n", ret);
  }

  // ret = aci_gap_update_adv_data(5, manuf_data);
  // printf("this return %d\n", ret);
}

/**
 * @brief  This function is called when there is a LE Connection Complete event.
 * @param  uint8_t Address of peer device
 * @param  uint16_t Connection handle
 * @retval None
 */
void GAP_Sens_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle)
{
  sens_connected = TRUE;
  sens_connection_handle = handle;

  printf("Connected to device:");
  for (uint32_t i = 5; i > 0; i--)
  {
    printf("%02X-", addr[i]);
  }
  printf("%02X\r\n", addr[0]);
}

/**
 * @brief  This function is called when the peer device gets disconnected.
 * @param  None 
 * @retval None
 */
void GAP_Sens_DisconnectionComplete_CB(void)
{
  sens_connected = FALSE;
  printf("Disconnected\r\n");
  /* Make the device connectable again. */
  set_sens_connectable = TRUE;
  sens_notification_enabled = FALSE;
}

/**
 * @brief  Read request callback.
 * @param  uint16_t Handle of the attribute
 * @retval None
 */
void Read_Request_CB(uint16_t handle)
{
  if (handle == accCharHandle + 1)
  {
    Acc_Update((AxesRaw_t *)&axes_data);
  }

  if (sens_connection_handle != 0)
    aci_gatt_allow_read(sens_connection_handle);
}

/**
 * @brief  Callback processing the ACI events.
 * @note   Inside this function each event must be identified and correctly
 *         parsed.
 * @param  void* Pointer to the ACI packet
 * @retval None
 */
void s4380479_conn_user_notify(void *pData)
{
  hci_uart_pckt *hci_pckt = pData;
  /* obtain event packet */
  hci_event_pckt *event_pckt = (hci_event_pckt *)hci_pckt->data;

  if (hci_pckt->type != HCI_EVENT_PKT)
  {
    return;
  }

  switch (event_pckt->evt)
  {

  case EVT_DISCONN_COMPLETE:
  {
    GAP_Sens_DisconnectionComplete_CB();
  }
  break;

  case EVT_LE_META_EVENT:
  {
    evt_le_meta_event *evt = (void *)event_pckt->data;

    switch (evt->subevent)
    {
    case EVT_LE_CONN_COMPLETE:
    {
      evt_le_connection_complete *cc = (void *)evt->data;
      GAP_Sens_ConnectionComplete_CB(cc->peer_bdaddr, cc->handle);
    }
    break;
    }
  }
  break;

  case EVT_VENDOR:
  {
    evt_blue_aci *blue_evt = (void *)event_pckt->data;
    switch (blue_evt->ecode)
    {

    case EVT_BLUE_GATT_READ_PERMIT_REQ:
    {
      evt_gatt_read_permit_req *pr = (void *)blue_evt->data;
      Read_Request_CB(pr->attr_handle);
    }
    break;
    }
  }
  break;
  }
}
