/**
  ******************************************************************************
  * File Name          : app_x-cube-ble1.c
  * Description        : Implementation file
  *             
  ******************************************************************************
  *
  * COPYRIGHT 2019 STMicroelectronics
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "app_x-cube-ble1.h"

/* Private defines -----------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
uint8_t bnrg_expansion_board = IDB05A1; /* at startup, suppose the X-NUCLEO-IDB04A1 is used */
static volatile uint8_t user_button_init_state = 1;
static volatile uint8_t user_button_pressed = 0;

// const char *name = "4380479";
uint8_t CLIENT_BDADDR[] = {0x00, 0x00, 0x34, 0x23, 0x58, 0x43}; //address to connect to
// uint8_t SERVER_BDADDR[] = {0x00, 0x00, 0x91, 0x47, 0x80, 0x43};
// uint8_t bdaddr[BDADDR_SIZE];

uint8_t server_detected = 0;
extern AxesRaw_t axes_data;
int16_t pDataXYZ[3] = {0};

// int advertising_status = 1;

extern volatile uint8_t set_connectable;
extern volatile uint8_t sens_connected;
extern volatile uint8_t set_sens_connectable;
extern volatile int connected;
extern volatile uint8_t notification_enabled;

extern volatile uint8_t end_read_tx_char_handle;
extern volatile uint8_t end_read_rx_char_handle;
extern volatile uint16_t connection_handle;

uint16_t service_handle, dev_name_char_handle, appearance_char_handle;

/* Private function prototypes -----------------------------------------------*/
void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle);
void s4380479_connection_server(void);
void s4380479_cli_User_Process(AxesRaw_t *p_axes, BLE_RoleTypeDef BLE_Role);
void BSP_PB_Callback(Button_TypeDef Button);

#if PRINT_CSV_FORMAT
extern volatile uint32_t ms_counter;
/**
 * @brief  This function is a utility to print the log time
 *         in the format HH:MM:SS:MSS (DK GUI time format)
 * @param  None
 * @retval None
 */
void print_csv_time(void)
{
    uint32_t ms = HAL_GetTick();
    PRINT_CSV("%02d:%02d:%02d.%03d", ms / (60 * 60 * 1000) % 24, ms / (60 * 1000) % 60, (ms / 1000) % 60, ms % 1000);
}
#endif

/**
 * @brief  Initialize User process.
 *
 * @param  None
 * @retval None
 */
void s4380479_User_Init(void)
{
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);
    BSP_LED_Init(LED2);

    BSP_COM_Init(COM1);
    // BSP_ACCELERO_Init();
}

/**
 * @brief  Make the device connectable
 * @param  None 
 * @retval None
 */
void s4380479_connection_server(void)
{
    tBleStatus ret;
    s4380479_os_log_message(ERROR_MSG, "ATTEMPTING client connection");
    ret = aci_gap_create_connection(SCAN_P, SCAN_L, PUBLIC_ADDR, CLIENT_BDADDR, PUBLIC_ADDR, CONN_P1, CONN_P2, 0,
                                    SUPERV_TIMEOUT, CONN_L1, CONN_L2);
    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "Error while starting connection. %02x\n", ret);
        osDelay(100);
    }
}

/**
  * @brief  BSP Push Button callback
  * @param  Button Specifies the pin connected EXTI line
  * @retval None.
  */
void BSP_PB_Callback(Button_TypeDef Button)
{
    /* Set the User Button flag */
    user_button_pressed = 1;
}
/* USER CODE END PV */
