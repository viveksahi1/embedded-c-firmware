/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_file.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   FILE CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_CLI_FILE_H
#define S4380479_CLI_FILE_H

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"
#include "board.h"
#include "argtable2.h"
#include <stdlib.h>
#include "s4380479_hal_qspi.h"
#include "s4380479_os_imu_log.h"

/* Private typedef -----------------------------------------------------------*/
#define fCreateFile 0
#define fViewFile 1
#define fDeleteFile 2 
#define fMoveFile 3
#define fHelpFile 4
#define fEndFile 5

#define progFile "file"

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_file_cmd();
void** s4380479_get_file_cmd();
void handle_file(int fileCreate, char *createName,
                int fileView, char *viewName,
                int fileDel, char *delName,
                int fileMove, char *moveFile, char *moveDir);

#endif
