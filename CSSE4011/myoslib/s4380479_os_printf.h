/**
 ******************************************************************************
6* @file    myoslib/s4380479_os_printf.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   OS printf driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPrintf(void) - task to receive from printf
 ******************************************************************************
 */

#ifndef S4380479_OS_PRINTF_H
#define S4380479_OS_PRINTF_H

/* Includes ------------------------------------------------------------------*/
// #include "stm32l4xx_hal_conf.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "main.h"
#include "board.h"

#include "s4380479_debug_print.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define rtosPrintf(fmt, args...)		s4380479_os_printf(fmt, args)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
QueueHandle_t s4380479_QueuePrintf;

/* External function prototypes -----------------------------------------------*/
void s4380479_os_printf_init(void);
void s4380479_os_printf(char* fmt, ...);

#endif

