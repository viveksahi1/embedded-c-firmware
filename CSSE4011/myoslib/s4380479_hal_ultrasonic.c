/**
 ******************************************************************************
* @file    myoslib/s4380479_hal_ultrasonic.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal ultrasonic drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_ultrasonic.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/

/**
 *  Creates a blocking delay
 */
void s4380479_Delay_us(uint16_t duration)
{
  static uint32_t i = 0, j = 0;

  for (i; i < duration; i++)
  {
    for (j = 0; j < 1; j++)
      ;
  }
}
/**
 *  Returns the echo  time
 */
uint32_t s4380479_get_echo_time(void)
{
  uint32_t echo_time = 0;
  int status = 0, error = 0;

  HAL_GPIO_WritePin(ARD_D8_GPIO_Port, ARD_D8_Pin, 1);
  s4380479_Delay_us(5);
  HAL_GPIO_WritePin(ARD_D8_GPIO_Port, ARD_D8_Pin, 0);
  osDelay(1);
  while ((status = HAL_GPIO_ReadPin(ARD_D7_GPIO_Port, ARD_D7_Pin)) == 0)
  {
    if (error++ > 1000)
    {
      break;
    }
  }
  while (HAL_GPIO_ReadPin(ARD_D7_GPIO_Port, ARD_D7_Pin) == 1)
  {
    echo_time++;
    s4380479_Delay_us(1);
  }
  s4380479_os_log_message(ERROR_MSG, "echo duration %d\r\n", echo_time / 148);
}