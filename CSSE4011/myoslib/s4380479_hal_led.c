/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_led.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal led drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_led.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/

/**
 * This method is used to update the led status on,off,toggle
 */
void s4380479_hal_led(int led, int status, int toggle)
{

    if (toggle) {
        s4380479_os_log_message(DEBUG_MSG, "Toggling LED: %d", led);
    } else {
        s4380479_os_log_message(DEBUG_MSG, "Setting LED %d, status to %d", led, status);
    }
    //handle led 2
    if (led == LED_2)
    {
        if (toggle)
        {
            HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
        } else {
            HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, status);
        }
        s4380479_os_log_message(LOG_MSG, "LED: %d level %d", led, HAL_GPIO_ReadPin(LED2_GPIO_Port, LED2_Pin));
        return;

    }
    else if (led == LED_3)     //handle led 2
    {
        if (toggle)
        {
            HAL_GPIO_TogglePin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin);
        } else if (status) {
            HAL_GPIO_WritePin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin, LED_HIGH);
        } else {
            HAL_GPIO_WritePin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin, LED_LOW);
        }
    } else if (led == LED_4)     //handle led 2
    {
        if (toggle)
        {
            HAL_GPIO_TogglePin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin);
        } else if (!status) {
            HAL_GPIO_WritePin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin, LED_HIGH);
        } else {
            HAL_GPIO_WritePin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin, LED_LOW);
        }
    } else {
        s4380479_os_log_message(ERROR_MSG, "Invalid LED. Available leds <2,3,4>");
        return;
    }

    s4380479_os_log_message(LOG_MSG, "LED: %d level %d", led, HAL_GPIO_ReadPin(LED3_WIFI__LED4_BLE_GPIO_Port, LED3_WIFI__LED4_BLE_Pin));

    return;
}
