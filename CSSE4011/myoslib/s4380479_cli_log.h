/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_led.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   LED CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_CLI_LOG_H
#define S4380479_CLI_LOG_H

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"
#include "board.h"
#include "argtable2.h"
#include <stdlib.h>

/* Private typedef -----------------------------------------------------------*/
#define cFilterLog 0
#define cHelpLog 1
#define cOnLog 2
#define cEndLog 3
#define progLog "log"

#define fErrCmd     "error"
#define fLogCmd     "log"
#define fDebugCmd   "debug"
#define fAllCmd     "all"
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_log_cmd();
void** s4380479_get_log_cmd();
void handle_log(int filterLog, const char *mode, const char *onLog);

#endif
