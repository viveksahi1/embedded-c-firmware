/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_advertise.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli advertise
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_advertise.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define ADVERTISE_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 4)
#define ADVERTISE_PRIORITY (tskIDLE_PRIORITY + 8)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableAdv; //variable to store adv cli cmds

/**
 * This method allcoates the memory for the argtable to store adv cmds.
 */
void allocate_adv_argtable()
{
    argtableAdv = malloc(sizeof(struct arg_str *) + sizeof(struct arg_lit *) + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * 10);
}

/**
 * Initialise the time CLI cmds
 */
void s4380479_init_adv_cmd()
{
    /** adv CLI CMD */
    // struct arg_rex *advCmd = arg_rex1(NULL, NULL, "adv", NULL, REG_ICASE, NULL);
    struct arg_str *Adv = arg_str1("d", "adv", "{init, on, off}", "Toggle advertising status");
    struct arg_lit *helpAdv = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *endAdv = arg_end(20);

    //redirect error func
    Adv->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    helpAdv->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    endAdv->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_adv_argtable();
    argtableAdv[cAdvLog] = Adv;
    argtableAdv[dHelpAdv] = helpAdv;
    argtableAdv[dEndAdv] = endAdv;
}

/**
 * This method return the artable for the adv cmd
 */
void **s4380479_get_adv_cmd()
{
    return argtableAdv;
}

/** 
 * freertos recv thread
 */
void s4380479_TaskAdvertise(void)
{
    // s4380479_init_BLE();
    s4380479_ADV_Init();

    /* Infinite loop */
    for (;;)
    {
        // s4380479_ADV_process();
        s4380479_ADV_Init();
        s4380479_update_advertising();
        HAL_GPIO_TogglePin(LED2_GPIO_PORT, LED2_PIN);
        //  s4380479_os_log_message(ERROR_MSG, "KILLING TASK");
        osDelay(1000);
    }
}

/**
 * This is the callback function to hadndle adv cmds
 */
void handle_adv(int Adv, const char *mode)
{
    s4380479_os_log_message(DEBUG_MSG, "Filtered Output: %d, Mode: %s",
                            Adv, mode);
    //check if advertise
    if (Adv)
    {
        // compare all the inputs with availabe adv cmds
        if (!strncmp(mode, advInitCmd, sizeof(advInitCmd)))
        {
            if (get_scan_status())
            {
                s4380479_os_log_message(ERROR_MSG, "KILLING TASK");
                vTaskDelete(get_conn_handle());
                set_scan_status(0);
            }
            s4380479_os_log_message(CLI_LOG, "Advertising enabled");
            if (xTaskCreate((TaskFunction_t)&s4380479_TaskAdvertise, "BLERECV", ADVERTISE_TASK_STACK_SIZE, NULL,
                            ADVERTISE_PRIORITY, get_advertise_handle_addr()) != pdPASS)
            {
                s4380479_os_log_message(CLI_LOG, "failed to create advertise task");
            } else {
                set_recv_status(1);
            }
        }
        else if (!strncmp(mode, advOnCmd, sizeof(advOnCmd)))
        {
            s4380479_toggle_advertising(ADVR_ON);
        }
        else if (!strncmp(mode, advOffCmd, sizeof(advOffCmd)))
        {
            s4380479_toggle_advertising(ADVR_OFF);
        }
        else
        {
            s4380479_os_log_message(ERROR_MSG, "Errror processing advertise cmd");
        }
    }
}
