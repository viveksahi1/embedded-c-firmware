/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_scan.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli scan
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_scan.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SCAN_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 4)
#define SCAN_PRIORITY (tskIDLE_PRIORITY + 5)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableScan; //variable to store scan cli cmds

/**
 * This method allcoates the memory for the argtable to store scan cmds.
 */
void allocate_scan_argtable()
{
    argtableScan = malloc(sizeof(struct arg_lit *) + sizeof(struct arg_end *) +
                          sizeof(void *) + sizeof(char) * MEM_OVF_BUF);
}

/**
 * Initialise the scan CLI cmds
 */
void s4380479_init_scan_cmd()
{
    /** TIME CLI CMD */
    // struct arg_rex *scanCmd = arg_rex1(NULL, NULL, "scan", NULL, REG_ICASE, NULL);
    struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *end = arg_end(20);

    //redirects the error function
    help->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    end->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_scan_argtable();
    argtableScan[helpScan] = help;
    argtableScan[endScan] = end;
}

/**
 * This method return the artable for the scan cmd
 */
void **s4380479_get_scan_cmd()
{
    return argtableScan;
}

/** 
 * freertos scan thread
 */
void s4380479_TaskScan(void)
{
    s4380479_init_BLE();
    s4380479_BLE_SCAN_INIT();
    /* Infinite loop */
    for (;;)
    {
        HAL_GPIO_TogglePin(LED2_GPIO_PORT, LED2_PIN);
        s4380479_SCAN_process();
        osDelay(100);
    }
}

/**
 * This is the callback function to hadndle scan cmds
 */
void get_sys_scan(void)
{
    if (get_recv_status()) {
        s4380479_os_log_message(ERROR_MSG, "KILLING TASK");
        vTaskDelete(get_advertise_handle());
        set_recv_status(0);
    }

    s4380479_os_log_message(DEBUG_MSG, "Scan cmd detected");
    if (xTaskCreate((TaskFunction_t)&s4380479_TaskScan, "BLESCAN", SCAN_TASK_STACK_SIZE, NULL,
                    SCAN_PRIORITY, get_conn_handle_addr()) != pdPASS)
    {
        s4380479_os_log_message(ERROR_MSG, "failed to create scan task");
    } else {
        set_scan_status(1);
    }

}
