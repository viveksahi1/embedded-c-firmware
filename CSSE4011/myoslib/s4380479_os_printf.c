/**
 ******************************************************************************
 * @file    myoslib/s4380479_os_printf.c
 * @author  Vivek Sahi - 43804791
 * @date    11-03-2019
 * @brief   OS printf drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPrintf(void) - task to receive from printf
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_printf.h"
#include "s4380479_debug_print.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define buffer 500
#define PRINT_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 2)
#define PRINT_PRIORITY (tskIDLE_PRIORITY + 8)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char dataToPrint[buffer];
char *dataPtr;

/**
 * Initialises the pins used for the system monitor
 */
void s4380479_TaskPrintf(void)
{
    char dataToRecv[buffer];

    while (1)
    {
        if (s4380479_QueuePrintf != NULL)
        {

            if (xQueueReceive(s4380479_QueuePrintf, &dataToRecv, __UINT32_MAX__))
            {
                debug_printf(ANSI_COLOR_YELLOW "%s\r\n" ANSI_COLOR_RESET, &dataToRecv);
            }
        }
    }
}

/** task safe printf function */
void s4380479_os_printf(char *fmt, ...)
{
    //receive arguments
    va_list args;
    va_start(args, fmt);

    dataPtr = dataToPrint;

    vsprintf(dataPtr, fmt, args);
    va_end(args);
     printf("%s\r\n",dataToPrint);

    if (xQueueSend(s4380479_QueuePrintf, dataToPrint,
                   (portTickType)100) != pdPASS)
    {
        //failed
        printf("%s\r\n", "failied to send queue");
    }
}

/**
 * Initialises the task for printing messages to stdout
 */
void s4380479_os_printf_init(void)
{
    //init Queue
    s4380479_QueuePrintf = xQueueCreate(5, sizeof(char) * buffer);
    
    if (xTaskCreate((TaskFunction_t)&s4380479_TaskPrintf, "PRINTF", PRINT_TASK_STACK_SIZE, NULL,
                    PRINT_PRIORITY, NULL) != pdPASS)
    {
        printf("%s\r\n", "failed to create debug printf task");
    } 
}
