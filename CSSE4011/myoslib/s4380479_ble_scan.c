/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_ble_scan.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli ble scan
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_ble_scan.h"
#include "es_wifi_io.h"
#include "FreeRTOS.h"
#include "task.h"


#define SCAN_EVENT_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 4)
#define SCAN_EVENT_PRIORITY (tskIDLE_PRIORITY + 5)
osThreadId scanningTaskHandle;
TaskHandle_t wifiTaskHandle;
#define RSSI_256    256
/* Private variables ---------------------------------------------------------*/
uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
extern volatile uint8_t set_connectable;
extern volatile int connected;
extern volatile uint8_t end_read_tx_char_handle;
extern volatile uint8_t end_read_rx_char_handle;
extern volatile uint8_t notification_enabled;
uint8_t CLIENT_DADDR[] = {0x25, 0x26, 0x44, 0xa0, 0xd1, 0x59}; //address to connect to

uint8_t NODE_1_DADDR[] = {0x00, 0x00, 0x91, 0x47, 0x80, 0x43};
uint8_t NODE_2_DADDR[] = {0x00, 0x01, 0x91, 0x47, 0x80, 0x43};
uint8_t NODE_3_DADDR[] = {0x00, 0x10, 0x91, 0x47, 0x80, 0x43};
uint8_t NODE_4_DADDR[] = {0x00, 0x11, 0x91, 0x47, 0x80, 0x43};

int node1_data = 0, node2_data = 0, node3_data = 0, node4_data = 0;

int node1_rssi = 0, node2_rssi = 0, node3_rssi = 0, node4_rssi = 0;
int node1_ultra = 0, node2_ultra = 0, node3_ultra = 0, node4_ultra = 0;


uint8_t TxWifiData[1000] = "";

int device_count = 0;

void s4380479_os_wifi(void);

void s4380479_BLE_SCAN_INIT(void)
{

    int ret;

    s4380479_os_log_message(LOG_MSG, "INIT SCANNING");
    //init device as client or server
    ret = aci_gap_init_IDB05A1(GAP_OBSERVER_ROLE_IDB05A1, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);

    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "GAP_Init failed.\r\n");
    }

    ret = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED,
                                       OOB_AUTH_DATA_ABSENT,
                                       NULL,
                                       7,
                                       16,
                                       USE_FIXED_PIN_FOR_PAIRING,
                                       123456,
                                       BONDING);
    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "BLE Stack not Initialized\r\n");
    }

    /* Set output power level */
    aci_hal_set_tx_power_level(1, 4);
    // set_connectable = TRUE;
    s4380479_SCAN_user_process();
}

/**
 * @brief  Configure the device as Client and scan for servers.
 *
 * @param  None
 * @retval None
 */
void s4380479_SCAN_user_process(void)
{

    if (set_connectable)
    {
        /* Establish connection with remote device */
        s4380479_execute_scan();
        set_connectable = FALSE;
    }
}

/*
 * BlueNRG-MS background scan task
 */
void s4380479_SCAN_process(void)
{
    // s4380479_SCAN_user_process();
    hci_user_evt_proc();
}

/**
 * @brief  Scan BLE 
 * @param  None 
 * @retval None
 */
void s4380479_execute_scan(void)
{
    tBleStatus ret;
    uint8_t addr[7];

    addr[0] = PUBLIC_ADDR;
    memcpy(&addr[1], CLIENT_DADDR, 6);

    s4380479_os_log_message(LOG_MSG, "Scanning ...\r\n");
    ret = aci_gap_start_observation_procedure(SCAN_P, SCAN_L, ACTIVE_SCAN, PUBLIC_ADDR, 0);

    if (ret != 0)
    {
        s4380479_os_log_message(ERROR_MSG, "Error while starting scanning.\n");
        osDelay(100);
    }
}

int s4380479_check_node(uint8_t *addr)
{
    //returnu if familiar node
    if (memcmp(addr, NODE_1_DADDR, 6) == 0)
    {
        return NODE1;
    }
    else if (memcmp(addr, NODE_2_DADDR, 6) == 0)
    {
        return NODE2;
    }
    else if (memcmp(addr, NODE_3_DADDR, 6) == 0)
    {
        return NODE3;
    }
    else if (memcmp(addr, NODE_4_DADDR, 6) == 0)
    {
        return NODE4;
    } 
    return UNKNOWN_NODE;
}


uint8_t* append_string(void) {

    int len = strlen(TxWifiData), j = 0;
     uint8_t text[] = "\"A\":{\"RSSI\": %d, \"Ultra\": %d}";
    for (int i = len - 1; i < (len + strlen(text)); i++) {
        TxWifiData[i] = text[j++];
    }

}

void s4380479_store_reject_ble_data(uint8_t *data, uint8_t *RSSI, int node)
{
    // char tempData[MSG_BUFFER];

    if ((node == NODE1) && !node1_data)
    {
        //store node1 data
        node1_rssi = *RSSI - RSSI_256;
        node1_ultra = (data[2] << 24) | (data[3] << 16) | (data[4] << 8) | (data[5]);// v4[0] | (v4[1] << 8) | (v4[2] << 16) | (v4[3] << 24)
        // sprintf(node1_ultra, "%x%x%x%x", )
        // sprintf(TxWifiData, "\"A\":{\"RSSI\": %d, \"Ultra\": %d}", *RSSI - RSSI_256, data[3]);
        // strcat((char*)TxWifiData, tempData);
        // s4380479_os_log_message(LOG_MSG, "%02X %02X %02X %02X",data[3],data[4],data[5],data[6]);
        // s4380479_os_log_message(LOG_MSG, "%d",node1_ultra);

        node1_data = 1;
    }
    else if ((node == NODE2) && !node2_data)
    {
        //store node1 data
        node2_rssi = *RSSI - RSSI_256;
        node2_ultra = (data[2] << 24) | (data[3] << 16) | (data[4] << 8) | (data[5]);
        node2_data = 1;
    } else if ((node == NODE3) && !node3_data)
    {
        //store node1 data
        node3_rssi = *RSSI - RSSI_256;
        node3_ultra = (data[2] << 24) | (data[3] << 16) | (data[4] << 8) | (data[5]);
        node3_data = 1;
    } else if ((node == NODE4) && !node4_data)
    {
        //store node1 data
        node4_rssi = *RSSI - RSSI_256;
        node4_ultra = (data[2] << 24) | (data[3] << 16) | (data[4] << 8) | (data[5]);
        node4_data = 1;
    }
    if (node1_data && node2_data && node3_data && node4_data)
    {
        read_acc_mag(1);
        read_acc_mag(0);
        int16_t *pAcc = get_acc_reading();
        int16_t *pMag = get_mag_reading();
        //send wifi message
        sprintf(TxWifiData, "{\"data\":{\"A\":{\"RSSI\": %d, \"Ultra\": %d},\"B\":{\"RSSI\": %d, \"Ultra\": %d},\"C\":{\"RSSI\": %d, \"Ultra\": %d},\"D\":{\"RSSI\": %d, \"Ultra\": %d}, \"Acc\":{\"x\": %d, \"y\": %d, \"z\":%d}, \"Gyro\":{\"x\": %d, \"y\": %d, \"z\":%d}}}",
                node1_rssi, node1_ultra, node2_rssi, node2_ultra,
                node3_rssi, node3_ultra, node4_rssi, node4_ultra,
                pAcc[0], pAcc[1], pAcc[2],
                pMag[0], pMag[1], pMag[2]);
                // s4380479_os_log_message(LOG_MSG, "%s", (char*)TxWifiData);
                    s4380479_send_wifi_message();
                    node1_data = 0, node2_data = 0, node3_data = 0, node4_data = 0;

    } 

}

void s4380479_send_wifi_message(void) {
        
    vTaskSuspend(get_conn_handle());
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    // __disable_irq();

    //set_ble_flag(0);

    //process data
    xSemaphoreGive(s4380479_SemaphoreSpi);

    s4380479_os_log_message(LOG_MSG, "finished sending taken\r\n");
    osDelay(100);

    reconfig_ble_SPI();
    // __enable_irq();
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

    //set_ble_flag(1);
    vTaskResume(get_conn_handle());

}

int test = 1;
void s4380479_printf_devices(uint8_t *addr, uint8_t *data_length,
                             uint8_t *data, uint8_t *RSSI)
{
    s4380479_store_reject_ble_data(data, RSSI, s4380479_check_node(addr));
    osDelay(100);
    
//     int node = s4380479_check_node(addr);
//       append_string();
//       if (test) {

// s4380479_os_log_message(LOG_MSG, "test");

//       s4380479_store_reject_ble_data(data, RSSI, NODE1);
//       osDelay(100);

//       s4380479_store_reject_ble_data(data, RSSI, NODE2);
//             osDelay(100);

//       s4380479_store_reject_ble_data(data, RSSI, NODE3);
//             osDelay(100);

//       s4380479_store_reject_ble_data(data, RSSI, NODE4);
//             osDelay(100);

//     // s4380479_send_wifi_message();
//       }

//     if (!UNKNOWN_NODE)
//     {
//         //familar device found
//         s4380479_os_log_message(LOG_MSG, "Addr = 0x%02x:%02x:%02x:%02x:%02x:%02x",
//                                 addr[5], addr[4], addr[3], addr[2], addr[1], addr[0]);
//         s4380479_os_log_message(LOG_MSG, "RSSI = 0x%02x\r\n", *RSSI);

//         vTaskSuspend(get_conn_handle());
//         HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
//         // __disable_irq();

//         //set_ble_flag(0);

//         //process data
//         xSemaphoreGive(s4380479_SemaphoreSpi);

//         s4380479_os_log_message(LOG_MSG, "finished sending taken\r\n");
//         osDelay(100);

//         reconfig_ble_SPI();
//         // __enable_irq();
//         HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

//         //set_ble_flag(1);
//         vTaskResume(get_conn_handle());
//     }
}

// /**
//  * Initialises the pins used for the system monitor
//  */
void s4380479_TaskScanQueue(void)
{
    struct ScanMessage scanMsg;

    while (1)
    {
        if (s4380479_QueueScanEvent != NULL)
        {

            if (xQueueReceive(s4380479_QueueScanEvent, &scanMsg, __UINT32_MAX__))
            {
                s4380479_printf_devices(scanMsg.addr, scanMsg.dataLength,
                                        scanMsg.data, scanMsg.RSSI);
            }
        }
    }
}

/** task safe printf function */
void s4380479_os_scan_discovery(uint8_t *addr, uint8_t *data_length,
                                uint8_t *data, uint8_t *RSSI)
{
    struct ScanMessage scanMsg;

    scanMsg.addr = addr;
    scanMsg.dataLength = data_length;
    scanMsg.data = data;
    scanMsg.RSSI = RSSI;

    if (xQueueSend(s4380479_QueueScanEvent, &scanMsg,
                   (portTickType)100) != pdPASS)
    {
        //failed
        printf("%s\r\n", "failied to send scan queue");
    }
}

/**
 * Initialises the task for printing messages to stdout
 */
void s4380479_os_scan_event_init(void)
{
    //init Queue
    s4380479_QueueScanEvent = xQueueCreate(100, sizeof(struct ScanMessage));

    if (xTaskCreate((TaskFunction_t)&s4380479_TaskScanQueue, "SCANEVENT", SCAN_EVENT_TASK_STACK_SIZE, NULL,
                    SCAN_EVENT_PRIORITY, &scanningTaskHandle) != pdPASS)
    {
        printf("%s\r\n", "failed to create debug printf task");
    }
}

/////////////////////////////////////////////

void s4380479_TaskWifi(void)
{
    // osThreadTerminate(get_conn_handle());
    // char dataToRecv[30];
    // for (;;) {
    //     if (s4380479_QueueWifiEvent != NULL)
    //     {
    //         if (xQueueReceive(s4380479_QueueWifiEvent, &dataToRecv, __UINT32_MAX__))
    //         {
    //             s4380479_os_log_message(LOG_MSG, "WIFI Semaphore taken %s", dataToRecv);
    //             //set_ble_flag(0);

    //         }
    //     }
    // }

    for (;;)
    {

        if (s4380479_SemaphoreSpi != NULL &&
            xSemaphoreTake(s4380479_SemaphoreSpi, 1) == pdTRUE)
        {

            // s4380479_os_log_message(LOG_MSG, "WIFI Semaphore taken\r\n");
            // osDelay(100);

            // //set_ble_flag(0);

            // HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
            // __disable_irq();
            s4308479_init_wifi();
            s4380479_wifi_connect();
            // s4380479_WebServer_Process();
            s4380479_Wifi_Process(&TxWifiData);

            // // __enable_irq();
            // //   HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
            // s4380479_os_log_message(LOG_MSG, "WIFI Semaphore taken\r\n");
            // osDelay(1000);
            // // xSemaphoreGive(s4380479_SemaphoreWifi);
        }
    }
}

void s4380479_os_wifi(void)
{
    char msg[10] = "test";

    //set_ble_flag(0);
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    // __disable_irq();
    s4308479_init_wifi();
    s4380479_wifi_connect();
    s4380479_WebServer_Process();

    // __enable_irq();
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    if (xQueueSend(s4380479_QueueWifiEvent, msg,
                   (portTickType)100) != pdPASS)
    {
        //failed
        printf("%s\r\n", "failied to send queue");
    }
}

/**
 * Initialises the task for printing messages to stdout
 */
void s4380479_os_wifi_init(void)
{
    //init Queue
    // s4380479_QueueWifiEvent = xQueueCreate(5, sizeof(char) * 50);

    if (xTaskCreate((TaskFunction_t)&s4380479_TaskWifi, "wifiinnit", SCAN_EVENT_TASK_STACK_SIZE, NULL,
                    tskIDLE_PRIORITY + 6, &wifiTaskHandle) != pdPASS)
    {
        printf("%s\r\n", "failed to create debug printf task");
    }
}
