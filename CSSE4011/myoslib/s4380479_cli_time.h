/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_led.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   LED CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_CLI_TIME_H
#define S4380479_CLI_TIME_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "argtable2.h"
#include <stdlib.h>
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define sformTime 0
#define helpTime 1
#define endTime 2
#define progTime    "time"
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_time_cmd();
void **s4380479_get_time_cmd();
void get_sys_time(int format);

#endif
