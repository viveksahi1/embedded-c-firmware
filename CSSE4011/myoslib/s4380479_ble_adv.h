/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_ble.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   ble CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_BLE_ADV_H
#define S4380479_BLE_ADV_H

#define DATA_LENGTH 6

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "s4380479_os_log_message.h"
#include "bluenrg_hal_aci.h"
#include "sample_service.h"
#include "sensor_service.h"

// #include "stm32l4_b_l475e_iot01a.h"
// #include "hci_tl.h"
// #include "sample_service.h"
// #include "role_type.h"
// #include "bluenrg_utils.h"
// #include "bluenrg_gatt_server.h"
// #include "bluenrg_gap_aci.h"
// #include "bluenrg_gatt_aci.h"

/* Exported Functions --------------------------------------------------------*/
void s4380479_ADV_Init(void);
void s4380479_toggle_advertising(int status);
int s4380479_get_adv_status(void);
// void s4380479_ADV_user_process(void);
// void s4380479_ADV_process(void);
void s4380479_update_advertising(void);
void s4380479_start_advertising(void);
void s4380479_setConnectable(void);


#endif
