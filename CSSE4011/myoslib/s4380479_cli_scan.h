/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_led.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   LED CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_CLI_SCAN_H
#define S4380479_CLI_SCAN_H

/* Includes ------------------------------------------------------------------*/
//#include "board.h"#include "argtable2.h"
#include <stdlib.h>
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "board.h"
#include "s4380479_debug_print.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define helpScan 0
#define endScan 1
#define progScan    "scan"
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_scan_cmd();
void **s4380479_get_scan_cmd();
void get_sys_scan(void);

#endif
