/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_ble.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   ble CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_BLE_SCAN_H
#define S4380479_BLE_SCAN_H

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "s4380479_os_log_message.h"
#include "bluenrg_hal_aci.h"
#include "s4380479_hal_wifi.h"
#include "task.h"

// #include "stm32l4_b_l475e_iot01a.h"
// #include "hci_tl.h"
// #include "sample_service.h"
// #include "role_type.h"
// #include "bluenrg_utils.h"
// #include "bluenrg_gatt_server.h"
// #include "bluenrg_gap_aci.h"
// #include "bluenrg_gatt_aci.h"

#define BDADDR_SIZE 6

#define NODE1   0
#define NODE2   1
#define NODE3   2
#define NODE4   3
#define UNKNOWN_NODE    4

#define MSG_BUFFER  250

QueueHandle_t s4380479_QueueScanEvent;
QueueHandle_t s4380479_QueueWifiEvent;

struct ScanMessage {
    uint8_t *addr;
    uint8_t *dataLength; 
    uint8_t *data;
    uint8_t *RSSI;
};
SemaphoreHandle_t s4380479_SemaphoreSpi;
SemaphoreHandle_t s4380479_SemaphoreWifi;

/* Exported Functions --------------------------------------------------------*/
void s4380479_BLE_SCAN_INIT(void);
void s4380479_SCAN_user_process(void);
void s4380479_SCAN_process(void);
void s4380479_execute_scan(void);
void s4380479_printf_devices_found(uint8_t status, uint8_t *addr_type, uint8_t *addr,
                                   uint8_t *data_length, uint8_t *data, uint8_t *RSSI);


void s4380479_os_scan_event_init(void);
void s4380479_os_scan_discovery(uint8_t *addr, uint8_t *data_length, 
                                uint8_t *data, uint8_t *RSSI);
void s4380479_TaskScanQueue(void);
void s4380479_printf_devices(uint8_t *addr, uint8_t *data_length, 
                            uint8_t *data, uint8_t *RSSI);

void s4380479_os_wifi_init(void);
int s4380479_check_node(uint8_t *addr);
                            
#endif
