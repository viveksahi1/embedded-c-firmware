/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_led.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   ADV CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_CLI_ADV_H
#define S4380479_CLI_ADV_H

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"
#include "board.h"

#include "argtable2.h"
#include <stdlib.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
//#include "board.h"#include "s4380479_debug_print.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
#define advOnCmd "on"
#define advOffCmd "off"
#define advInitCmd "init"
#define progAdv "recv"

#define cAdvLog 0
#define dHelpAdv 1
#define dEndAdv 2

#define ADVR_ON 1
#define ADVR_OFF 0

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_adv_cmd();
void **s4380479_get_adv_cmd();
void handle_adv(int Adv, const char *mode);

#endif
