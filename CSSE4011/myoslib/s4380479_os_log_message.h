/**
 ******************************************************************************
6* @file    myoslib/s4380479_os_log_messages.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   OS LOG driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_os_log_message_init(void) - task to receive log/error msgs
 ******************************************************************************
 */

#ifndef S4380479_OS_LOG_MESSAGE_H
#define S4380479_OS_LOG_MESSAGE_H

#define MESSAGE_BUFFER 500
#define QUEUE_SIZE 100
#define COLOR_CODE_SIZE 25

/* Includes ------------------------------------------------------------------*/
// #include "stm32l4xx_hal_conf.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "board.h"
#include "s4380479_os_printf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
QueueHandle_t s4380479_QueueLogMessage;

struct LogMessage {

	int type;
	char messageRec[MESSAGE_BUFFER];
};


/* External function prototypes -----------------------------------------------*/
void s4380479_os_log_message_init(void);
void s4380479_os_log_message(int messageType, char* message, ...);
void update_log_type(int mode);

#endif

