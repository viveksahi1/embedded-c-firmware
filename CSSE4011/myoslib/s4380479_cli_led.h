/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_led.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   LED CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_CLI_LED_H
#define S4380479_CLI_LED_H

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_led.h"
#include "s4380479_cli_argtable.h"
#include "board.h"
#include "argtable2.h"
#include <stdlib.h>

/* Private typedef -----------------------------------------------------------*/
#define cOffLed 0
#define cOnLed 1
#define cTogLed 2
#define cHelpLed 3
#define cEndLed 4
#define progLed "led"
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_led_cmd();
void** s4380479_get_led_cmd();
void handle_led(int off, int offLed, int on,
                int onLed, int tog, int togLed);

#endif
