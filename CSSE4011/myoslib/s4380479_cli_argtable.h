/**
 ******************************************************************************
6 * @@file  mylib/s4380479_cli_led.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   argtable CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_CLI_ARGTABLE_H
#define S4380479_CLI_ARGTABLE_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include <stdlib.h>
#include <regex.h>      /* REG_ICASE */
#include "s4380479_os_log_message.h"
#include "s4380479_debug_print.h"
#include "s4380479_os_printf.h"
#include "argtable2.h"
#include "s4380479_cli_file.h"
#include "s4380479_cli_dir.h"
#include "s4380479_cli_cd.h"
#include "s4380479_cli_rec.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UNK_CMD 0
#define TIME_CMD 1
#define LED_CMD 2
#define LOG_CMD 3
#define ADV_CMD 4
#define SCAN_CMD 5
#define FILE_CMD 6
#define DIR_CMD 7
#define CD_CMD 8
#define REC_CMD 9

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
int allocation_error(const char *prog);
int print_help(const char *prog, void **argtable);
int print_errors(const char *prog, struct arg_end *end);
int identify_cmd_entered(char *cmd);
int process_cli_cmds(int argc, char **argv);
void cliErrorFunc(void *parent, FILE *fp, int error, const char *argval, const char *progname);

#endif
