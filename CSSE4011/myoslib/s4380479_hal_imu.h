/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_imu.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal imu
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_HAL_IMU_H
#define S4380479_HAL_IMU_H

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "s4380479_os_log_message.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define ACC_SENSOR  1
#define MAG_SENSOR  0
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
uint16_t *s4380479_get_gyr_reading(void);
uint16_t *s4380479_get_acc_reading(void);
uint16_t *s4380479_get_mag_reading(void);
void s4380479_read_acc_mag(int device);
void s4380479_init_gyr_acc(void);



#endif

