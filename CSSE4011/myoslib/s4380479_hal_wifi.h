/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_wifi.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal wifi
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_HAL_WIFI_H
#define S4380479_HAL_WIFI_H

/* Includes ------------------------------------------------------------------*/
#include "wifi.h"
// #include "stm32l4_b_l475e_iot01a.h"
// #include "s4380479_os_log_message.h"

/* Private typedef -----------------------------------------------------------*/
typedef enum
{
  WS_IDLE = 0,
  WS_CONNECTED,
  WS_DISCONNECTED,
  WS_ERROR,
} WebServerState_t;
/* Private define ------------------------------------------------------------*/
// #define SSID "Dominic's iPhone"
// #define SSID "Dodo"
// #define PASSWORD "1notebook"

// #define SSID "deepanshu"
// #define PASSWORD "deepanshu"

// #define PORT 10002
#define WIFI_WRITE_TIMEOUT 100
#define WIFI_READ_TIMEOUT 5000

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void s4308479_init_wifi(void);
void s4380479_WebServer_Process(void);
WIFI_Status_t SendWebPage(uint8_t ledIsOn, uint8_t temperature);
void s4380479_wifi_connect(void);
void s4380479_Wifi_Process(uint8_t *data);

#endif

