/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_cd.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli cd
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_cd.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableCd; //variable to store cd cli cmds

/**
 * This method allcoates the memory for the argtable to store cd cmds.
 */
void allocate_cd_argtable()
{
    argtableCd = malloc(sizeof(struct arg_str *) * 3 + sizeof(struct arg_lit *) + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * 10);
}

/**
 * Initialise the cd cmds
 */
void s4380479_init_cd_cmd()
{
    /** cd CLI CMD */
    // struct arg_rex *logCmd = arg_rex1(NULL, NULL, "cd", NULL, REG_ICASE, NULL);
    struct arg_str *Cd = arg_str1(NULL, NULL, "directory name", "change directory with name specified");
    struct arg_lit *helpCd = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *endCd = arg_end(20);

    //recdect error func
    Cd->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    helpCd->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    endCd->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_cd_argtable();
    argtableCd[cCd] = Cd;
    argtableCd[cHelpCd] = helpCd;
    argtableCd[cEndCd] = endCd;
}

/**
 * This method return the artable for the cd cmd
 */
void **s4380479_get_cd_cmd()
{
    return argtableCd;
}

/**
 * This is the callback function to hadndle cd cmds
 */
void handle_cd(int cdCmd, const char *dir)
{
    if (cdCmd) {
        s4380479_cd_dir(dir);
    }
}
