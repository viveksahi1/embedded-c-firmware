/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEBUG_PRINT_H
#define __DEBUG_PRINT_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
// #include <syscalls.c>
#include "board.h"
#ifdef BLE_PROJECT
    #include "stm32l4_b_l475e_iot01a.h"
#endif

extern void debug_putc(char c);
extern void debug_flush();
extern unsigned char debug_getc();
extern void debug_rxflush();
void debug_printf(const char *fmt, ...);

#endif