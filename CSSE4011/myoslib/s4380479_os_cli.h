/**
 ******************************************************************************
6 * @@file    mylib/s4380479_os_cli.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_os_cli_init() - init cli interface
 ******************************************************************************
 */

#ifndef S4380479_OS_CLI_H
#define S4380479_OS_CLI_H

/* Includes ------------------------------------------------------------------*/
// #include "stm32f4xx_hal_conf.h"
#include <stdlib.h>
#include "s4380479_os_log_message.h"
#include "s4380479_debug_print.h"
#include "s4380479_cli_argtable.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "FreeRTOS_CLI.h"
#include "board.h"
#define INDEX_SIZE 50
#define MAX_CMD_ARG 20

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
extern void s4380479_os_cli_init();

#endif

