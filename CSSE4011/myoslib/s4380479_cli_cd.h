/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_cd.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   DIR CLI drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_CLI_CD_H
#define S4380479_CLI_CD_H

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"
#include "board.h"

#include "argtable2.h"
#include <stdlib.h>

/* Private typedef -----------------------------------------------------------*/
#define cCd     0
#define cHelpCd 1
#define cEndCd  2 

#define progCd "cd"

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_cd_cmd();
void** s4380479_get_cd_cmd();
void handle_cd(int cdCmd, const char *dir);

#endif
