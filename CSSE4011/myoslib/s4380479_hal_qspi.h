/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_qspi.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal qspi
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

#ifndef S4380479_HAL_QSPI_H
#define S4380479_HAL_QSPI_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "s4380479_os_log_message.h"

#ifdef BLE_PROJECT
    #include "stm32l475e_iot01_qspi.h"
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
  

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void s4380479_init_fileSystem(void);
void qspi_test(void);

void s4380479_remove_file(char* fileName);
void s4380479_create_file(char* filename);
void s4380479_read_file(char *fileName);
void s4380479_list_file(void);

char* s4380479_check_file_in_dir(char *dir);
int s4380479_next_dir(char* checkNext);
int s4380479_file_exists(char* fileName);
void s4380479_tree(void);
void s4380479_create_dir(char* dirName);
void s4380479_remove_dir(char* dirName);

signed short s4380479_open_log_file(char *filename);
void s4380479_close_log_file(signed short fd);
void s4380479_write_log_file(signed short fd, char* data);
void s4380479_clean_file(signed short fd);

#endif

