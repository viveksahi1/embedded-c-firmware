/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_log.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli log
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_log.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableLog; //variable to store log cli cmds

/**
 * This method allcoates the memory for the argtable to store log cmds.
 */
void allocate_log_argtable()
{
    argtableLog = malloc(sizeof(struct arg_str *) * 2 + sizeof(struct arg_lit *) + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * 10);
}

/**
 * Initialise the time CLI cmds
 */
void s4380479_init_log_cmd()
{
    /** LOG CLI CMD */
    // struct arg_rex *logCmd = arg_rex1(NULL, NULL, "log", NULL, REG_ICASE, NULL);
    struct arg_str *filterLog = arg_str1("f", "filter", "{error, log, debug, all}", "Filter the log message output");
    struct arg_lit *helpLog = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_str *onLog = arg_str1(NULL, NULL, "on", "Enable the selected message type");
    struct arg_end *endLog = arg_end(20);

    //redirect error func
    filterLog->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    helpLog->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    onLog->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    endLog->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_log_argtable();
    argtableLog[cFilterLog] = filterLog;
    argtableLog[cHelpLog] = helpLog;
    argtableLog[cOnLog] = onLog;
    argtableLog[cEndLog] = endLog;
}

/**
 * This method return the artable for the log cmd
 */
void **s4380479_get_log_cmd()
{
    return argtableLog;
}

/**
 * This is the callback function to hadndle log cmds
 */
void handle_log(int filterLog, const char *mode, const char *onLog)
{
    s4380479_os_log_message(DEBUG_MSG, "Filtered Output: %d, Mode: %s, Status:%S", 
                        filterLog, mode, onLog);
    //check if 'filter' argument is requested with 'on' argument
    if (filterLog && !strncmp(onLog, "on", sizeof("on")))
    {
        // compare all the inputs with availabe log cmds
        if (!strncmp(mode, fErrCmd, sizeof(fErrCmd)))
        {
            s4380479_os_log_message(CLI_ERROR, "");
        }
        else if (!strncmp(mode, fLogCmd, sizeof(fLogCmd)))
        {
            s4380479_os_log_message(CLI_LOG, "");
        }
        else if (!strncmp(mode, fDebugCmd, sizeof(fDebugCmd)))
        {
            s4380479_os_log_message(CLI_DEBUG, "");
        }
        else if (!strncmp(mode, fAllCmd, sizeof(fAllCmd)))
        {
            s4380479_os_log_message(CLI_ALL, "");
        }
        else
        {
            s4380479_os_log_message(ERROR_MSG, "Invalid Mode. Type --help for more.");
        }
    }
    else
    {
        s4380479_os_log_message(ERROR_MSG, "Invalid Mode. Type --help for more.");
    }
}
