/**
 ******************************************************************************
6 * @@file    mylib/s4380479_cli_rec.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   FILE rec drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
* ******************************************************************************
 */

#ifndef S4380479_CLI_REC_H
#define S4380479_CLI_REC_H

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_log_message.h"
#include "s4380479_cli_argtable.h"
#include "board.h"
#include "argtable2.h"
#include <stdlib.h>
#include "s4380479_hal_qspi.h"
#include "s4380479_os_imu_log.h"
#include "s4380479_hal_imu.h"

SemaphoreHandle_t s4380479_SemaphoreSensor;

/* Private typedef -----------------------------------------------------------*/
#define recEnable 0
#define recDisable 1
#define recHelp 2
#define recEnd 3

#define progRec "rec"

#define SENSOR_ENABLE   1
#define SENSOR_DISABLE  0
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* External function prototypes -----------------------------------------------*/
void s4380479_init_rec_cmd();
void** s4380479_get_rec_cmd();
void handle_rec(int recEnb, char *sensor, char *file, char* rate,
                int recDsb, char *sensorName, char *fileName);

#endif
