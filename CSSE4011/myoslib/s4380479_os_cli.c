/**
 ******************************************************************************
 * @file    mylib/s4380479_os_josystick.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   Os joystick drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_os_joystick_init(void) - init joystick
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
// #include "debug_printf.h"
#include "s4380479_os_cli.h"
#include "string.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define CLI_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 3)
#define CLI_PRIORITY (tskIDLE_PRIORITY + 6)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
void s4380479_CLI_Task(void *param);

/* Init the cli tasks*/
extern void s4380479_os_cli_init()
{
    xTaskCreate(s4380479_CLI_Task, "CLI TASK", CLI_TASK_STACK_SIZE,
                NULL, CLI_PRIORITY, NULL);
}

/**
 * THis method clear the input buffer
 */
void clear_input_buffer(char **buffer)
{
    //clear array using memset
    for (int i = 0; i < MAX_CMD_ARG; ++i)
    {
        memset(buffer[i], 0, INDEX_SIZE);
    }
}

/**
 *  the CLI task
 */
void s4380479_CLI_Task(void *param)
{

    int numCmdArg = 0, InputIndex = 0;
    char cRxedChar;

    char **cmdInputBuffer = pvPortMalloc(sizeof(char *) * MAX_CMD_ARG);
    //allocate nmemory for the input buffer
    for (int i = 0; i < MAX_CMD_ARG; ++i)
    {
        cmdInputBuffer[i] = pvPortMalloc(sizeof(char) * INDEX_SIZE);
    }
    clear_input_buffer(cmdInputBuffer);
    for (;;)
    {
        /* Receive character from terminal */
        cRxedChar = debug_getc();

        /* Process if character if not Null */
        if (cRxedChar != '\0')
        {

            /* Echo character */
            debug_putc(cRxedChar);

            /* Process only if return is received. */
            if (cRxedChar == '\r')
            {

                //Put new line and transmit buffer
                debug_putc('\n');
                debug_flush();

                /* Put null character in command input string. */
                cmdInputBuffer[numCmdArg++][InputIndex] = '\0';

                //Handle no argument case
                if (!strlen(*cmdInputBuffer))
                {
                    numCmdArg--;
                }

                //process CLI
                process_cli_cmds(numCmdArg, cmdInputBuffer);

                //clear input buffer
                clear_input_buffer(cmdInputBuffer);
                InputIndex = 0, numCmdArg = 0;
            }
            else
            {

                debug_flush(); //Transmit USB buffer

                if (cRxedChar == '\r')
                {
                    /* Ignore the character. */
                }
                else if (cRxedChar == '\b')
                {

                    /* Backspace was pressed.  Erase the last character in the
					 string - if any.*/
                    if (InputIndex > 0)
                    {
                        cmdInputBuffer[numCmdArg][--InputIndex] = '\0';
                    }
                }
                else
                {
                    /* A character was entered.  Add it to the string
					 entered so far.  When a \n is entered the complete
					 string will be passed to the command interpreter. */
                    if (InputIndex < INDEX_SIZE)
                    {
                        if (cRxedChar != ' ')
                        {
                            cmdInputBuffer[numCmdArg][InputIndex++] = cRxedChar;
                        }
                        else
                        {
                            cmdInputBuffer[numCmdArg++][InputIndex] = '\0';
                            InputIndex = 0;
                        }
                    }
                }
            }
        }

        vTaskDelay(1);
    }
}
