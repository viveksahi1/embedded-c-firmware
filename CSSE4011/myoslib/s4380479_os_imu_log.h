/**
 ******************************************************************************
6* @file    myoslib/s4380479_os_imu_log.h
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   OS imu log driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 ******************************************************************************
 */

#ifndef S4380479_OS_IMU_LOG_H
#define S4380479_OS_IMU_LOG_H

/* Includes ------------------------------------------------------------------*/
// #include "stm32l4xx_hal_conf.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "board.h"
#include "s4380479_hal_qspi.h"


#define IMU_DATA_BUFFER 200
// Sensors
#define OPEN_ACC_FILE   0 
#define ACC_DATA    1
#define CLOSE_ACC_FILE  2
#define OPEN_MAG_FILE   3 
#define MAG_DATA    4
#define CLOSE_MAG_FILE  5

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
QueueHandle_t s4380479_QueueImuLog;

struct ImuMessage {

	int type; // 0-> open file (name in data), 1-> imu data, 2-> close file
	char data[IMU_DATA_BUFFER];
};

/* External function prototypes -----------------------------------------------*/
void s4380479_os_imu_log_init(void);
void s4380479_os_imu_log(int messageType, char *message, ...);
#endif

