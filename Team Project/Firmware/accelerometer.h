#ifndef _ACCELEROMETER_SENSOR_H_
#define _ACCELEROMETER_SENSOR_H_

#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "uv_sensor.h"

// CTRL registers
#define CTRL_REG1 0x2A
#define CTRL_REG2 0x2B
#define CTRL_REG3 0x2C
#define CTRL_REG4 0x2D
#define CTRL_REG5 0x2E

// CTRL registers values
#define CLR_SENSOR 0x40
#define ACTIVE_STATE 0x19
#define STANDBY_MODE 0x18
#define REG2_VALUE 0x1D
#define REG3_VALUE 0x38
#define REG4_VALUE 0x20
#define REG5_VALUE 0x20
#define LOW_POWER_REG_1 0x29

#define MMA845_SENSOR_ADDR 0x1C
// data registers
#define X_MSB 0x01
#define X_LSB 0x02
#define Y_MSB 0x03
#define Y_LSB 0x04
#define Z_MSB 0x05
#define Z_LSB 0x06

// int satus registers
#define INT_SRC 0x0C
#define TRANSIENT_CFG 0x1D
#define TRANSIENT_THR 0x1F
#define TRANSIENT_COUNT 0x20
#define TRANSIENT_SCR 0x1E

//INTERRUPT SETUP
#define TRANSIENT_CFG_VALUE 0x16
#define TRANSIENT_THR_VALUE 0x03
#define TRANSIENT_COUNT_VALUE 0x01

esp_err_t acc_write_reg(i2c_port_t i2c_num, uint8_t reg, uint8_t data);
esp_err_t acc_read_reg(i2c_port_t i2c_num, uint8_t *data_l, uint8_t reg);
int accelerometer_read(void);
void init_acclerometer(void);
void acc_low_power(void);

#endif