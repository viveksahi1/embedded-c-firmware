#ifndef _SLEEP_LED_H_
#define _SLEEP_LED_H_

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "driver/gpio.h"
#include "freertos/queue.h"
#include <string.h>
#include <stdlib.h>
#include "driver/rtc_io.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/rtc.h"
#include <time.h>
#include <sys/time.h>
#include "freertos/semphr.h"

#define LED_GREEN_GPIO 12 //GREEN led
#define LED_RED_GPIO 13   //RED led

void green_led_task(void *param);
void red_led_task(void *param);

#endif