#ifndef _WIFI_MQTT_H_
#define _WIFI_MQTT_H_

#include <stdio.h>
#include <stdlib.h>
#include "driver/gpio.h"
#include "uart_gps.h"

/*sockets*/
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "mqtt_client.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"
#include "soc/rtc.h"
#include "uart_gps.h"
#include "voltage_measurement.h"

/*esp includes */
#include "esp_sleep.h"
#include "esp_log.h"
#include "esp32/ulp.h"
#include "esp_adc_cal.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"

/*freertos includes */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "temp_sensor.h"
#include "tone_detection.h"

/* MQTT defines */
#define CONFIG_WIFI_PASSWORD "Vivid5Propel4cover"
#define CONFIG_WIFI_SSID "ax209"
#define MAX_MSG_SIZE 10000
#define MAX_SCAN_RESULT 10
#define BUFFER_MESSAGE 200
#define WAIT_WIFI 1000

void wifi_setup(void);
void wifi_scan(void);
void mqtt_app_start(void);
char *generate_packet(int uvInd, float tempValue, float humValue,
					  int voltValue, int gpsEnable);
void no_movement_package(void);
int publish_mqtt_msg(char *packet);
int get_wifi_status(void);

#endif