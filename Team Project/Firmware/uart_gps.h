#ifndef _UART_GPS_H_
#define _UART_GPS_H_

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_sleep.h"
#include <sys/types.h>
#include <sys/reent.h>
#include <sys/lock.h>
#include <rom/rtc.h>
#include "esp_intr_alloc.h"
#include "esp_clk.h"
#include "esp_timer.h"
#include "soc/soc.h"
#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/frc_timer_reg.h"
#include "rom/ets_sys.h"
#include "sdkconfig.h"
#include "driver/uart.h"

#define ECHO_TEST_TXD (GPIO_NUM_17)
#define ECHO_TEST_RXD (GPIO_NUM_16)
#define ECHO_TEST_RTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS (UART_PIN_NO_CHANGE)
#define BUF_SIZE (1024)
#define GPS_INFO_BUFFER 20

#define EX_UART_NUM UART_NUM_2
#define PATTERN_CHR_NUM (3)
#define RD_BUF_SIZE (BUF_SIZE)

/* defines for gps string position */
#define UTC 1
#define STATUS 2
#define LATITUDE 3
#define NORTH_SOUTH 4
#define LONGITUDE 5
#define EAST_WEST 6
#define DATE_GPS 9

char *get_utc(void);
int get_hours(void);
int get_min(void);
int get_sec(void);
int get_status(void);
char *get_lat(void);
char *get_north_south(void);
char *get_long(void);
char *get_east_west(void);
char *get_date(void);
char *get_time(void);

int parse_nmea(void);
int get_gps_data_len(void);
void gps_init(void);
void rtc_gps(int h, int m, int s);

#endif