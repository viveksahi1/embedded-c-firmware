#ifndef _TONE_DETECTION_H_
#define _TONE_DETECTION_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"

//define for 1k samples
#define SAMPLES_PER_CALC 640
#define DEFAULT_VREF 1100
#define NO_OF_TONE_SAMPLES 640 //Multisampling   3318499
#define TARGET_FREQUENCY_1k 1050
#define SAMPLING_RATE_1k 32500

//defines for 4k samples
#define SAMPLES_PER_CALC_4k 640
#define NO_OF_SAMPLES_4k 640
#define TARGET_FREQUENCY_4k 4250
#define SAMPLING_RATE_4k 32500
#define THRESHOLD_1K 50000 //0
#define THRESHOLD_4K 40000

//tone status defines
#define NO_TONE_DETECTED 0
#define ONE_TONE_DETECTED 1
#define FOUR_TONE_DETECTED 2
#define BOTH_TONE_DETECTED 3
#define NO_TONE_THR 200
#define DETECTION_OFF 4

float goertzel_mag_1k(int numSamples, int TARGET_FREQUENCY, int SAMPLING_RATE, float *data);
float goertzel_mag_4k(int numSamples, int TARGET_FREQUENCY, int SAMPLING_RATE, float *data);
void tone_detection(void);
void init_tone_detection(void);
void sample_tone();
void task_sample_tone(void *param);
int get_result(void);
void reset_result(void);

#endif