#ifndef _VOLTAGE_MEASUREMENT_H_
#define _VOLTAGE_MEASUREMENT_H_

#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "tone_detection.h"

#define DEFAULT_VREF 1100 //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES 64  //
#define OFFSET 200        //VOLTAGE OFFSET
#define RT_VOLTAGE 136.3
#define R2_VOLTAGE 100

void voltage_measurement(void);
void init_voltage_meassurement(void);
int get_voltage(void);

#endif