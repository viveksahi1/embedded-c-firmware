/* ADC1 Example
   This example code is in the Public Domain (or CC0 licensed, at your option.)
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
   */
#include <stdio.h>
#include <stdlib.h>
#include "voltage_measurement.h"

static esp_adc_cal_characteristics_t *adc_chars;
static const adc_channel_t channel = ADC_CHANNEL_2; //GPIO38 if ADC1
static const adc_atten_t atten = ADC_ATTEN_DB_11;
static const adc_unit_t unit = ADC_UNIT_1;
int voltageMeasure = 0;

/**
 * Init the adc for voltage measurement
 */
void init_voltage_meassurement(void)
{
	//Configure ADC
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(channel, atten);

	//Characterize ADC
	adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);
}

/**
 * Returns the voltage measurement from the adc pin
 */
int get_voltage(void)
{
	return voltageMeasure;
}

/**
 * Perform a voltage measurement from adc
 */
void voltage_measurement(void)
{
	uint32_t voltage, adcReading = 0;
	int raw, batteryVoltage;

	init_voltage_meassurement();
	//Multisampling
	for (int i = 0; i < NO_OF_SAMPLES; i++)
	{
		if (unit == ADC_UNIT_1)
		{
			adcReading += adc1_get_raw((adc1_channel_t)channel);
		}
	}
	adcReading /= NO_OF_SAMPLES;
	//Convert adcReading to voltage in mV,
	voltage = esp_adc_cal_raw_to_voltage(adcReading, adc_chars);
	if (adcReading == 0)
	{
		voltageMeasure = 0;
	}
	else
	{
		voltageMeasure = (RT_VOLTAGE / R2_VOLTAGE) * (voltage) + OFFSET;
	}
	init_tone_detection();
}
