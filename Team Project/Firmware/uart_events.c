#include <stdio.h>
#include "uart_gps.h"

/**
 * This example shows how to use the UART driver to handle special UART events.
 *
 * It also reads data from UART0 directly, and echoes it to console.
 *
 * - Port: UART0
 * - Receive (Rx) buffer: on
 * - Transmit (Tx) buffer: off
 * - Flow control: off
 * - Event queue: on
 * - Pin assignment: TxD (default), RxD (default)
 */

uint8_t *dtmp;
char *gpsData;
char **dataField;
char *timeText;

/**
 * Return the hour of time retrieved from gps 
 */
int get_hours(void)
{
	//0 and 1 refers to hours in utc string
	return ((dataField[UTC][0] - '0') * 10 + (dataField[UTC][1] - '0') - 2);
}

/**
 * Returns the minutes of the time retrieved from gps lock
 */
int get_min(void)
{
	//2 and 3 refers to minutues in utc string
	return ((dataField[UTC][2] - '0') * 10 + (dataField[UTC][3] - '0'));
}

/**
 * Return the seconds of the time retrieved from gps lock
 */
int get_sec(void)
{
	//4 and 5 refers to seconds in utc string
	return ((dataField[UTC][4] - '0') * 10 + (dataField[UTC][5] - '0'));
}

/**
 * Return the string with utc time retrieved from gps 
 */
char *get_utc(void)
{
	return dataField[UTC];
}

/**
 * Return the gps lock status 
 */
int get_status(void)
{

	if (dataField[STATUS] == NULL || !strlen(dataField[STATUS]))
	{ //if gps is not available

		return 0;
	}
	return !!(strcmp(dataField[STATUS], "V"));
}

/**
 * Returns the latitude of the location from gps lock
 */
char *get_lat(void)
{
	return dataField[LATITUDE];
}

/**
 * Returns the north/south indicator
 */
char *get_north_south(void)
{
	return dataField[NORTH_SOUTH];
}

/**
 * Returns the longitude value from gps
 */
char *get_long(void)
{
	return dataField[LONGITUDE];
}

/**
 * Returns the east/west indicator from gps
 */
char *get_east_west(void)
{
	return dataField[EAST_WEST];
}

/**
 * Return the current date as retrived frommgps
 */
char *get_date(void)
{
	return dataField[DATE_GPS];
}

/**
 * Returns the string with formatted gps time
 */
char *get_time(void)
{
	//struct to hold current time
	struct timeval rtcTime;
	gettimeofday(&rtcTime, NULL);

	uint32_t hour = rtcTime.tv_sec / 3600;
	uint32_t minutes = (rtcTime.tv_sec - (hour * 3600)) / 60;
	uint32_t sec = rtcTime.tv_sec - (hour * 3600) - (minutes * 60);
	sprintf(timeText, " %d:%d:%d", hour, minutes, sec);
	timeText[strlen(timeText)] = '\0';
	return timeText;
}

/**
 * Parses the string retrieved from the gps 
 */
int parse_nmea(void)
{
	int letterIndex = 0, arrayIndex = 0, startIndex = 0;

	gpsData = strstr(gpsData, "$GPRMC"); // move pinter to RMC line
	if (gpsData == NULL)
	{
		return 0;
	}

	while (gpsData[letterIndex] != '\n')
	{
		//separate string at ','
		if (gpsData[letterIndex] == ',')
		{
			//point the string to pointer in array
			gpsData[letterIndex] = '\0';
			dataField[arrayIndex++] = gpsData + startIndex;
			startIndex = letterIndex + 1;
		}
		letterIndex++;
	}
	return 1;
}

/**
 * Returns the length of the gps data
 */
int get_gps_data_len(void)
{

	int len = uart_read_bytes(EX_UART_NUM, dtmp, BUF_SIZE, 20 / portMAX_DELAY);
	if (len)
	{
		gpsData = (char *)dtmp;
		if (parse_nmea())
		{
			//set rtc time
			if (get_status())
			{
				rtc_gps(get_hours(), get_min(), get_sec());
			}

			return 1;
		}
	}

	return 0;
}

/**
 * Init the gps 
 */
void gps_init(void)
{
	/* Configure parameters of an UART driver,
	 * communication pins and install the driver */
	uart_config_t uart_config = {
		.baud_rate = 9600,
		.data_bits = UART_DATA_8_BITS,
		.parity = UART_PARITY_DISABLE,
		.stop_bits = UART_STOP_BITS_1,
		.flow_ctrl = UART_HW_FLOWCTRL_DISABLE};

	uart_param_config(EX_UART_NUM, &uart_config);
	//Set UART pins (using UART0 default pins ie no changes.)
	uart_set_pin(EX_UART_NUM, ECHO_TEST_TXD, ECHO_TEST_RXD, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
	//Install UART driver, and get the queue.
	uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, 0, 0, NULL, 0);

	// malloc space for strings
	gpsData = (char *)malloc(sizeof(char) * BUF_SIZE);
	dtmp = (uint8_t *)malloc(RD_BUF_SIZE);
	dataField = malloc(sizeof(char *) * GPS_INFO_BUFFER);
	timeText = malloc(sizeof(char) * BUF_SIZE);
}

/**
 * Set the rtc time to time retrieved by gps
 */
void rtc_gps(int h, int m, int s)
{
	//store gps time as rtc time
	struct timeval tv;
	tv.tv_sec = h * 3600 + m * 60 + s;
	settimeofday(&tv, NULL);
}
