#include <stdio.h>
#include <stdlib.h>
#include "tone_detection.h"

static esp_adc_cal_characteristics_t *adc_chars;
static const adc_channel_t channel = ADC_CHANNEL_0;
static const adc_atten_t atten = ADC_ATTEN_DB_11;
static const adc_unit_t unit = ADC_UNIT_1;

float SAMPLE_BUFFER[SAMPLES_PER_CALC];

int count = 0, average = 0, average4k = 0, result = 0;
float totaltone1k = 0, totaltone4k = 0, tone1k = 0, tone4k = 0;

/** Return the result of the tone detection sample */
int get_result(void)
{
	return result;
}

/** Resets the result of the tone detection sample */
void reset_result(void)
{
	result = NO_TONE_DETECTED;
}

/** Initialize the ADC pin to get the samples */
void init_tone_detection(void)
{
	//Configure ADC
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(channel, atten);

	//Characterize ADC
	adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);
}

/** Get the samples from ADC, save them and process them */
void task_sample_tone(void *param)
{
	uint32_t adc_reading = 0;
	int raw, i = 0;

	while (1)
	{
		//Multisampling
		for (i = 0; i < NO_OF_TONE_SAMPLES; i++)
		{
			if (unit == ADC_UNIT_1)
			{
				adc_reading = adc1_get_raw((adc1_channel_t)channel);
				//Buffer that keeps the samples from ADC
				SAMPLE_BUFFER[i] = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
			}
		}

		//Calculates the frequency of the tone, if it matches the target frequencies or not
		tone1k = goertzel_mag_1k(NO_OF_TONE_SAMPLES, TARGET_FREQUENCY_1k, SAMPLING_RATE_1k, SAMPLE_BUFFER);
		tone4k = goertzel_mag_4k(NO_OF_SAMPLES_4k, TARGET_FREQUENCY_4k, SAMPLING_RATE_4k, SAMPLE_BUFFER);

		totaltone1k += tone1k;
		totaltone4k += tone4k;
		//Process the samples 15 times and then it compares the average to a threshold value
		if (count >= 15)
		{
			average = totaltone1k / count;
			average4k = totaltone4k / count;
			count = 0;
			totaltone1k = 0;
			totaltone4k = 0;
			tone_detection();
			average = 0;
			average4k = 0;
		}

		vTaskDelay(pdMS_TO_TICKS(250));
	}
}

/** 
 * Calculates the magnitude of the signal obtained according the target frequency (1KHz) 
 */
float goertzel_mag_1k(int numSamples, int TARGET_FREQUENCY, int SAMPLING_RATE, float *data)
{
	int k, i;
	float floatnumSamples, omega, cosine, coeff, q0, q1, q2;

	//algorithm to calculate the freq
	count++;
	floatnumSamples = (float)numSamples;
	k = (int)(0.5 + ((floatnumSamples * TARGET_FREQUENCY) / SAMPLING_RATE));
	omega = (((2.0 * M_PI) / floatnumSamples) * k);
	cosine = cos(omega);
	coeff = 2.0 * cosine;
	q0 = 0, q1 = 0, q2 = 0;

	for (i = 0; i < (floatnumSamples - 1); i++)
	{
		q0 = coeff * q1 - q2 + data[i];
		q2 = q1;
		q1 = q0;
	}

	// calculate the real and imaginary results
	// scaling appropriately
	return sqrtf(q1 * q1 + q2 * q2 - q1 * q2 * coeff);
}

/** 
 * Calculates the magnitude of the signal obtained according the target frequency (4KHz) 
 */
float goertzel_mag_4k(int numSamples, int TARGET_FREQUENCY, int SAMPLING_RATE, float *data)
{
	int k, i;
	float floatnumSamples, omega, cosine, coeff, q0, q1, q2;

	floatnumSamples = (float)numSamples;
	k = (int)(0.5 + ((floatnumSamples * TARGET_FREQUENCY) / SAMPLING_RATE));
	omega = (((2.0 * M_PI) / floatnumSamples) * k);
	cosine = cos(omega);
	coeff = 2.0 * cosine;
	q0 = 0, q1 = 0, q2 = 0;

	for (i = 0; i < (floatnumSamples - 1); i++)
	{
		q0 = coeff * q1 - q2 + data[i];
		q2 = q1;
		q1 = q0;
	}

	// calculate the real and imaginary results
	// scaling appropriately
	return sqrtf(q1 * q1 + q2 * q2 - q1 * q2 * coeff);
}

/** 
 * Checks which tone was detected 
 */
void tone_detection(void)
{
	if (average > THRESHOLD_1K)
	{ //check for 1k
		if (result == NO_TONE_DETECTED)
		{
			result = ONE_TONE_DETECTED;
		}
		else if (result == FOUR_TONE_DETECTED)
		{

			result = BOTH_TONE_DETECTED;
		}
	}

	// if 4k detected
	if (average4k > THRESHOLD_4K)
	{ //check for 4k

		if (result == NO_TONE_DETECTED)
		{
			//only 4k
			result = FOUR_TONE_DETECTED;
		}
		else if (result == ONE_TONE_DETECTED)
		{
			//if 1k also detected
			result = BOTH_TONE_DETECTED;
		}
	}

	//update tone detect result
	if (average < NO_TONE_THR && average4k < NO_TONE_THR)
	{
		result = DETECTION_OFF;
	}
}
