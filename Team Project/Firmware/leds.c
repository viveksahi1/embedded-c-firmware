#include <stdio.h>
#include "leds.h"

/** function to handle red led in active mode
  * This function sets the gpio pin 12 to high and low.
  */
void red_led_task(void *param)
{
	while (1)
	{
		//ligth sleep or active mode
		gpio_set_level(LED_RED_GPIO, 1);
		vTaskDelay(100 / portTICK_RATE_MS);
		gpio_set_level(LED_RED_GPIO, 0);
		vTaskDelay(100 / portTICK_RATE_MS);
		gpio_set_level(LED_RED_GPIO, 1);

		vTaskDelay(100 / portTICK_RATE_MS);
		gpio_set_level(LED_RED_GPIO, 0);
		vTaskDelay(9700 / portTICK_RATE_MS);
	}
}

/** function to handle red led in active mode
  * This function sets the gpio pin 12 to high and low.
  */
void green_led_task(void *param)
{
	while (1)
	{
		//ligth sleep or active mode
		gpio_set_level(LED_GREEN_GPIO, 1);
		vTaskDelay(100 / portTICK_RATE_MS);
		gpio_set_level(LED_GREEN_GPIO, 0);
		vTaskDelay(250 / portTICK_RATE_MS);
	}
}