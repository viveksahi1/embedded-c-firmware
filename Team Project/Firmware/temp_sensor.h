#ifndef _TEMP_SENSOR_H_
#define _TEMP_SENSOR_H_

#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "tone_detection.h"

#define DEFAULT_VREF 1100 //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES 64  //Multisampling
#define GPS_DATA_BUFFER 1000

void get_temp_reading(void);
float get_temp_value(void);
float get_hum_value(void);
void init_temp_sensor(void);

#endif