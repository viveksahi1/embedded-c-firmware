/*standard includes */
#include "driver/i2c.h"
#include "sdkconfig.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

/* drivers */
#include "driver/adc.h"
#include "driver/gpio.h"
#include "driver/rtc_io.h"
#include "driver/touch_pad.h"
#include "esp_partition.h"
#include "nvs.h"
#include "rom/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/rtc_io_reg.h"

/* file includes */
#include "accelerometer.h"
#include "leds.h"
#include "temp_sensor.h"
#include "tone_detection.h"
#include "uart_gps.h"
#include "uv_sensor.h"
#include "voltage_measurement.h"
#include "wifi_mqtt.h"

/*switch defines */
#define GPIO_INPUT_IO_0 25
#define GPIO_INPUT_IO_GPS 32
#define GPIO_INPUT_TESTMODE 33
#define GPIO_INPUT_IO_TEST 3
#define GPS_SWITCH_PIN 5
#define TEMP_OUTPUT_IO 27
#define GPIO_INPUT_PIN_SEL                                                     \
  ((1ULL << GPIO_INPUT_IO_0) | (1ULL << GPIO_INPUT_IO_GPS) |                   \
   (1ULL << GPIO_INPUT_TESTMODE))
#define ESP_INTR_FLAG_DEFAULT 0

/* sleep modes */
#define ACTIVE_MODE 0
#define LIGHT_SLEEP_MODE 1
#define DEEP_SLEEP_MODE 2
#define SLEEP_MS 1000
#define DEEP_SLEEP_TIME (10 * SLEEP_MS * SLEEP_MS)

#define MAX_PACKETS 60
#define LIGHT_SLEEP_TIME 30
#define WAKE_LIGHT_SLEEP_SEC 9
#define MSG_COUNT_BUFFER 3

// MQTT client
esp_mqtt_client_handle_t client;
// GPIo interrupt handler
static xQueueHandle gpio_evt_queue = NULL;

/* handler for freertos tass */
TaskHandle_t xHandleRed = NULL;
TaskHandle_t xHandleGreen = NULL;
TaskHandle_t xHandleTone = NULL;

// sleep mode enter time
static RTC_DATA_ATTR struct timeval sleep_enter_time;
RTC_DATA_ATTR int numPacketStored = 0;
int mode = ACTIVE_MODE;
int deepSleep = 0;

nvs_handle my_handle; // vns flash handle

/**
 * Push button ISR
 */
static void IRAM_ATTR gpio_isr_handler(void *arg) {
  uint32_t gpio_num = (uint32_t)arg;
  xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

/**
 * Interrupt handler for gpio
 */
static void gpio_task_example(void *arg) {
  uint32_t io_num;

  // push button interrpt
  for (;;) {
    if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {

      if (io_num == GPIO_INPUT_IO_0) {

        deepSleep = 1; // enable deep sleep

      } else if (io_num == GPIO_INPUT_IO_GPS) {

        // update gps switch level
        gpio_set_level(GPS_SWITCH_PIN, gpio_get_level(io_num));
      }
    }
  }
}

/**
 * Init fiven gpio as output pin
 * Param	gpioPin
 *			-pin to configure as output
 */
void init_output_gpio(int gpioPin) {
  /* Set pin 5 to output mode for gps */
  gpio_config_t io_conf;
  // disable interrupt
  io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
  // set as output mode
  io_conf.mode = GPIO_MODE_OUTPUT;
  // bit mask of the pins that you want to set
  io_conf.pin_bit_mask = 1ULL << gpioPin;
  // disable pull-down mode
  io_conf.pull_down_en = 0;
  // disable pull-up mode
  io_conf.pull_up_en = 0;
  // configure GPIO with the given settings
  gpio_config(&io_conf);
}

/**
 * initialise toggle switch as input mode
 */
static void init_switch(void) {

  /* Set pin 5 to output mode for gps */
  gpio_config_t io_conf;

  init_output_gpio(GPIO_INPUT_IO_GPS);
  init_output_gpio(TEMP_OUTPUT_IO);
  init_output_gpio(LED_GREEN_GPIO);
  init_output_gpio(LED_RED_GPIO);

  /* setup pins 32 and 27 for input interrupt */
  io_conf.intr_type = GPIO_PIN_INTR_POSEDGE;
  io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
  io_conf.mode = GPIO_MODE_INPUT; // set as input mode
  io_conf.pull_down_en = 1;       // enable pull-up mode
  gpio_config(&io_conf);

  // change gpio intrrupt type for one pin
  gpio_set_intr_type(GPIO_INPUT_IO_0, GPIO_INTR_ANYEDGE);
  gpio_set_intr_type(GPIO_INPUT_IO_GPS, GPIO_INTR_ANYEDGE);

  // create a queue to handle gpio event from isr
  gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
  // start gpio task
  xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);

  // install gpio isr service
  gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
  // hook isr handler for specific gpio pin
  gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler,
                       (void *)GPIO_INPUT_IO_0);
  gpio_isr_handler_add(GPIO_INPUT_IO_GPS, gpio_isr_handler,
                       (void *)GPIO_INPUT_IO_GPS);

  // remove isr handler for gpio number.
  gpio_isr_handler_remove(GPIO_INPUT_IO_0);
  // hook isr handler for specific gpio pin again
  gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler,
                       (void *)GPIO_INPUT_IO_0);
}

/**
 *	Deep sleep wake up stub
 */
void RTC_IRAM_ATTR esp_wake_deep_sleep(void) { esp_default_wake_deep_sleep(); }

/**
 * Sets the gpio led pin to high and low.
 * Blinks red led
 */
void flash_led(void) {

  gpio_set_level(LED_RED_GPIO, 1);
  vTaskDelay(100 / portTICK_RATE_MS);
  gpio_set_level(LED_RED_GPIO, 0);
  vTaskDelay(100 / portTICK_RATE_MS);
}

/**
 * Handles the wakeup from deep sleep
 */
void handle_deep_sleep_led(void) {

  if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_TIMER) {

    // if wake up was caused by a timer
    flash_led(); // flash led
    esp_sleep_enable_timer_wakeup(DEEP_SLEEP_TIME);
    esp_sleep_enable_ext0_wakeup(GPIO_INPUT_IO_0, 1); // Wake if GPIO is low
    printf("/*********  Entering Deep sleep ************/\n");
    esp_set_deep_sleep_wake_stub(&esp_wake_deep_sleep);
    esp_deep_sleep_start();
  } else if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT0) {

    mode = LIGHT_SLEEP_MODE;
  }
}

/**
 * Remove green led freertos task
 * Sets the GRIO level to 0
 */
void delete_active_led(void) {

  // delete green led task
  if (xHandleGreen != NULL) {

    vTaskDelete(xHandleGreen);
    xHandleGreen = NULL;
  }

  gpio_set_level(LED_GREEN_GPIO, 0);
}

/**
 * Removes red led and tone detect task from the stack
 *
 */
void delete_tone_detect_task(void) {

  // delete red led task
  if (xHandleRed != NULL) {

    vTaskDelete(xHandleRed);
    xHandleRed = NULL;
  }
  gpio_set_level(LED_RED_GPIO, 0);

  // delete tone detect task
  if (xHandleTone != NULL) {

    vTaskDelete(xHandleTone);
    xHandleTone = NULL;
  }
}

/**
 * Store given packet in flash memory
 */
void store_packets_in_flash(char *packet) {

  char msgCountBuffer[MSG_COUNT_BUFFER];

  // store packet in flash memory
  sprintf(msgCountBuffer, "%d", numPacketStored++);
  msgCountBuffer[strlen(msgCountBuffer)] = '\0';
  nvs_set_str(my_handle, msgCountBuffer, packet);
  nvs_commit(my_handle);
}

/**
 * Init hardware devices
 */
void hardware_init(void) {

  // init devices
  gpio_set_level(TEMP_OUTPUT_IO, 1);
  i2c_example_master_init();
  init_acclerometer();
  init_temp_sensor();
  init_voltage_meassurement();
  init_tone_detection();
  gps_init();
}

void app_main() {
  struct timeval now;
  int lightSleepTime = 10000, testMode = 1;
  uint32_t timeToneStart;
  char *packet;
  char *msg = malloc(MAX_MSG_SIZE);
  char msgCountBuffer[MSG_COUNT_BUFFER];

  /* Init hardware and handle wake up stub */
  init_switch();           // init push button interrupts
  handle_deep_sleep_led(); // setup deep sleep wake up stub
  // hardware_init
  hardware_init();
  voltage_measurement();
  esp_err_t err = nvs_flash_init();
  wifi_setup();
  mqtt_app_start();
  esp_mqtt_client_start(client);
  vTaskDelay(1000 / portTICK_RATE_MS);
  get_temp_reading();

  /* Initialise flash partiotion */
  if (err == ESP_ERR_NVS_NO_FREE_PAGES) {

    // find the NVS partition
    const esp_partition_t *nvs_partition = esp_partition_find_first(
        ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, NULL);
    if (!nvs_partition) {

      printf("FATAL ERROR: No NVS partition found\n");
      while (1)
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }

    // erase the partition
    err = (esp_partition_erase_range(nvs_partition, 0, nvs_partition->size));
    if (err != ESP_OK) {

      printf("FATAL ERROR: Unable to erase the partition\n");
      while (1)
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
  }

  // now try to initialize it again
  err = nvs_flash_init();
  nvs_open("storage", NVS_READWRITE, &my_handle);

  while (1) {

    /*chane this */
    testMode = gpio_get_level(GPIO_INPUT_TESTMODE); // update test mode

    switch (mode) {

    case ACTIVE_MODE:

      printf("/*************  ACTIVE MODE *************/\r\n");
      xTaskCreate(red_led_task, "red_led_task", 1024, NULL, 2, &xHandleRed);
      xTaskCreate(green_led_task, "green_led_task", 1024, NULL, 2,
                  &xHandleGreen);
      xTaskCreate(task_sample_tone, "task_sample_tone", 10240, NULL, 2,
                  &xHandleTone);

      if (accelerometer_read()) { // if movememnt detected

        printf("MOVEMENT DETeCTED\r\n");
        if (gpio_get_level(GPIO_INPUT_IO_GPS)) {

          while (!get_gps_data_len())
            ;
          printf("%s,%d,%s,%s,%s,%s,%s\r\n", get_utc(), get_status(), get_lat(),
                 get_north_south(), get_long(), get_east_west(), get_date());
          vTaskDelay(1000 / portTICK_RATE_MS); // delay to allow wifi to connect
        }
        esp_wifi_start();
        vTaskDelay(1000 / portTICK_RATE_MS); // delay to allow wifi to connect
        wifi_scan();
        vTaskDelay(1000 / portTICK_RATE_MS); // delay to scan to finish

        packet = generate_packet(
            sensor_print(), get_temp_value(), get_hum_value(), get_voltage(),
            gpio_get_level(
                GPIO_INPUT_IO_GPS)); // append gps string to mqtt server

        if (get_wifi_status()) {

          if (esp_mqtt_client_start(client) != ESP_FAIL) { // start the client
            vTaskDelay(1000 / portTICK_RATE_MS);
            /* Attemp to publish message to MQTT */
            if (!publish_mqtt_msg(packet)) {

              while (numPacketStored) {

                sprintf(msgCountBuffer, "%d", numPacketStored);
                msgCountBuffer[strlen(msgCountBuffer)] = '\0';
                size_t string_size;
                nvs_get_str(my_handle, msgCountBuffer, msg, &string_size);
                if (!publish_mqtt_msg(msg)) {

                  break;
                }

                vTaskDelay(1000 / portTICK_RATE_MS);
              }

            } else {

              store_packets_in_flash(packet);
            }
          } else {

            store_packets_in_flash(packet);
          }
        } else {

          store_packets_in_flash(packet);
        }
        esp_mqtt_client_stop(client);
        // delay to allow wifi to connect
        vTaskDelay(1000 / portTICK_RATE_MS);
      } else { // no movement

        printf("NO MOVEMENT DETECTED\r\n");
        esp_wifi_start();
        if (get_wifi_status()) {

          if (esp_mqtt_client_start(client) != ESP_FAIL) { // start the client

            vTaskDelay(1000 / portTICK_RATE_MS);
            if (!publish_mqtt_msg("{\"movement\":0}")) {

              while (numPacketStored) {

                sprintf(msgCountBuffer, "%d", numPacketStored);
                msgCountBuffer[strlen(msgCountBuffer)] = '\0';
                size_t string_size;
                nvs_get_str(my_handle, msgCountBuffer, msg, &string_size);
                if (!publish_mqtt_msg(msg)) {

                  break;
                }

                vTaskDelay(1000 / portTICK_RATE_MS);
              }
            }
          }
        }
      }

      // transmit data
      mode = LIGHT_SLEEP_MODE;
      esp_wifi_stop();

      delete_active_led(); // stop green led
      vTaskDelay(1000 / portTICK_RATE_MS);

      delete_tone_detect_task();
      get_temp_reading();
      vTaskDelay(1000 / portTICK_RATE_MS);
      break;

    case LIGHT_SLEEP_MODE:

      gpio_set_level(TEMP_OUTPUT_IO, 0);
      esp_wifi_stop();
      /* loop to wake up from light sleep every 10 sec to flash led */
      for (int i = 0; i < 3; i++) // dvide the sleep mode into 3 modes
      {
        // Setup sleep mode to wake every 10 sec to flash led
        if (!testMode) {
          lightSleepTime = 10000; // 10000 -> 10s
        } else {

          lightSleepTime = 1000; // 1000 ms
          i = 3;                 // ensure only 1 interation in test mode
        }

        // go back to sleep if sleep time left > 50ms
        while (lightSleepTime > 50) // 50 ms
        {

          // setup wakeup sources
          esp_sleep_enable_timer_wakeup(lightSleepTime * SLEEP_MS);
          esp_sleep_enable_ext0_wakeup(GPIO_INPUT_IO_0,
                                       1); // Wake if GPIO is low

          gettimeofday(&sleep_enter_time, NULL);
          esp_light_sleep_start();
          // time spent in sleep
          gettimeofday(&now, NULL);

          int timeSpentInSleep =
              ((now.tv_sec - sleep_enter_time.tv_sec) * 1000 +
               (now.tv_usec - sleep_enter_time.tv_usec) / 1000);
          lightSleepTime -= timeSpentInSleep;

        } // exit light sleep to blink led

        if (i < 2) // blink led every 10 sec
        {
          flash_led();
          flash_led();
        }

        // diable sources
        ESP_ERROR_CHECK(esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_EXT0));
        ESP_ERROR_CHECK(
            esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_TIMER));
      }

      if (deepSleep) // enter deep sleep
      {

        esp_wifi_stop();
        mode = DEEP_SLEEP_MODE;
      } else {
        // reinit gpio push button pin
        rtc_gpio_deinit(GPIO_INPUT_IO_0);
        get_voltage();
        esp_wifi_start();
        gpio_set_level(TEMP_OUTPUT_IO, 1);
        vTaskDelay(100); // 100ms delay
        mode = ACTIVE_MODE;
      }
      break;
    case DEEP_SLEEP_MODE:

      gpio_set_level(TEMP_OUTPUT_IO, 0);
      gpio_set_level(GPS_SWITCH_PIN, 0);
      printf("/*********  Entering Deep sleep ************/\n");
      esp_sleep_enable_timer_wakeup(1); // reset chip to allow stub wake up
      esp_deep_sleep_start();           // enter deep sleep
      break;

    default:
      break;
    }
  }
}
