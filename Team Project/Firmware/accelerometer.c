#include "accelerometer.h"

/** Acceleremoter register read/write 
 * @param i2c_num : i2c port
 * 
 * @param reg	: register number
 * 
 * @param data	: stores return data
 * 
 * @return	esp_err
 */
esp_err_t acc_write_reg(i2c_port_t i2c_num, uint8_t reg, uint8_t data)
{
	int ret;

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, MMA845_SENSOR_ADDR << 1 | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (0xFF & data), ACK_CHECK_EN);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

	return ret;
}

/**
 * @brief test code to write esp-i2c-slave
 *
 */
esp_err_t acc_read_reg(i2c_port_t i2c_num, uint8_t *data_l, uint8_t reg)
{
	int ret;

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, MMA845_SENSOR_ADDR << 1 | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, MMA845_SENSOR_ADDR << 1 | READ_BIT, ACK_CHECK_EN);
	i2c_master_read_byte(cmd, data_l, NACK_VAL);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

	return ret;
}

/**
 *  read INT_STATUS register and report movement 
 */
int accelerometer_read(void)
{
	int ret;
	uint8_t sensor_data_Y = 0;

	ret = acc_read_reg(I2C_EXAMPLE_MASTER_NUM, &sensor_data_Y, INT_SRC);
	if (ret == ESP_ERR_TIMEOUT)
	{
		printf("I2C timeout\n");
	}
	else if (ret == ESP_OK)
	{
		if ((sensor_data_Y & 0x20) == 0x20)
		{
			ret = acc_read_reg(I2C_EXAMPLE_MASTER_NUM, &sensor_data_Y,
							   TRANSIENT_SCR);
			return 1;
		}
	}
	else
	{
		printf("%s: No ack, sensor not connected...skip...\n", esp_err_to_name(ret));
	}

	return 0;
}

/**
 * Set the accelerometer in low power mode
 */
void acc_low_power(void)
{
	acc_write_reg(I2C_NUM_1, CTRL_REG2, CLR_SENSOR);
	acc_write_reg(I2C_NUM_1, CTRL_REG1, STANDBY_MODE);
	acc_write_reg(I2C_NUM_1, CTRL_REG1, 0x60);
	acc_write_reg(I2C_NUM_1, CTRL_REG2, 0x1F);
	//acc_write_reg(I2C_NUM_1, CTRL_REG1, ACTIVE_STATE);
}

/**
 * Init the accelerometer in i2c device
 */
void init_acclerometer(void)
{
	uint8_t xValue;

	/* INIT ACCELEROMTER FOR INTERRUPT SETUP */
	acc_write_reg(I2C_NUM_1, CTRL_REG2, CLR_SENSOR);
	acc_write_reg(I2C_NUM_1, CTRL_REG1, STANDBY_MODE);

	/* Setup transient interrupt */
	acc_write_reg(I2C_NUM_1, TRANSIENT_CFG, TRANSIENT_CFG_VALUE);
	acc_write_reg(I2C_NUM_1, TRANSIENT_THR, TRANSIENT_THR_VALUE);
	acc_write_reg(I2C_NUM_1, TRANSIENT_COUNT, TRANSIENT_COUNT_VALUE);
	acc_write_reg(I2C_NUM_1, CTRL_REG4, REG4_VALUE);
	acc_write_reg(I2C_NUM_1, CTRL_REG5, REG5_VALUE);
	acc_write_reg(I2C_NUM_1, CTRL_REG1, ACTIVE_STATE);
}
