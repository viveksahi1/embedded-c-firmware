#ifndef _UV_SENSOR_H_
#define _UV_SENSOR_H_

#include <stdio.h>
#include <stdlib.h>
#include "driver/i2c.h"
#include <driver/i2c.h>
#include <esp_log.h>
#include "sdkconfig.h"

/* UV sensor defines */
#define VEML6075_REG_CONF 0x00    // Configuration register (options below)
#define VEML6075_REG_UVA 0x07     // UVA register
#define VEML6075_REG_DUMMY 0x08   // Dark current register (NOT DUMMY)
#define VEML6075_REG_UVB 0x09     // UVB register
#define VEML6075_REG_UVCOMP1 0x0A // Visible compensation register
#define VEML6075_REG_UVCOMP2 0x0B // IR compensation register
#define VEML6075_REG_DEVID 0x0C   // Device ID register

#define VEML6075_UVI_UVA_VIS_COEFF 3.33
#define VEML6075_UVI_UVA_IR_COEFF 2.5
#define VEML6075_UVI_UVB_VIS_COEFF 3.66
#define VEML6075_UVI_UVB_IR_COEFF 2.75

#define VEML6075_UVI_UVA_RESPONSE (1.0 / 909.0)
#define VEML6075_UVI_UVB_RESPONSE (1.0 / 800.0)

#define I2C_EXAMPLE_MASTER_SCL_IO 22        /*!< gpio number for I2C master clock */
#define I2C_EXAMPLE_MASTER_SDA_IO 21        /*!< gpio number for I2C master data  */
#define I2C_EXAMPLE_MASTER_NUM I2C_NUM_1    /*!< I2C port number for master dev */
#define I2C_EXAMPLE_MASTER_TX_BUF_DISABLE 0 /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_RX_BUF_DISABLE 0 /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_FREQ_HZ 100000   /*!< I2C master clock frequency */

#define VEML6075_SENSOR_ADDR 0x10  /*!< slave address for BH1750 sensor */
#define BH1750_CMD_START 0x07      /*!< Command to set measure mode */
#define ESP_SLAVE_ADDR 0x28        /*!< ESP32 slave address, you can set any 7bit value */
#define WRITE_BIT I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ   /*!< I2C master read */
#define ACK_CHECK_EN 0x1           /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0          /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                /*!< I2C ack value */
#define NACK_VAL 0x1               /*!< I2C nack value */

esp_err_t i2c_example_master_sensor_test(i2c_port_t i2c_num, uint8_t *data_h, uint8_t *data_l, uint8_t reg);
int sensor_print(void);
void i2c_example_master_init(void);

#endif