#include "wifi_mqtt.h"

/* mqtt client setup variables */
wifi_ap_record_t *scan_list;				//list of scan wifi
static EventGroupHandle_t wifi_event_group; //wifi group
const static int CONNECTED_BIT = BIT0;
esp_mqtt_client_handle_t client2;		//mqtt client
uint16_t apCount;						//list of scan results
int scan_flag = 0;						// scan complete flag
static const char *TAG = "MQTT_SAMPLE"; //MQTT tag
extern esp_mqtt_client_handle_t client; //MQTT client
char *msgToSend;						// msg to send to the mqttt server
const TickType_t xTicksToWait = 1000 / portTICK_PERIOD_MS;

/**
 * Event handler for MQTT client 
 */
esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
	esp_mqtt_client_handle_t client_1 = event->client;
	client2 = event->client;
	int msg_id = 0;

	switch (event->event_id)
	{

	case MQTT_EVENT_CONNECTED:

		ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
		msg_id = esp_mqtt_client_subscribe(client_1, "/engg4810_2018/team15", 0);
		ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
		break;

	case MQTT_EVENT_DISCONNECTED:

		ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
		break;

	case MQTT_EVENT_SUBSCRIBED:

		ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
		break;

	case MQTT_EVENT_UNSUBSCRIBED:

		ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
		break;

	case MQTT_EVENT_PUBLISHED:

		ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
		break;

	case MQTT_EVENT_DATA:

		ESP_LOGI(TAG, "MQTT_EVENT_DATA");
		printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
		printf("DATA=%.*s\r\n", event->data_len, event->data);
		break;

	case MQTT_EVENT_ERROR:

		ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
		break;
	}

	return ESP_OK;
}

/**
 * Generate packet with all the sensor data
 * 
 */
char *generate_packet(int uvInd, float tempValue, float humValue,
					  int voltValue, int gpsEnable)
{
	char buffer[BUFFER_MESSAGE];
	memset(msgToSend, 0, MAX_MSG_SIZE); //clear msg buffer*/
	/* if gps data available */
	if (gpsEnable)
	{
		sprintf(buffer, "{\"status\":%d,\"movement\":1,",
				get_status() && gpsEnable);
		strcat(msgToSend, buffer);
	}
	else
	{

		sprintf(buffer, "{\"status\":%d,\"movement\":1,", gpsEnable);
		strcat(msgToSend, buffer);
	}

	/* gps data based on gps status */
	if (gpsEnable && get_status())
	{ // gps data available

		sprintf(buffer, "\"sensors\":[{\"Time\":\"%s\",\"Lat\":\"%s\","
						"\"Long\":\"%s\",\"dir1\":\"%s\",\"dir2\":\"%s\","
						"\"Voltage\":%d,\"UV\":%d,\"Temp\":%02f,\"Hum\":%02f,"
						"\"Tone\":%d}],\"networks\":[",
				get_time(), get_lat(), get_long(), get_north_south(),
				get_east_west(), voltValue, uvInd,
				tempValue, humValue, get_result());
		strcat(msgToSend, buffer);
	}
	else
	{ //packet with no gps data

		sprintf(buffer, "\"sensors\":[{\"Time\":\"%s\",\"Voltage\":%d,"
						"\"UV\":%d,\"Temp\":%02f,\"Hum\":%02f,"
						"\"Tone\":%d}],\"networks\":[",
				get_time(), voltValue, uvInd,
				tempValue, humValue, get_result());

		strcat(msgToSend, buffer);
	}
	reset_result(); // reset tone detetion result
	// limit scan result to MAX to avoid buffer overflow
	if (apCount > MAX_SCAN_RESULT)
	{
		apCount = MAX_SCAN_RESULT;
	}

	// if no server found 
	if (!apCount)
	{
		msgToSend[strlen(msgToSend)] = ']';
		msgToSend[strlen(msgToSend)] = '}';
		msgToSend[strlen(msgToSend)] = '\0';
	}
	/* extract scan results and prepare packet to sent to mqtt */
	for (int i = 0; i < apCount; i++)
	{
		if (!strlen((char *)scan_list[i].ssid))
		{ //igonre network with no ssid

			continue;
		}
		sprintf(buffer, "{\"SSID\":\"%s\",\"RSSI\":%d,\"BSSID\":\"", (char *)scan_list[i].ssid,
				scan_list[i].rssi);
		strcat(msgToSend, buffer);

		for (int j = 0; j < 6; j++)
		{ //rssi is 6 hex digits

			sprintf(buffer, "%02X", scan_list[i].bssid[j]);
			strcat(msgToSend, buffer);
		}
		strcat(msgToSend, "\"},");
	}

	msgToSend[strlen(msgToSend) - 1] = ']';
	msgToSend[strlen(msgToSend)] = '}';
	msgToSend[strlen(msgToSend)] = '\0';
	scan_flag = 0;

	return msgToSend;
}

/**
 * Attempt to publisht the given package to the mqtt server
 */
int publish_mqtt_msg(char *packet)
{

	if (esp_mqtt_client_publish(client2, "/engg4810_2018/team15",
								packet, 0, 0, 0) == ESP_OK)
	{
		return ESP_OK;
	}

	return 1; // not delivered
}

/**
 * publish no movement package to the mqtt server
 */
void no_movement_package(void)
{
	esp_mqtt_client_publish(client2, "/engg4810_2018/team15",
							"{\"movement\":0}", 0, 0, 0);
}

/** 
 * handler for wifi connection
 */
esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
	switch (event->event_id)
	{

	case SYSTEM_EVENT_STA_START:

		esp_wifi_connect();
		break;

	case SYSTEM_EVENT_STA_GOT_IP:

		xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
		break;

	case SYSTEM_EVENT_STA_DISCONNECTED:

		esp_wifi_connect();
		xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
		break;

	case SYSTEM_EVENT_SCAN_DONE:

		scan_flag = 0;
		apCount = 0;
		esp_wifi_scan_get_ap_num(&apCount);

		if (apCount == 0)
		{
			return ESP_OK;
		}

		scan_list = (wifi_ap_record_t *)malloc(sizeof(wifi_ap_record_t) * apCount);
		ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&apCount, scan_list));
		scan_flag = 1;
		break;

	default:
		break;
	}

	return ESP_OK;
}

/** 
 * Setup the wifi network
 */
void wifi_setup(void)
{

	msgToSend = (char *)malloc(sizeof(char) * MAX_MSG_SIZE);
	/* setup tcp ip adapter */
	tcpip_adapter_init();

	/* setup wifi event group */
	wifi_event_group = xEventGroupCreate();
	ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

	/*configure wifi */
	wifi_config_t wifi_config = {};
	strcpy((char *)wifi_config.sta.ssid, CONFIG_WIFI_SSID);
	strcpy((char *)wifi_config.sta.password, CONFIG_WIFI_PASSWORD);
	wifi_config.sta.bssid_set = false;

	/* connec to wifi */
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_LOGI(TAG, "start the WIFI SSID:[%s] password:[%s]", CONFIG_WIFI_SSID, CONFIG_WIFI_PASSWORD);
	ESP_ERROR_CHECK(esp_wifi_start());
	ESP_LOGI(TAG, "Waiting for wifi");
	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, WAIT_WIFI);
}

/**
 * Returns the wifi connection status
 */
int get_wifi_status(void)
{
	return xEventGroupGetBits(wifi_event_group);
}

/**
 * Peroforms a wifi scan to get scan networks
 */
void wifi_scan(void)
{
	/* setup eifi confguration */
	wifi_scan_config_t wifi_config = {
		.ssid = NULL,
		.bssid = NULL,
		.channel = 0,
		.show_hidden = true};

	ESP_ERROR_CHECK(esp_wifi_scan_start(&wifi_config, true));
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	ESP_ERROR_CHECK(esp_wifi_scan_stop());
}

/**
 * Starts the MQTT client server
 */
void mqtt_app_start(void)
{
	/* Setup MQTT client connection */
	const esp_mqtt_client_config_t mqtt_cfg = {
		.uri = "mqtt://tp-mqtt.zones.eait.uq.edu.au",
		.event_handle = mqtt_event_handler,
		.username = "engg4810_2018",
		.password = "blpc7n2DYExpBGY5BP7",
	};

	client = esp_mqtt_client_init(&mqtt_cfg);
}
